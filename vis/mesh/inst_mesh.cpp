#include "inst_mesh.h"

#include <utility>

InstancedMesh::InstancedMesh(
        vector<Vertex> base_verts, vector<unsigned> base_inds,
        CellContainer *cont, std::pair<double, double> color_range)
        : Mesh::Mesh(std::move(base_verts), std::move(base_inds)), s_con(cont),
          m_range_min_sq(color_range.first  * color_range.first),
          m_range_max_sq(color_range.second * color_range.second)
{
    m_num_inst = s_con->size();

    //bind vertexArrayObject
    glBindVertexArray(m_VAO);

    //generate and bind VBO for instanced data
    glGenBuffers(1, &m_inst_VBO);
    glBindBuffer(GL_ARRAY_BUFFER, m_inst_VBO);
    //reserve space on GPU memory
    glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * m_num_inst * 2, nullptr, GL_STATIC_DRAW);
    //map buffer, copy everything into it and make sure to unmap after we're done
    copyToArrayBuffer();

    //bind attribArray 2 to instance positions
    glEnableVertexAttribArray(2);
    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE,
                          (3 + 3) * sizeof(float), (void *) 0);
    glVertexAttribDivisor(2, 1); // tell OpenGL this is an instanced vertex attribute.

    //bind attribArray 3 to instance velocities
    glEnableVertexAttribArray(3);
    glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE,
                          (3 + 3) * sizeof(float), (void *) (3 * sizeof(float)));
    glVertexAttribDivisor(3, 1); // tell OpenGL this is an instanced vertex attribute.


    //unbind VAO first and then the buffers
    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

InstancedMesh::~InstancedMesh()
{
    glDeleteBuffers(1, &m_inst_VBO);
}

void InstancedMesh::updateInstAttribs()
{
    //bind vertexArrayObject
    glBindVertexArray(m_VAO);

    //generate VBO and fill with instance data
    glBindBuffer(GL_ARRAY_BUFFER, m_inst_VBO);

    //map buffer, copy everything into it and make sure to unmap after we're done
    copyToArrayBuffer();

    //unbind VAO first and then the buffers
    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void InstancedMesh::draw(ShaderProgram *shader)
{
    //bind VAO and draw it
    glBindVertexArray(m_VAO);
    glDrawElementsInstanced(GL_TRIANGLES, m_drawCount, GL_UNSIGNED_INT, nullptr, m_num_inst);
    glBindVertexArray(0);
}


void InstancedMesh::copyToArrayBuffer()
{
    //map buffer, copy everything into it and make sure to unmap after we're done
    auto *buf_ptr = (float *)glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
    memset(buf_ptr, 0, sizeof(glm::vec3) * m_num_inst * 2);

    auto sp_iter = s_con->createSPIterator();
    while(!sp_iter->isEnd()) {
        Particle *p = sp_iter->deref();
        buf_ptr[p->getID() * 3*2 + 0] = (float)p->getX().x;
        buf_ptr[p->getID() * 3*2 + 1] = (float)p->getX().y;
        buf_ptr[p->getID() * 3*2 + 2] = (float)p->getX().z;

        glm::vec3 color = getColorFromVec(p->getV());
        buf_ptr[p->getID() * 3*2 + 3 + 0] = color.r;
        buf_ptr[p->getID() * 3*2 + 3 + 1] = color.g;
        buf_ptr[p->getID() * 3*2 + 3 + 2] = color.b;

        sp_iter->increment();
    }
    glUnmapBuffer(GL_ARRAY_BUFFER);
}

#define COLOR_DAMPENING 0.8 // value of "dampening" the colors (darker when smaller)
glm::vec3 InstancedMesh::getColorFromVec(const glm::dvec3 &vec)
{
    // use l2Norm as value FIXME: sqrt
    //double norm = glm::l2Norm(vec);
    double norm = glm::dot(vec, vec);

    // half of the distance between max and min (min + 2*half_dist = max)
    double half_dist = (m_range_max_sq - m_range_min_sq) / 2.0;

    if(norm <= (m_range_max_sq - half_dist)) {
        // if in lower half of color range -> interpolate from blue to white
        float tmp = glm::clamp(((norm - m_range_min_sq) / half_dist), 0.0, 1.0);
        return glm::vec3(tmp, tmp, 1.0) * COLOR_DAMPENING;
    }else {
        // if in upper half of color range -> interpolate from white to red
        float tmp = glm::clamp(((m_range_max_sq - norm) / half_dist), 0.0, 1.0);
        return glm::vec3(1.0, tmp, tmp) * COLOR_DAMPENING;
    }
}
