#pragma once

#include "mesh.h"
#include "Container/CellContainer.h"
#include "Container/Iterator/CellIterator/SPIteratorCell.h"


class InstancedMesh : public Mesh
{
public:
    InstancedMesh(vector<Vertex> base_verts, vector<unsigned> base_inds,
                  CellContainer *s_con, std::pair<double, double> color_range);
    ~InstancedMesh() override;

    void updateInstAttribs();
    void draw(ShaderProgram *shader) override;

private:
    void copyToArrayBuffer();
    glm::vec3 getColorFromVec(const glm::dvec3 &vec);

    GLuint m_inst_VBO;
    CellContainer *s_con;
    double m_range_min_sq, m_range_max_sq;
    unsigned m_num_inst;
};