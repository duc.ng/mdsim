#include "vf_program.h"

VF_Program::VF_Program(const string &vertexFilePath, const string &fragmentFilePath) : m_max_lights(glm::uvec3(0,0,0))
{
    // Create the shaders
    m_vs = glCreateShader(GL_VERTEX_SHADER);
    m_fs = glCreateShader(GL_FRAGMENT_SHADER);

    // Read the Vertex Shader code from the file and compile it
    compile_shader(m_vs, load_shader(vertexFilePath));
    compile_shader(m_fs, load_shader(fragmentFilePath));

    //create shader program and link shaders to it
    m_program = glCreateProgram();
    glAttachShader(m_program, m_vs);
    glAttachShader(m_program, m_fs);
    glLinkProgram(m_program);
    check_shader_error(m_program, true, GL_LINK_STATUS);

    //detach vs/fs ids from program (not needed any more for it's been linked together already)
    //and delete them from graphics memory (removal of source code/object file boilerplate)
    glDetachShader(m_program, m_vs);
    glDeleteShader(m_vs);
    glDetachShader(m_program, m_fs);
    glDeleteShader(m_fs);
}

VF_Program::~VF_Program()
{
    glDeleteProgram(m_program);
}



void VF_Program::bind()
{
    glUseProgram(m_program);
}




