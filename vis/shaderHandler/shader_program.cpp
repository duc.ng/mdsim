#include "shader_program.h"

string ShaderProgram::load_shader(const string &fileName)
{
    m_fileName = fileName;

    ifstream file;
    stringstream ss;
    file.exceptions(std::ifstream::failbit | std::ifstream::badbit);
    try {
        file.open(fileName);
        ss << file.rdbuf();

    } catch(ifstream::failure &e) {
        cerr << "Failed to load shader '" << fileName << "':" << endl << e.what() << ", " << e.code() << endl;
    }
    return ss.str();
}


void ShaderProgram::compile_shader(GLuint shader, const string &code)
{
    //compile shader
    GLchar const *sourcePointer = code.c_str();
    glShaderSource(shader, 1, &sourcePointer, nullptr);
    glCompileShader(shader);

    //check if shader compiled right
    check_shader_error(shader, false, GL_COMPILE_STATUS);
}


void ShaderProgram::check_shader_error(GLuint id, bool isProgram, GLuint flag)
{
    //check shader
    GLint result = GL_FALSE, infoLogLen = 0;

    if(isProgram) {
        glGetProgramiv(id, flag, &result);
        glGetProgramiv(id, GL_INFO_LOG_LENGTH, &infoLogLen);
    } else {
        glGetShaderiv(id, flag, &result);
        glGetShaderiv(id, GL_INFO_LOG_LENGTH, &infoLogLen);
    }

    if(infoLogLen > 0) {
        //System error message
        vector<char> error_msg ((unsigned)infoLogLen + 1);
        if(isProgram) {
            glGetProgramInfoLog(id, infoLogLen, nullptr, &error_msg[0]);
            cerr << "Program linking error in program with '" << m_fileName << "' shader-file:" << endl;
        } else {
            glGetShaderInfoLog(id, infoLogLen, nullptr, &error_msg[0]);
            cerr << "Shader compilation error in shader '" << m_fileName << "':" << endl;
        }
        cerr << &error_msg[0] << endl;
    }
}

