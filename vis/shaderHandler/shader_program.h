#pragma once

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <GL/glew.h>
#include <glm/glm.hpp>

using namespace std;

class ShaderProgram
{
public:
    ShaderProgram() = default;
    virtual ~ShaderProgram() = default;

    virtual void bind() = 0;

    GLint getUniformLocation(const string &name)
        { return glGetUniformLocation(m_program, name.c_str());}
    GLuint getProgramResource(GLenum interface, const string &name)
        { return glGetProgramResourceIndex(m_program, interface, name.c_str()); }

    void setUniform1i(const string &name, GLint val)
        { this->bind(); glUniform1i(getUniformLocation(name), val); }
    void setUniform1f(const string &name, GLfloat val)
        { this->bind(); glUniform1f(getUniformLocation(name), val); }
    void setUniform3fv(const string &name, const glm::vec3 &val)
        { this->bind(); glUniform3fv(getUniformLocation(name), 1, &val[0]); }
    void setUniform4Matrix4fv(const string &name, const glm::mat4 &val)
        { this->bind(); glUniformMatrix4fv(getUniformLocation(name), 1, GL_FALSE, &val[0][0]); }

    void setSSBObinding(const string &name, GLuint binding_point)
        { glShaderStorageBlockBinding(m_program, getProgramResource(GL_SHADER_STORAGE_BLOCK, name), binding_point); }


protected:
    string load_shader(const string &fileName);
    void compile_shader(GLuint shader, const string &code);
    void check_shader_error(GLuint id, bool isProgram, GLuint flag);

    GLuint m_program = 0;
    string m_fileName;
};

