#pragma once

#include "shader_program.h"


using namespace std;

class VF_Program : public ShaderProgram
{
public:
    VF_Program(const string &vertexFilePath, const string &fragmentFilePath);
    ~VF_Program() override;

    void bind() override;

    glm::uvec3 getMaxLights() { return m_max_lights; }
    void setMaxLights(const glm::uvec3 &maxLights) { m_max_lights = maxLights; }

private:
    GLuint m_vs, m_fs;
    glm::uvec3 m_max_lights;
};
