# make doc_doxygen optional if someone does not have / like doxygen

# TODO: create CMake build option for the target.

# TODO: Add a custom target for building the documentation.

# Quellen:
#  - https://vicrucann.github.io/tutorials/quick-cmake-doxygen/
#  - https://majewsky.wordpress.com/2010/08/14/tip-of-the-day-cmake-and-doxygen/

option(BUILD_DOCUMENTATION "Build documentation" ON)

if(BUILD_DOCUMENTATION)
#    message("BUILD_DOCUMENTATION wurde AKTIVIERT")

    # check if Doxygen is installed
    find_package(Doxygen
            OPTIONAL_COMPONENTS dot mscgen dia)
    if (DOXYGEN_FOUND)
#        message("Doxygen is installed")
	
	# set input files
        configure_file(${CMAKE_CURRENT_SOURCE_DIR}/Doxyfile.in ${CMAKE_CURRENT_BINARY_DIR}/Doxyfile @ONLY)

	# set make-call argument name
        add_custom_target(doc_doxygen
                ${DOXYGEN_EXECUTABLE} ${CMAKE_CURRENT_BINARY_DIR}/Doxyfile
                WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
                COMMENT "Generating API documentation with Doxygen" VERBATIM)

    else (DOXYGEN_FOUND)
        message("Doxygen need to be installed to generate the doxygen documentation")
    endif (DOXYGEN_FOUND)

else()
#    message("BUILD_DOCUMENTATION wurde NICHT aktiviert")
endif()
