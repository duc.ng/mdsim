#include <gtest/gtest.h>
//#include <log4cxx/log4cxx.h>
//#include <log4cxx/propertyconfigurator.h>
//#include <log4cxx/helpers/exception.h>


int main(int argc, char **argv)
{
    ::testing::InitGoogleTest(&argc, argv);

    // still need to set up log4cxx config-file
    //log4cxx::PropertyConfigurator::configure("../../log4cxx-config_file.txt");

    return RUN_ALL_TESTS();
}
