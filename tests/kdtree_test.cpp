#include <gtest/gtest.h>
#include "Container/KdTree/KdTree.h"


class KdTreeTest : public ::testing::Test
{
protected:

    void SetUp() override {}

    void TearDown() override {}
};

/**
 * tests partioning of a 5*5*1 example with 6 threads and compare with pre-calculated results
 */
#define THREAD_COUNT_TEST0 6
TEST_F(KdTreeTest, testTreeNodePartition)
{
    // define scores in grid
    double scoresOfGrid[5 * 5 * 1] = {1, 0, 0, 0, 1,
                                      1, 0, 3, 5, 2,
                                      0, 1, 4, 7, 6,
                                      0, 0, 1, 4, 3,
                                      1, 0, 0, 1, 0};
    // compute total score of grid
    double total_sum = 0;
    for(int i=0; i<5*5*1; i++)
        total_sum += scoresOfGrid[i];

    // pre-calculated results of partitioning
    glm::ivec3 start_vecs[THREAD_COUNT_TEST0] = {
            glm::ivec3(0, 0, 0), glm::ivec3(3, 0, 0), glm::ivec3(0, 2, 0),
            glm::ivec3(3, 2, 0), glm::ivec3(4, 2, 0), glm::ivec3(3, 3, 0)};
    glm::ivec3 end_vecs[THREAD_COUNT_TEST0] = {
            glm::ivec3(2, 1, 0), glm::ivec3(4, 1, 0), glm::ivec3(2, 4, 0),
            glm::ivec3(3, 2, 0), glm::ivec3(4, 2, 0), glm::ivec3(4, 4, 0)};
    double scores[THREAD_COUNT_TEST0] = {5, 8, 7, 7, 6, 8};


    // create grid
    std::vector<GridElem> grid_vec;
    for(double &score : scoresOfGrid)
        grid_vec.push_back({nullptr, score});
    CellGrid grid(std::move(grid_vec), glm::ivec3(5, 5, 1));

    // create leaves vector
    std::vector<const KdTreeNode *> leaves;
    leaves.resize(THREAD_COUNT_TEST0);

    // create root node and partition on it
    KdTreeNode root(0, THREAD_COUNT_TEST0, glm::ivec3(0, 0, 0), glm::ivec3(4, 4, 0), total_sum);
    root.partitionOnGrid(true, grid, &leaves);

    // compare result with pre-calculated
    for(int i = 0; i < leaves.size(); i++) {
        const KdTreeNode *node = leaves[i];
        ASSERT_TRUE(node->start_vec == start_vecs[i]);
        ASSERT_TRUE(node->end_vec == end_vecs[i]);
        ASSERT_FLOAT_EQ(node->score, scores[i]);
    }
}


/**
 * tests if partitioning checks if splittings are valid (whether hasEnoughCells() works correctly)
 * -> partitioning segfaults here if hasEnoughCells() isn't being called / doesn't work correctly
 */
#define THREAD_COUNT_TEST1 6
TEST_F(KdTreeTest, testHasEnoughCells)
{
    // define scores in grid
    double scoresOfGrid[4 * 4 * 1] = {5  , 0  , 0  , 1.5,
                                      0  , 4.5, 0  , 1,
                                      1  , 0  , 0  , 0,
                                      0  , 0  , 0  , 2};
    // compute total score of grid
    double total_sum = 0;
    for(int i=0; i<4*4*1; i++)
        total_sum += scoresOfGrid[i];

    // pre-calculated results of partitioning
    glm::ivec3 start_vecs[THREAD_COUNT_TEST1] = {
            glm::ivec3(0, 0, 0), glm::ivec3(1, 0, 0), glm::ivec3(0, 1, 0),
            glm::ivec3(1, 1, 0), glm::ivec3(2, 0, 0), glm::ivec3(2, 2, 0)};
    glm::ivec3 end_vecs[THREAD_COUNT_TEST1] = {
            glm::ivec3(0, 0, 0), glm::ivec3(1, 0, 0), glm::ivec3(0, 3, 0),
            glm::ivec3(1, 3, 0), glm::ivec3(3, 1, 0), glm::ivec3(3, 3, 0)};
    double scores[THREAD_COUNT_TEST1] = {5, 0, 1, 4.5, 2.5, 2};


    // create grid
    std::vector<GridElem> grid_vec;
    for(double &score : scoresOfGrid)
        grid_vec.push_back({nullptr, score});
    CellGrid grid(std::move(grid_vec), glm::ivec3(4, 4, 1));

    // create leaves vector
    std::vector<const KdTreeNode *> leaves;
    leaves.resize(THREAD_COUNT_TEST1);

    // create root node and partition on it
    KdTreeNode root(0, THREAD_COUNT_TEST1, glm::ivec3(0, 0, 0), glm::ivec3(3, 3, 0), total_sum);
    root.partitionOnGrid(true, grid, &leaves);

    // compare result with pre-calculated
    for(int i = 0; i < leaves.size(); i++) {
        const KdTreeNode *node = leaves[i];
        ASSERT_TRUE(node->start_vec == start_vecs[i]);
        ASSERT_TRUE(node->end_vec == end_vecs[i]);
        ASSERT_FLOAT_EQ(node->score, scores[i]);
    }
}

/**
 * tests if partitioning fails correctly when too few cells are distributed on too many threads
 */
#define THREAD_COUNT_TEST2 10
TEST_F(KdTreeTest, testTooFewCells)
{
    // define scores in grid
    double scoresOfGrid[3 * 3 * 1] = {5  , 0  , 0,
                                      0  , 4.5, 0,
                                      1  , 0  , 0};
    // compute total score of grid
    double total_sum = 0;
    for(int i=0; i<3*3*1; i++)
        total_sum += scoresOfGrid[i];

    // create grid
    std::vector<GridElem> grid_vec;
    for(double &score : scoresOfGrid)
        grid_vec.push_back({nullptr, score});
    CellGrid grid(std::move(grid_vec), glm::ivec3(3, 3, 1));

    // create leaves vector
    std::vector<const KdTreeNode *> leaves;
    leaves.resize(THREAD_COUNT_TEST2);

    // create root node
    KdTreeNode root(0, THREAD_COUNT_TEST2, glm::ivec3(0, 0, 0), glm::ivec3(2, 2, 0), total_sum);

    // assert that partitioning throws a runtime exception
    ASSERT_THROW(root.partitionOnGrid(true, grid, &leaves), std::runtime_error);
}

#define THREAD_COUNT_TEST3 7
TEST_F(KdTreeTest, testHomogenousPartitioning)
{
    // define scores in grid
    double scoresOfGrid[4 * 4 * 1] = {10 , 0  , 0  , 4,
                                      0  , 6  , 0  , 2,
                                      3  , 0  , 0  , 0,
                                      0  , 0  , 0  , 2};
    // compute total score of grid
    double total_sum = 0;
    for(int i=0; i<4*4*1; i++)
        total_sum += scoresOfGrid[i];

    // pre-calculated results of partitioning
    glm::ivec3 start_vecs[THREAD_COUNT_TEST3] = {
            glm::ivec3(0, 0, 0), glm::ivec3(0, 1, 0), glm::ivec3(1, 0, 0),
            glm::ivec3(1, 1, 0), glm::ivec3(0, 2, 0), glm::ivec3(2, 0, 0),
            glm::ivec3(2, 1, 0)};
    glm::ivec3 end_vecs[THREAD_COUNT_TEST3] = {
            glm::ivec3(0, 0, 0), glm::ivec3(0, 1, 0), glm::ivec3(1, 0, 0),
            glm::ivec3(1, 1, 0), glm::ivec3(1, 3, 0), glm::ivec3(3, 0, 0),
            glm::ivec3(3, 3, 0)};
    double scores[THREAD_COUNT_TEST3] = {10, 0, 0, 6, 3, 4, 4};


    // create grid
    std::vector<GridElem> grid_vec;
    for(double &score : scoresOfGrid)
        grid_vec.push_back({nullptr, score});
    CellGrid grid(std::move(grid_vec), glm::ivec3(4, 4, 1));

    // create leaves vector
    std::vector<const KdTreeNode *> leaves;
    leaves.resize(THREAD_COUNT_TEST3);

    // create root node and partition on it
    KdTreeNode root(0, THREAD_COUNT_TEST3, glm::ivec3(0, 0, 0), glm::ivec3(3, 3, 0), total_sum);
    root.partitionOnGrid(true, grid, &leaves);

    // compare result with pre-calculated
    for(int i = 0; i < leaves.size(); i++) {
        const KdTreeNode *node = leaves[i];
        ASSERT_TRUE(node->start_vec == start_vecs[i]);
        ASSERT_TRUE(node->end_vec == end_vecs[i]);
        ASSERT_FLOAT_EQ(node->score, scores[i]);
    }
}
