#include <gtest/gtest.h>
#include "Container/BasicContainer.h"
#include "Thermo/StandardThermo.h"


class ThermostatTest : public ::testing::Test
{
protected:

    void SetUp() override
    {
        // fill particle list with some particles
        m_particles = std::make_shared<std::vector<Particle>>();
        m_particles->reserve(10);
        glm::dvec3 x0 = {9.5, 32.0, 0.0};
        glm::dvec3 v0 = {0.0, 0.0, 0.0};
        m_particles->emplace_back(x0, v0, 0, 0);
        glm::dvec3 x1 = {9.5, 30.5, 0.0};
        glm::dvec3 v1 = {0.0, 0.0, 0.0};
        m_particles->emplace_back(x1, v1, 0, 1);
        glm::dvec3 x2 = {11.5, 32.3, 0.0};
        glm::dvec3 v2 = {0.0, 0.0, 0.0};
        m_particles->emplace_back(x2, v2, 0, 2);


        // CellContainer
        std::array<BoundType, 6> bounds_outflow = {OUTFLOW_BOUND, OUTFLOW_BOUND,
                                                   OUTFLOW_BOUND, OUTFLOW_BOUND,
                                                   OUTFLOW_BOUND, OUTFLOW_BOUND};
        std::vector<GeneralParticleInfo> general_info;
        GeneralParticleInfo info = {.sigma=1.0, .epsilon=5.0, .mass=1.0};
        general_info.push_back(info);

        glm::dvec3 center = glm::dvec3(10.0, 30.0, 0.0);
        glm::dvec3 domain_size = glm::dvec3(12.0, 12.0, 6.0);
        double cut_off = 6.0;

        // create new cellcontainer
        cont = std::make_shared<CellContainer>(
                *m_particles, general_info,
                center, domain_size,
                bounds_outflow, cut_off, 10);

        //create Single Part Iterator
        it = cont->createSPIterator();

    }

    void TearDown() override {}

    std::shared_ptr<std::vector<Particle>> m_particles;
    std::shared_ptr<CellContainer> cont;
    std::shared_ptr<CellContainer::SPIteratorCell> it;
};



/**
 * test Thermostat with no initial velocities
 */
TEST_F(ThermostatTest, test_zero_velocities)
{
    //initialize variables
    std::shared_ptr<StandardThermo> t = std::make_shared<StandardThermo>(cont.get(), 2, 25.0, true);
    glm::dvec3 zeroV = {0, 0, 0};

    //Test no initial velocities
    ASSERT_EQ(cont->size(), 3);
    EXPECT_TRUE(it->deref()->getV() != zeroV);

    it->increment();
    EXPECT_TRUE(it->deref()->getV() != zeroV);

    it->increment();
    EXPECT_TRUE(it->deref()->getV() != zeroV);

    it->increment();
    ASSERT_EQ(it->isEnd(), true);
}


/**
 * test Thermostat with initial velocities
 */
TEST_F(ThermostatTest, test_velocities)
{
    //initialize variables
    std::shared_ptr<CellContainer::SPIteratorCell> it2 = cont->createSPIterator();
    glm::dvec3 oneV = {1, 1, 1};
    glm::dvec3 zeroV = {0, 0, 0};
    it2->deref()->setV(oneV);
    it2->increment();
    it2->deref()->setV(oneV);
    std::shared_ptr<StandardThermo> t = std::make_shared<StandardThermo>(cont.get(), 2, 25.0, false);

    //Test initial velocities
    ASSERT_EQ(cont->size(), 3);
    EXPECT_TRUE(it->deref()->getV() == oneV);

    it->increment();
    EXPECT_TRUE(it->deref()->getV() == oneV);

    it->increment();
    EXPECT_TRUE(it->deref()->getV() == zeroV);

    it->increment();
    ASSERT_EQ(it->isEnd(), true);
}


/**
 * test getTemperature()
 */
TEST_F(ThermostatTest, test_getTemperature)
{
    //initialize variables
    std::shared_ptr<StandardThermo> t = std::make_shared<StandardThermo>(cont.get(), 2, 25.0, false);
    std::shared_ptr<CellContainer::SPIteratorCell> it2 = cont->createSPIterator();
    while(!it2->isEnd()) {
        it2->deref()->setV({1, 2, 3});
        it2->increment();
    }

    //Test current temperature
    EXPECT_TRUE(t->getTemperature() == 7);
}


/**
 * test setTemperature()
 */
TEST_F(ThermostatTest, test_setTemperature)
{
    //initialize variables
    std::shared_ptr<StandardThermo> t = std::make_shared<StandardThermo>(cont.get(), 2, 25.0, false);
    std::shared_ptr<CellContainer::SPIteratorCell> it2 = cont->createSPIterator();
    while(!it2->isEnd()) {
        it2->deref()->setV({1, 2, 3});
        it2->increment();
    }

    //test positive gradual scaling
    t->setTemperature(true, 0.0005, 25);
    EXPECT_DOUBLE_EQ(t->getTemperature(), 7.0005);

    //test negative gradual scaling
    t->setTemperature(true, 0.5, 6);
    EXPECT_DOUBLE_EQ(t->getTemperature(), 6.5005);

    //test positive direct scaling
    t->setTemperature(false, 0.5, 10);
    EXPECT_DOUBLE_EQ(t->getTemperature(), 10);

    //test negative direct scaling
    t->setTemperature(false, 0.5, 2);
    EXPECT_DOUBLE_EQ(t->getTemperature(), 2);

    //test keeping temperature
    t->setTemperature(true, 0.5, 2);
    EXPECT_DOUBLE_EQ(t->getTemperature(), 2);
}
