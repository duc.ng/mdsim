#include <gtest/gtest.h>

#include "Calc/CellCalculator.h"

class CellCalculatorPeriodTest : public ::testing::Test
{
protected:
    void SetUp() override
    {
        // cell grid is 3x3x3 around origin (cutoff radius 3)
        glm::dvec3 center = glm::dvec3(0.0, 0.0, 0.0);
        glm::dvec3 domain_size = glm::dvec3(9.0, 9.0, 9.0);
        double cut_off = 3.0;

        // bounds are periodic except for y-axis boundaries which are outflow
        std::array<BoundType, 6> bounds = { PERIODIC_BOUND, PERIODIC_BOUND,
                                            OUTFLOW_BOUND, OUTFLOW_BOUND,
                                            PERIODIC_BOUND, PERIODIC_BOUND };
        // dummy particle type
        std::vector<GeneralParticleInfo> general_info;
        GeneralParticleInfo info = {.sigma=1.0, .epsilon=5.0, .mass=1.0};
        general_info.push_back(info);


        /*
         * Particle vector has 8 particles
         * -> 4 in upper corner cells
         * -> 4 in lower corner cells (one main)
         * => main particle in lower front right corner cell is in reach of all other lower particles
         *    -> all other particles aren't in reach of any other particle than main part (upper not at all)
         */
        m_particles = std::make_shared<std::vector<Particle>>();
        glm::dvec3 x0 = { -2.9, -3.0, -2.9 };
        glm::dvec3 v0 = {  0.0,  0.0,  0.0 };
        m_particles->emplace_back(x0, v0, 0, 0);
        glm::dvec3 x1 = { -2.9, -3.0,  2.9 };
        glm::dvec3 v1 = {  0.0,  0.0,  0.0 };
        m_particles->emplace_back(x1, v1, 0, 1);

            glm::dvec3 x_main = {  4.4, -3.0, -4.4 };
            glm::dvec3 v_main = {  10.0,  0.0,  0.0 };
            m_particles->emplace_back(x_main, v_main, 0, 3);

        glm::dvec3 x3 = {  2.9, -3.0,  2.9 };
        glm::dvec3 v3 = {  0.0,  0.0,  0.0 };
        m_particles->emplace_back(x3, v3, 0, 2);


        glm::dvec3 x4 = { -2.9,  3.0, -2.9 };
        glm::dvec3 v4 = {  0.0,  0.0,  0.0 };
        m_particles->emplace_back(x4, v4, 0, 4);
        glm::dvec3 x5 = { -2.9,  3.0,  2.9 };
        glm::dvec3 v5 = {  0.0,  0.0,  0.0 };
        m_particles->emplace_back(x5, v5, 0, 5);
        glm::dvec3 x6 = {  2.9,  3.0, -2.9 };
        glm::dvec3 v6 = {  0.0,  0.0,  0.0 };
        m_particles->emplace_back(x6, v6, 0, 6);
        glm::dvec3 x7 = {  2.9,  3.0,  2.9 };
        glm::dvec3 v7 = {  0.0,  0.0,  0.0 };
        m_particles->emplace_back(x7, v7, 0, 7);

        // create cellcontainer
        m_con_period = std::make_shared<CellContainer>(
                *m_particles, general_info,
                center, domain_size,
                bounds, cut_off, 10);

        // create cellcalculator
        m_delta_t = 0.01;
        m_cell_calc = std::make_shared<CellCalculator>(m_con_period.get(), m_delta_t, false, 0, 0);
    }

    void TearDown() override
    {
    }

    std::shared_ptr<CellContainer>          m_con_period;
    std::shared_ptr<std::vector<Particle>>  m_particles;
    std::shared_ptr<CellCalculator>         m_cell_calc;
    double                                  m_delta_t;
};

/**
 * test if iterating and moving particles with periodic boundaries works
 */
TEST_F(CellCalculatorPeriodTest, test_period)
{
    // tests if main particle (id=3) reaches other 3 lower particles
    // and all other particles don't interact with each other
    auto np_it = m_con_period->createNPIterator();
    for(int i=0; i<3; i++) {
        if(np_it->isEnd())
            FAIL();
        auto pair = np_it->deref();
        EXPECT_TRUE(pair.first->getID() <= 3 && pair.second->getID() <= 3);
        EXPECT_TRUE(pair.first->getID() == 3 || pair.second->getID() == 3);
        np_it->increment();
    }
    ASSERT_TRUE(np_it->isEnd());


    // performs some calculate steps to move main particle to other side via periodic wraparound
    m_cell_calc->calculateF();
    for(int i=0; i<8; i++) {
        m_cell_calc->calculatePos();
        m_cell_calc->calculateF();
        m_cell_calc->calculateV();
    }

    // tests if main particle (id=3) is actually in right corner cell
    auto sp_it = m_con_period->createSPIterator();
    while(sp_it->isEnd()) {
        Particle *p = sp_it->deref();
        if(p->getID() == 3) {
            glm::dvec3 pos = p->getPos();
            EXPECT_TRUE(pos.x < -1.5);
            EXPECT_TRUE(pos.y < -1.5);
            EXPECT_TRUE(pos.z < -1.5);
            break;
        }
        sp_it->increment();
    }
    ASSERT_TRUE(!sp_it->isEnd());
}

