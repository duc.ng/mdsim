#include <gtest/gtest.h>

#include "Calc/CellCalculator.h"
#include "Calc/PCellCalculator.h"

class CellCalculatorCompNoIter : public ::testing::Test
{
protected:
    void SetUp() override
    {
        // generic cell grid (particles should not exceed this)
        glm::dvec3 center = glm::dvec3(3.0, 4.0, 0.0);
        glm::dvec3 domain_size = glm::dvec3(4.0, 4.0, 1.0);
        double cut_off = 1.0;

        // dummy particle type
        std::vector<GeneralParticleInfo> general_info;
        GeneralParticleInfo info = {.sigma=0.1, .epsilon=0.2, .mass=0.1};
        general_info.push_back(info);

        // only outflow boundaries for others are tested separately and would confuse basic calculator
        std::array<BoundType, 6> bounds_outflow = { PERIODIC_BOUND, PERIODIC_BOUND,
                                                    REFLECT_BOUND, REFLECT_BOUND,
                                                    REFLECT_BOUND, REFLECT_BOUND };

        // fill particle list with some particles
        m_particles = std::make_shared<std::vector<Particle>>();
        m_particles->reserve(10);
        glm::dvec3 v = { 1.0, 1.0, 0.0};

        glm::dvec3 x0 = { 1.2, 2.2, 0.0 };
        m_particles->emplace_back(x0, v, 0, 0);
        glm::dvec3 x1 = { 1.5, 2.5, 0.0 };
        m_particles->emplace_back(x1, v, 0, 1);

        glm::dvec3 x2 = { 4.5, 2.1, 0.0 };
        m_particles->emplace_back(x2, v, 0, 2);

        glm::dvec3 x3 = { 2.1, 3.1, 0.0 };
        m_particles->emplace_back(x3, v, 0, 3);
        glm::dvec3 x4 = { 2.2, 3.9, 0.0 };
        m_particles->emplace_back(x4, v, 0, 4);
        glm::dvec3 x5 = { 2.1, 3.8, 0.0 };
        m_particles->emplace_back(x5, v, 0, 5);

        glm::dvec3 x6 = { 4.2, 3.7, 0.0 };
        m_particles->emplace_back(x6, v, 0, 6);

        glm::dvec3 x7 = { 1.6, 4.5, 0.0 };
        m_particles->emplace_back(x7, v, 0, 7);

        glm::dvec3 x8 = { 4.2, 5.7, 0.0 };
        m_particles->emplace_back(x8, v, 0, 8);
        glm::dvec3 x9 = { 4.8, 5.2, 0.0 };
        m_particles->emplace_back(x9, v, 0, 9);


        // use small delta_t
        m_delta_t = 0.0001;

        ///////////////////
        // CellContainer //
        ///////////////////
        // create m_con_ref
        m_con_ref = std::make_shared<CellContainer>(
                    *m_particles, general_info,
                    center, domain_size,
                    bounds_outflow, cut_off, 10);
        // create cell calculator
        m_calc_ref = std::make_shared<CellCalculator>(m_con_ref.get(), m_delta_t, false, 0, 0);

        // create m_con_testee
        m_con_testee = std::make_shared<ParallelCellCont>(
                *m_particles, general_info,
                center, domain_size,
                bounds_outflow, cut_off, 10);
        // create cell calculator no iter
        m_calc_testee = std::make_shared<PCellCalculator>(
                m_con_testee.get(), m_delta_t, false, 0, 0);
    }

    void TearDown() override
    {
    }

    void compareAllParticles();

    std::shared_ptr<std::vector<Particle>>  m_particles;
    double                                  m_delta_t;

    std::shared_ptr<CellContainer>          m_con_ref;
    std::shared_ptr<CellCalculator>         m_calc_ref;
    std::shared_ptr<ParallelCellCont>       m_con_testee;
    std::shared_ptr<PCellCalculator>   m_calc_testee;
};




/**
 * test if cellcalculator actually calculates Lennard-Jones-Potential correctly with small absolute error
 * Idea: compare to same operation done by old molecularcalculator
 */
TEST_F(CellCalculatorCompNoIter, test_compare_no_iter)
{
    // compare directly at start and after init force calculate
    compareAllParticles();
    m_calc_ref->calculateF();
    m_calc_testee->calculateF();
    compareAllParticles();

    // do some calculate steps and compare after every single step (pos, force, velocity)
    for(int i=0; i<1000; i++) {
        // position
        m_calc_ref->calculatePos();
        m_calc_testee->calculatePos();
        compareAllParticles();

        // force
        m_calc_ref->calculateF();
        m_calc_testee->calculateF();
        compareAllParticles();

        // velocity
        m_calc_ref->calculateV();
        m_calc_testee->calculateV();
        compareAllParticles();
    }
}

// check if all particles are equal in position, force and velocity (with small absolute error)
// -> here quite high, but only because of += and + arithmetic difference
#define ABS_ERR 1e-9
void CellCalculatorCompNoIter::compareAllParticles()
{
    auto it_calc = m_con_ref->createSPIterator();
    while(!it_calc->isEnd()) {
        Particle *p_cell_calc = it_calc->deref();
        auto it_no_iter  = m_con_testee->createSPIterator();
        while(p_cell_calc->getID() != it_no_iter->deref()->getID()) {
            if(it_no_iter->isEnd())
                throw std::runtime_error("error: cell_cont contains less elements than basic_cont");
            it_no_iter->increment();
        }
        Particle *p_testee = it_no_iter->deref();

        // positions
        ASSERT_NEAR(p_cell_calc->getPos().x, p_testee->getPos().x, ABS_ERR);
        ASSERT_NEAR(p_cell_calc->getPos().y, p_testee->getPos().y, ABS_ERR);
        ASSERT_NEAR(p_cell_calc->getPos().z, p_testee->getPos().z, ABS_ERR);

        // forces
        ASSERT_NEAR(p_cell_calc->getF().x, p_testee->getF().x, ABS_ERR);
        ASSERT_NEAR(p_cell_calc->getF().y, p_testee->getF().y, ABS_ERR);
        ASSERT_NEAR(p_cell_calc->getF().z, p_testee->getF().z, ABS_ERR);

        // velocities
        ASSERT_NEAR(p_cell_calc->getV().x, p_testee->getV().x, ABS_ERR);
        ASSERT_NEAR(p_cell_calc->getV().y, p_testee->getV().y, ABS_ERR);
        ASSERT_NEAR(p_cell_calc->getV().z, p_testee->getV().z, ABS_ERR);

        it_calc->increment();
    }
}
