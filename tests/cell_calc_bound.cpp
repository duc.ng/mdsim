#include <gtest/gtest.h>

#include "Calc/CellCalculator.h"

class CellCalculatorBoundTest : public ::testing::Test
{
protected:
    void SetUp() override
    {
        // cell grid is 1x1x1 with some random shift of center (cutoff radius 3)
        glm::dvec3 center = glm::dvec3(3.5, 4.5, 0.0);
        glm::dvec3 domain_size = glm::dvec3(3.0, 3.0, 3.0);
        double cut_off = 3.0;
        // dummy particle type
        std::vector<GeneralParticleInfo> general_info;
        GeneralParticleInfo info = {.sigma=1.0, .epsilon=5.0, .mass=1.0};
        general_info.push_back(info);


        // fill particle list with one test particle
        m_particles = std::make_shared<std::vector<Particle>>();
        glm::dvec3 x0 = {  3.5,  4.5,  0.0 };
        glm::dvec3 v0 = { -1.0,  1.0,  0.0 };
        p0 = std::make_unique<Particle>(x0, v0, 0, 0);
        m_particles->push_back(*p0);

        // create cellcontainer with all outflow boundaries
        std::array<BoundType, 6> bounds_outflow = { OUTFLOW_BOUND, OUTFLOW_BOUND,
                                                    OUTFLOW_BOUND, OUTFLOW_BOUND,
                                                    OUTFLOW_BOUND, OUTFLOW_BOUND };
        m_con_outflow = std::make_shared<CellContainer>(
                *m_particles, general_info,
                center, domain_size,
                bounds_outflow, cut_off, 10);
        // create m_cell_calc_outflow with big delta_t
        m_delta_t_outf = 2.0;
        m_cell_calc_outflow = std::make_shared<CellCalculator>(m_con_outflow.get(), m_delta_t_outf, false, 0, 0);

        // create cellcontainer with all reflecting boundaries
        std::array<BoundType, 6> bounds_reflect = { REFLECT_BOUND, REFLECT_BOUND,
                                                    REFLECT_BOUND, REFLECT_BOUND,
                                                    REFLECT_BOUND, REFLECT_BOUND };
        m_con_reflect = std::make_shared<CellContainer>(
                *m_particles, general_info,
                center, domain_size,
                bounds_reflect, cut_off, 10);

        // create m_cell_calc_refl with smaller delta_t
        m_delta_t_refl = 0.001;
        m_cell_calc_reflect = std::make_shared<CellCalculator>(m_con_reflect.get(), m_delta_t_refl, false, 0, 0);
    }

    void TearDown() override
    {
    }

    std::shared_ptr<CellContainer>          m_con_outflow, m_con_reflect;
    std::shared_ptr<std::vector<Particle>>  m_particles;
    std::unique_ptr<Particle>               p0;
    std::shared_ptr<CellCalculator>         m_cell_calc_outflow, m_cell_calc_reflect;
    double                                  m_delta_t_outf, m_delta_t_refl;
};

/**
 * test if outflow boundary works (particles get deleted)
 */
TEST_F(CellCalculatorBoundTest, test_outflow)
{
    // perform one calculate step to move particle beyond cell grid limits (works because of big delta_t)
    m_cell_calc_outflow->calculateF();

    m_cell_calc_outflow->calculatePos();
    m_cell_calc_outflow->calculateF();
    m_cell_calc_outflow->calculateV();

    // tests if container is empty now (particle was deleted)
    auto it = m_con_outflow->createSPIterator();
    ASSERT_TRUE(it->isEnd());

    // and it doesn't come back
    m_cell_calc_outflow->calculatePos();
    it = m_con_outflow->createSPIterator();
    ASSERT_TRUE(it->isEnd());
}


/**
 * test if particle gets reflected with reflecting boundaries
 */
static bool sgn(double v)
{
    return (v >= 0);
}
#define ABS_ERR 1e-7
TEST_F(CellCalculatorBoundTest, test_reflect)
{
    m_cell_calc_reflect->calculateF();
    bool switched = false;
    glm::dvec3 old_v = p0->getV();

    // perform some calculate steps to demonstrate reflecting
    // -> needs some steps so particle doesn't accidentally exceed cell grid
    for(int i=0; i<1000; i++) {
        m_cell_calc_reflect->calculatePos();
        m_cell_calc_reflect->calculateF();
        m_cell_calc_reflect->calculateV();

        // look if particle changes sign on x and y value of its velocity
        auto it = m_con_reflect->createSPIterator();
        glm::dvec3 new_v = it->deref()->getV();
        if(!sgn(new_v.x) == sgn(old_v.x) && !sgn(new_v.y) == sgn(old_v.y)) {
            // -> checks that it doesn't change sign more than once
            if(switched)
                FAIL();
            switched = true;

            // the ratio of the new reflected velocity should now be about the same as at start
            double v0_diff = p0->getV().x / p0->getV().y;
            EXPECT_NEAR(new_v.x / new_v.y, v0_diff, ABS_ERR);

            it->increment();
        }
        old_v = new_v;
    }
    // -> checks that it has at least once changed sign
    if(!switched)
        FAIL();
}