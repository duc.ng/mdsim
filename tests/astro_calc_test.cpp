#include <gtest/gtest.h>

#include "Calc/AstroCalculator.h"


class AstroCalculatorTest : public ::testing::Test
{
protected:
    void SetUp() override
    {
        // fill particle list with some particles
        m_particles = std::make_shared<std::vector<Particle>>();

        glm::dvec3 x0 = { 0.0, 0.0, 0.0 };
        glm::dvec3 v0 = { 0.0, 0.0, 0.0 };
        m_particles->emplace_back(x0, v0, 0, 0);

        glm::dvec3 x1 = { 1.0f, 0.0f, 0.0f };
        glm::dvec3 v1 = { -1.0f, 0.0f, 0.0f };
        m_particles->emplace_back(x1, v1, 1, 1);

        glm::dvec3 x2 = { 0.0, 5.36, 0.0 };
        glm::dvec3 v2 = { -0.425, 0.0, 0.0 };
        m_particles->emplace_back(x2, v2, 2, 2);


        // different masses for each particle
        std::vector<GeneralParticleInfo> general_info;
        GeneralParticleInfo info0 = {.sigma=1.0, .epsilon=5.0, .mass=1.0};
        general_info.push_back(info0);
        GeneralParticleInfo info1 = {.sigma=1.0, .epsilon=5.0, .mass=2.0};
        general_info.push_back(info1);
        GeneralParticleInfo info2 = {.sigma=1.0, .epsilon=5.0, .mass=3.0};
        general_info.push_back(info2);

        // create new basiccontainer
        m_con = std::make_shared<BasicContainer>(*m_particles, general_info);

        //create new AstroCalculator
        m_astro_calc = std::make_shared<AstroCalculator>(m_con.get(), 0.1);
    }

    void TearDown() override
    {
    }

    std::shared_ptr<BasicContainer>         m_con;
    std::shared_ptr<std::vector<Particle>>  m_particles;
    std::shared_ptr<AstroCalculator>        m_astro_calc;
};


/**
 * test if astrocalculator calculates force correctly
 * Idea: Check correctness of force, velocity and coordinates calculation against hand-calculated value
 */
TEST_F(AstroCalculatorTest, test_calculateF)
{
    //calculate forces
    m_astro_calc->calculateF();

    //save forces
    std::vector<glm::dvec3> forces;
    auto sp_iter = m_con->createSPIterator();
    while(!sp_iter->isEnd()){
        const glm::dvec3 f = sp_iter->deref()->getF();
        forces.emplace_back(f);
        sp_iter->increment();
    }

    //test force 1
    glm::dvec3 testF1 = {2, 0.104421920249498774782802405881042548451770995767431499220, 0.0 };
    EXPECT_DOUBLE_EQ(testF1[0],forces.at(0)[0]);
    EXPECT_DOUBLE_EQ(testF1[1],forces.at(0)[1]);
    EXPECT_DOUBLE_EQ(testF1[2],forces.at(0)[2]);

    //test force 2
    glm::dvec3 testF2 = {-2.037014138988205005087351364631732474611715317892442, 0.1983957849767788272682033144260860639187941039034897, 0.0 };
    EXPECT_DOUBLE_EQ(testF2[0],forces.at(1)[0]);
    EXPECT_DOUBLE_EQ(testF2[1],forces.at(1)[1]);
    EXPECT_DOUBLE_EQ(testF2[2],forces.at(1)[2]);

    //test force 3
    glm::dvec3 testF3 = {0.037014138988205005087351364631732474611715317892442, -0.30281770522627760205100572030712861237056509967092119922, 0.0 };
    EXPECT_DOUBLE_EQ(testF3[0],forces.at(2)[0]);
    EXPECT_DOUBLE_EQ(testF3[1],forces.at(2)[1]);
    EXPECT_DOUBLE_EQ(testF3[2],forces.at(2)[2]);

}


/**
 * test if astrocalculator calculates position correctly
 * Idea: Check correctness of force, velocity and coordinates calculation against hand-calculated value
 */
TEST_F(AstroCalculatorTest, test_calculateX)
{
    //calculate coordinates
    m_astro_calc->calculateF();
    m_astro_calc->calculatePos();

    //get coordinates
    std::vector<glm::dvec3> coord;
    auto sp_iter = m_con->createSPIterator();
    while(!sp_iter->isEnd()){
        const glm::dvec3 x = sp_iter->deref()->getPos();
        coord.emplace_back(x);
        sp_iter->increment();
    }

    //test coordinate 1
    glm::dvec3 testX1 = {0.01, 0.000522109601247493873914012029405212742258854978837157496, 0.0 };
    EXPECT_DOUBLE_EQ(testX1[0],coord.at(0)[0]);
    EXPECT_DOUBLE_EQ(testX1[1],coord.at(0)[1]);
    EXPECT_DOUBLE_EQ(testX1[2],coord.at(0)[2]);

    //test coordinate 2
    glm::dvec3 testX2 = {0.894907464652529487487281621588420668813470711705268895, 0.00049598946244194706817050828606521515979698525975872425, 0.0 };
    EXPECT_DOUBLE_EQ(testX2[0],coord.at(1)[0]);
    EXPECT_DOUBLE_EQ(testX2[1],coord.at(1)[1]);
    EXPECT_DOUBLE_EQ(testX2[2],coord.at(1)[2]);

    //test coordinate 3
    glm::dvec3 testX3 = {-0.04243830976835299165818774772561377920898047447017926333,5.359495303824622870663248323799488118979382391500548464667 , 0.0 };
    EXPECT_DOUBLE_EQ(testX3[0],coord.at(2)[0]);
    EXPECT_DOUBLE_EQ(testX3[1],coord.at(2)[1]);
    EXPECT_DOUBLE_EQ(testX3[2],coord.at(2)[2]);

}


/**
 * test if astrocalculator calculates velocity correctly
 * Idea: Check correctness of force, velocity and coordinates calculation against hand-calculated value
 */
TEST_F(AstroCalculatorTest, test_calculateV)
{
    //calculate velocities
    m_astro_calc->calculateF();
    m_astro_calc->calculatePos();
    m_astro_calc->calculateF();
    m_astro_calc->calculateV();

    //get velocities
    std::vector<glm::dvec3> velo;
    auto sp_iter = m_con->createSPIterator();
    while(!sp_iter->isEnd()){
        const glm::dvec3 v = sp_iter->deref()->getV();
        velo.emplace_back(v);
        sp_iter->increment();
    }

    //test velocity 1
    glm::dvec3 testV1 = {0.22765283436648598, 0.010439673438668964 ,0 };
    EXPECT_DOUBLE_EQ(testV1[0],velo.at(0)[0]);
    EXPECT_DOUBLE_EQ(testV1[1],velo.at(0)[1]);
    EXPECT_DOUBLE_EQ(testV1[2],velo.at(0)[2]);

    //test velocity 2
    glm::dvec3 testV2 = {-1.1156505113415083,  0.0099539867724788376, 0 };
    EXPECT_DOUBLE_EQ(testV2[0],velo.at(1)[0]);
    EXPECT_DOUBLE_EQ(testV2[1],velo.at(1)[1]);
    EXPECT_DOUBLE_EQ(testV2[2],velo.at(1)[2]);

    //test velocity 3
    glm::dvec3 testV3 = {-0.42378393722782315,-0.010115882327875546, 0.0 };
    EXPECT_DOUBLE_EQ(testV3[0],velo.at(2)[0]);
    EXPECT_DOUBLE_EQ(testV3[1],velo.at(2)[1]);
    EXPECT_DOUBLE_EQ(testV3[2],velo.at(2)[2]);
}


