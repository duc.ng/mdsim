#include <gtest/gtest.h>

#include "Container/BasicContainer.h"

class BasicContainerTest : public ::testing::Test
{
protected:
    void SetUp() override
    {
        // fill particle list with some particles
        std::vector<Particle> particles;
        glm::dvec3 x0 = { 0.0, 0.0, 0.0 };
        glm::dvec3 v0 = { 0.0, 0.0, 0.0 };
        p0 = std::make_unique<Particle>(x0, v0, 0, 0);
        particles.push_back(*p0);

        glm::dvec3 x1 = { 1.0f, 0.0f, 0.0f };
        glm::dvec3 v1 = { -1.0f, 0.0f, 0.0f };
        double m1 = 3.0e-6;
        p1 = std::make_unique<Particle>(x1, v1, 0, 1);
        particles.push_back(*p1);

        glm::dvec3 x2 = { 0.0, 5.36, 0.0 };
        glm::dvec3 v2 = { -0.425, 0.0, 0.0 };
        double m2 = 9.55e-4;
        p2 = std::make_unique<Particle>(x2, v2, 0, 2);
        particles.push_back(*p2);

        // dummy particle type
        std::vector<GeneralParticleInfo> general_info;
        GeneralParticleInfo info = {.sigma=1.0, .epsilon=5.0, .mass=1.0};
        general_info.push_back(info);

        // create new BasicContainer
        m_con = std::make_unique<BasicContainer>(particles, general_info);

        // create iterators to test
        sp_iter = m_con->createSPIterator();
        np_iter = m_con->createNPIterator();
    }

    void TearDown() override
    {
    }

    std::unique_ptr<BasicContainer>     m_con;
    std::unique_ptr<Particle>           p0, p1, p2;
    std::unique_ptr<BasicContainer::SPIteratorBasic>    sp_iter;
    std::unique_ptr<BasicContainer::NPIteratorBasic>    np_iter;
};



/**
 * test the single particle iterator, that iterates through every single particle
 */
TEST_F(BasicContainerTest, test_SinglePartIter)
{
    ASSERT_EQ(sp_iter->isEnd(), false);
    EXPECT_TRUE(*sp_iter->deref() == *p0);

    sp_iter->increment();
    ASSERT_EQ(sp_iter->isEnd(), false);
    EXPECT_TRUE(*sp_iter->deref() == *p1);

    sp_iter->increment();
    ASSERT_EQ(sp_iter->isEnd(), false);
    EXPECT_TRUE(*sp_iter->deref() == *p2);

    sp_iter->increment();
    ASSERT_EQ(sp_iter->isEnd(), true);
}


/**
 * test the pair iterator optimized for MolSim (Newton's 3. Law)
 */
TEST_F(BasicContainerTest, test_NewtPairPartIter)
{
    ASSERT_EQ(np_iter->isEnd(), false);
    std::pair<Particle *, Particle *> pair = np_iter->deref();
    EXPECT_TRUE(*pair.first == *p0 && *pair.second == *p1);

    np_iter->increment();
    ASSERT_EQ(np_iter->isEnd(), false);
    pair = np_iter->deref();
    EXPECT_TRUE(*pair.first == *p0 && *pair.second == *p2);

    np_iter->increment();
    ASSERT_EQ(np_iter->isEnd(), false);
    pair = np_iter->deref();
    EXPECT_TRUE(*pair.first == *p1 && *pair.second == *p2);

    np_iter->increment();
    ASSERT_EQ(np_iter->isEnd(), true);
}

