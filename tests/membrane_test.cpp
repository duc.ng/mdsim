#include <gtest/gtest.h>

#include "Container/ParallelCellCont.h"
#include "Calc/MembraneCalculator.h"


class MembraneTest : public ::testing::Test
{
protected:
    void SetUp() override
    {
        // only outflow boundaries (we're mainly testing iterators)
        std::array<BoundType, 6> bounds = {OUTFLOW_BOUND, OUTFLOW_BOUND,
                                           OUTFLOW_BOUND, OUTFLOW_BOUND,
                                           OUTFLOW_BOUND, OUTFLOW_BOUND};
        // dummy particle type
        std::vector<GeneralParticleInfo> general_info;
        GeneralParticleInfo info = {.sigma=1.0, .epsilon=5.0, .mass=1.0};
        general_info.push_back(info);


        // fill particle list with some particles
        // place in right order
        m_particles = std::make_shared<std::vector<Particle>>();

        glm::dvec3 x0 = {1.0, 2.0, 0.0};
        m_particles->emplace_back(x0, glm::dvec3(0.0), 0, 0);
        glm::dvec3 x1 = {1.5, 2.5, 0.0};
        m_particles->emplace_back(x1, glm::dvec3(0.0), 0, 1);

        glm::dvec3 x2 = {4.5, 2.1, 0.0};
        m_particles->emplace_back(x2, glm::dvec3(0.0), 0, 2);

        glm::dvec3 x3 = {2.1, 3.1, 0.0};
        m_particles->emplace_back(x3, glm::dvec3(0.0), 0, 3);
        glm::dvec3 x4 = {2.0, 3.9, 0.0};
        m_particles->emplace_back(x4, glm::dvec3(0.0), 0, 1217);
        glm::dvec3 x5 = {2.1, 3.8, 0.0};
        m_particles->emplace_back(x5, glm::dvec3(0.0), 0, 1218);

        glm::dvec3 x6 = {4.2, 3.7, 0.0};
        m_particles->emplace_back(x6, glm::dvec3(0.0), 0, 1267);

        glm::dvec3 x7 = {1.6, 4.5, 0.0};
        m_particles->emplace_back(x7, glm::dvec3(0.0), 0, 1268);

        glm::dvec3 x8 = {4.2, 5.7, 0.0};
        m_particles->emplace_back(x8, glm::dvec3(0.0), 0, 8);
        glm::dvec3 x9 = {4.8, 5.2, 0.0};
        m_particles->emplace_back(x9, glm::dvec3(0.0), 0, 9);

        // create new CellContainer
        m_con = std::make_shared<ParallelCellCont>(
                *m_particles, general_info,
                glm::dvec3(3.0, 4.0, 0.0),
                glm::dvec3(4.0, 4.0, 1.0),
                bounds, 1.0, 10);

        // create Membrane Calculator
        m_partCalc = std::make_unique<MembraneCalculator>(m_con.get(),0.01,true, -0.001,2, 300,2.2,50);

    }

    void TearDown() override
    {
    }

    std::unique_ptr<MembraneCalculator> m_partCalc;
    std::shared_ptr<ParallelCellCont> m_con;
    std::shared_ptr<std::vector<Particle>> m_particles;
    std::unique_ptr<ParallelCellCont::SPIteratorParallel> sp_iter;

};


/**
 * test, if particles with coordinates (17/24),(17/25),(18/24),(18/25) are pulled up
 */
TEST_F(MembraneTest, test_fz_up)
{

    //Test fz_up
    auto it = m_con->createSPIterator();
    m_partCalc->calculateFZ(0.8);

    while(!it->isEnd()) {
        PPartVFID *partVFID = it->getPDeref().partVFID;
        if (partVFID->id == 1217 || partVFID->id == 1218 || partVFID->id == 1267 || partVFID->id == 1268) {
            ASSERT_TRUE(partVFID->old_f.z<partVFID->force.z);
        }
        it->increment();
    }
}


/**
 * test, harmonic potential by checking against random values
 */
TEST_F(MembraneTest, test_harmonicpotential)
{

    //calculate against random values
    glm::dvec3 pos1(1,2,3);
    glm::dvec3 pos2(3,5,4);
    glm::dvec3 res(924.99443206436467,1387.4916480965471,462.49721603218234);
    glm::dvec3 calc = m_partCalc->calcHarmonicP(1, 2, pos1,pos2, 2.2 , 300, 1, 1);
    EXPECT_DOUBLE_EQ(res.x,calc.x);
    EXPECT_DOUBLE_EQ(res.y,calc.y);
    EXPECT_DOUBLE_EQ(res.z,calc.z);

    //calculate against random values
    pos1=glm::dvec3(13,52,34);
    pos2=glm::dvec3(34,53,4);
    res=glm::dvec3 (216929.90445857891,10329.99545040852,-309899.8635122556);
    calc=m_partCalc->calcHarmonicP(1, 2, pos1,pos2, 2.2 , 300, 1, 1);
    std::cout << "saddas"<<calc.x<<std::endl;
    std::cout << "saddas"<<calc.y<<std::endl;
    std::cout << "saddas"<<calc.z<<std::endl;
    EXPECT_DOUBLE_EQ(res.x,calc.x);
    EXPECT_DOUBLE_EQ(res.y,calc.y);
    EXPECT_DOUBLE_EQ(res.z,calc.z);

    //calculate against random values
    pos1=glm::dvec3(3,2,4);
    pos2=glm::dvec3(334,1.3,4);
    res=glm::dvec3 (3264991.3499917821,-6904.8155437892674,0);
    calc=m_partCalc->calcHarmonicP(5, 6, pos1,pos2, 2.2 , 30, 1.3, 1);
    EXPECT_DOUBLE_EQ(res.x,calc.x);
    EXPECT_DOUBLE_EQ(res.y,calc.y);
    EXPECT_DOUBLE_EQ(res.z,calc.z);

}