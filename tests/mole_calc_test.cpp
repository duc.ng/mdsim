#include <gtest/gtest.h>

#include "Calc/MolecularCalculator.h"

class MolecularCalculatorTest : public ::testing::Test
{
protected:
    void SetUp() override
    {
        // dummy particle type
        std::vector<GeneralParticleInfo> general_info;
        GeneralParticleInfo info = {.sigma=1.0, .epsilon=5.0, .mass=1.0};
        general_info.push_back(info);

        // fill particle list with some particles
        m_particles = std::make_shared<std::vector<Particle>>();
        glm::dvec3 x0 = { 0.0, 0.0, 0.0 };
        glm::dvec3 v0 = { 0.0, 0.0, 0.0 };
        m_particles->emplace_back(x0, v0, 0, 0);

        glm::dvec3 x1 = { 1.0f, 0.0f, 0.0f };
        glm::dvec3 v1 = { -1.0f, 0.0f, 0.0f };
        m_particles->emplace_back(x1, v1, 0, 1);

        glm::dvec3 x2 = { 0.0, 5.36, 0.0 };
        glm::dvec3 v2 = { -0.425, 0.0, 0.0 };
        m_particles->emplace_back(x2, v2, 0, 2);


        // create new basicContainer
        m_con = std::make_shared<BasicContainer>(*m_particles, general_info);

        //create new molecularCalculator
        m_mole_calc = std::make_shared<MolecularCalculator>(m_con.get(), 0.1);

        //create new Single Part Iterator
        sp_iter = m_con->createSPIterator();
    }

    void TearDown() override
    {
    }

    std::shared_ptr<BasicContainer>         m_con;
    std::shared_ptr<std::vector<Particle>>  m_particles;
    std::shared_ptr<MolecularCalculator>    m_mole_calc;
    std::shared_ptr<BasicContainer::SPIteratorBasic> sp_iter;
};


/**
 * test if molecularcalculator computes Lennard-Jones-Potential correctly
 * Idea: check correctness of force against hand-calculated value
 */
TEST_F(MolecularCalculatorTest, test_calculateF)
{
    //calculate forces
    m_mole_calc->calculateF();

    //save forces
    std::vector<glm::dvec3> forces;
    while(!sp_iter->isEnd()){
        const glm::dvec3 f = sp_iter->deref()->getF();
        forces.push_back(f);
        sp_iter->increment();
    }

    //test force 1
    glm::dvec3 testF1 = { -120, 0.00094404150290674398, 0.0 };
    EXPECT_DOUBLE_EQ(testF1[0],forces.at(0)[0]);
    EXPECT_DOUBLE_EQ(testF1[1],forces.at(0)[1]);
    EXPECT_DOUBLE_EQ(testF1[2],forces.at(0)[2]);

    //test force 2
    glm::dvec3 testF2 = { 119.9998463997491, 0.00082329734481965294, 0.0 };
    EXPECT_DOUBLE_EQ(testF2[0],forces.at(1)[0]);
    EXPECT_DOUBLE_EQ(testF2[1],forces.at(1)[1]);
    EXPECT_DOUBLE_EQ(testF2[2],forces.at(1)[2]);

    //test force 3
    glm::dvec3 testF3 = { 0.00015360025089918898,  -0.0017673388477263969, 0.0 };
    EXPECT_DOUBLE_EQ(testF3[0],forces.at(2)[0]);
    EXPECT_DOUBLE_EQ(testF3[1],forces.at(2)[1]);
    EXPECT_DOUBLE_EQ(testF3[2],forces.at(2)[2]);
}
