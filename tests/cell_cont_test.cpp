#include <gtest/gtest.h>

#include "Container/CellContainer.h"


class CellContainerTest : public ::testing::Test
{
protected:
    void SetUp() override
    {
        // only outflow boundaries (we're mainly testing iterators)
        std::array<BoundType, 6> bounds = { OUTFLOW_BOUND, OUTFLOW_BOUND,
                                            OUTFLOW_BOUND, OUTFLOW_BOUND,
                                            OUTFLOW_BOUND, OUTFLOW_BOUND };
        // dummy particle type
        std::vector<GeneralParticleInfo> general_info;
        GeneralParticleInfo info = {.sigma=1.0, .epsilon=5.0, .mass=1.0};
        general_info.push_back(info);


        // fill particle list with some particles
        // place in right order
        m_particles = std::make_shared<std::vector<Particle>>();

        glm::dvec3 x0 = { 1.0, 2.0, 0.0 };
        m_particles->emplace_back(x0, glm::dvec3(0.0), 0, 0);
        glm::dvec3 x1 = { 1.5, 2.5, 0.0 };
        m_particles->emplace_back(x1, glm::dvec3(0.0), 0, 1);

        glm::dvec3 x2 = { 4.5, 2.1, 0.0 };
        m_particles->emplace_back(x2, glm::dvec3(0.0), 0, 2);

        glm::dvec3 x3 = { 2.1, 3.1, 0.0 };
        m_particles->emplace_back(x3, glm::dvec3(0.0), 0, 3);
        glm::dvec3 x4 = { 2.0, 3.9, 0.0 };
        m_particles->emplace_back(x4, glm::dvec3(0.0), 0, 4);
        glm::dvec3 x5 = { 2.1, 3.8, 0.0 };
        m_particles->emplace_back(x5, glm::dvec3(0.0), 0, 5);

        glm::dvec3 x6 = { 4.2, 3.7, 0.0 };
        m_particles->emplace_back(x6, glm::dvec3(0.0), 0, 6);

        glm::dvec3 x7 = { 1.6, 4.5, 0.0 };
        m_particles->emplace_back(x7, glm::dvec3(0.0), 0, 7);

        glm::dvec3 x8 = { 4.2, 5.7, 0.0 };
        m_particles->emplace_back(x8, glm::dvec3(0.0), 0, 8);
        glm::dvec3 x9 = { 4.8, 5.2, 0.0 };
        m_particles->emplace_back(x9, glm::dvec3(0.0), 0, 9);

        // create new CellContainer
        m_con = std::make_shared<CellContainer>(
                *m_particles, general_info,
                glm::dvec3(3.0, 4.0, 0.0),
                glm::dvec3(4.0, 4.0, 1.0),
                bounds, 1.0, 10);

        // create iterators
        sp_iter = m_con->createSPIterator();
        np_iter = m_con->createNPIterator();
    }

    void TearDown() override
    {
    }

    std::shared_ptr<CellContainer>                  m_con;
    std::shared_ptr<std::vector<Particle>>          m_particles;
    std::unique_ptr<CellContainer::SPIteratorCell>  sp_iter;
    std::unique_ptr<CellContainer::NPIteratorCell>  np_iter;
};

/**
 * test if single part iterator iterates correctly
 */
TEST_F(CellContainerTest, test_SinglePartIter)
{
    // iterates through whole container and compares it to particles in vector
    // -> order in particle vector is important
    ASSERT_EQ(sp_iter->isEnd(), false);
    EXPECT_TRUE(*sp_iter->deref() == (*m_particles)[0]);

    for(int i=1; i < m_particles->size(); i++) {
        sp_iter->increment();
        ASSERT_EQ(sp_iter->isEnd(), false);

        ASSERT_TRUE(*sp_iter->deref() == (*m_particles)[i]);
    }

    sp_iter->increment();
    ASSERT_EQ(sp_iter->isEnd(), true);
}


/**
 * test if newt pair iterator iterates correctly
 */
TEST_F(CellContainerTest, test_NewtPairPartIter)
{
    // expected result pairs (in indices)
    std::pair<int, int> result_inds[] = {
            {0,1},
            {1,3},
            {3,4}, {3,5},
            {4,5}, {4,7},
            {5,7},
            {8,9}
    };
    /*std::pair<int, int> result_inds[] = {
            {0,1}, {0,3}, {0,4}, {0,5},
            {1,3}, {1,4}, {1,5},
            {2,6},
            {3,4}, {3,5}, {3,7},
            {4,5}, {4,7},
            {5,7},
            {8,9}
    };*/

    // iterates through whole container and compares it to expected indexed particle pairs
    ASSERT_EQ(np_iter->isEnd(), false);
    auto pair = np_iter->deref();
    EXPECT_TRUE(*pair.first  == (*m_particles)[result_inds[0].first] &&
                *pair.second == (*m_particles)[result_inds[0].second]);

    for(int i=1; i<sizeof(result_inds)/sizeof(result_inds[0]); i++) {
        np_iter->increment();
        ASSERT_EQ(np_iter->isEnd(), false);
        pair = np_iter->deref();
        EXPECT_TRUE(*pair.first  == (*m_particles)[result_inds[i].first] &&
                    *pair.second == (*m_particles)[result_inds[i].second]);
    }

    np_iter->increment();
    ASSERT_EQ(np_iter->isEnd(), true);


    // reset and do it again
    np_iter->resetIterator();
    // iterates through whole container and compares it to expected indexed particle pairs
    ASSERT_EQ(np_iter->isEnd(), false);
    pair = np_iter->deref();
    EXPECT_TRUE(*pair.first  == (*m_particles)[result_inds[0].first] &&
                *pair.second == (*m_particles)[result_inds[0].second]);

    for(int i=1; i<sizeof(result_inds)/sizeof(result_inds[0]); i++) {
        np_iter->increment();
        ASSERT_EQ(np_iter->isEnd(), false);
        pair = np_iter->deref();
        EXPECT_TRUE(*pair.first  == (*m_particles)[result_inds[i].first] &&
                    *pair.second == (*m_particles)[result_inds[i].second]);
    }

    np_iter->increment();
    ASSERT_EQ(np_iter->isEnd(), true);

}