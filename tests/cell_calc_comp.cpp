#include <gtest/gtest.h>

#include "Calc/CellCalculator.h"
#include "Calc/MolecularCalculator.h"

class CellCalculatorCompTest : public ::testing::Test
{
protected:
    void SetUp() override
    {
        // generic cell grid (particles should not exceed this)
        glm::dvec3 center = glm::dvec3(10.0, 30.0, 0.0);
        glm::dvec3 domain_size = glm::dvec3(12.0, 12.0, 6.0);
        double cut_off = 6.0;

        // dummy particle type
        std::vector<GeneralParticleInfo> general_info;
        GeneralParticleInfo info = {.sigma=1.0, .epsilon=5.0, .mass=1.0};
        general_info.push_back(info);

        // only outflow boundaries for others are tested separately and would confuse basic calculator
        std::array<BoundType, 6> bounds_outflow = { OUTFLOW_BOUND, OUTFLOW_BOUND,
                                                    OUTFLOW_BOUND, OUTFLOW_BOUND,
                                                    OUTFLOW_BOUND, OUTFLOW_BOUND };

        // fill particle list with some particles
        m_particles = std::make_shared<std::vector<Particle>>();
        m_particles->reserve(10);
        glm::dvec3 x0 = {  9.5,  32.0,  0.0 };
        glm::dvec3 v0 = {  10.0,  0.0,  0.0 };
        m_particles->emplace_back(x0, v0, 0, 0);
        glm::dvec3 x1 = {  9.5,  30.5,  0.0 };
        glm::dvec3 v1 = {  10.0,  0.0,  0.0 };
        m_particles->emplace_back(x1, v1, 0, 1);
        glm::dvec3 x2 = {  11.5,  32.3,  0.0 };
        glm::dvec3 v2 = { -7.0,  0.0,  0.0 };
        m_particles->emplace_back(x2, v2, 0, 2);
        glm::dvec3 x3 = {  11.5,  30.3,  0.0 };
        glm::dvec3 v3 = { -7.0,  0.0,  0.0 };
        m_particles->emplace_back(x3, v3, 0, 3);


        // use small delta_t
        m_delta_t = 0.0001;

        ///////////////////
        // CellContainer //
        ///////////////////
        // create m_con_outflow
        m_con_outflow = std::make_shared<CellContainer>(
                *m_particles, general_info,
                center, domain_size,
                bounds_outflow, cut_off, 10);
        // create cell calculator
        m_cell_calc_outflow = std::make_shared<CellCalculator>(m_con_outflow.get(), m_delta_t, false, 0, 0);


        ////////////////////
        // BasicContainer //
        ////////////////////
        // create m_con_basic and molecularcalculator (because this also uses Lennard-Jones-Potential)
        m_con_basic = std::make_shared<BasicContainer>(*m_particles, general_info);
        m_mole_calc = std::make_shared<MolecularCalculator>(m_con_basic.get(), m_delta_t);
    }

    void TearDown() override
    {
    }

    void compareAllParticles();

    std::shared_ptr<std::vector<Particle>>  m_particles;
    double                                  m_delta_t;

    std::shared_ptr<CellContainer>          m_con_outflow;
    std::shared_ptr<CellCalculator>         m_cell_calc_outflow;

    std::shared_ptr<BasicContainer>         m_con_basic;
    std::shared_ptr<MolecularCalculator>    m_mole_calc;
};




/**
 * test if cellcalculator actually calculates Lennard-Jones-Potential correctly with small absolute error
 * Idea: compare to same operation done by old molecularcalculator
 */
TEST_F(CellCalculatorCompTest, test_compare)
{
    // compare directly at start and after init force calculate
    compareAllParticles();
    m_mole_calc->calculateF();
    m_cell_calc_outflow->calculateF();
    compareAllParticles();

    // do some calculate steps and compare after every single step (pos, force, velocity)
    for(int i=0; i<10; i++) {
        // position
        m_mole_calc->calculatePos();
        m_cell_calc_outflow->calculatePos();
        compareAllParticles();

        // force
        m_mole_calc->calculateF();
        m_cell_calc_outflow->calculateF();
        compareAllParticles();

        // velocity
        m_mole_calc->calculateV();
        m_cell_calc_outflow->calculateV();
        compareAllParticles();
    }
}

// check if all particles are equal in position, force and velocity (with small absolute error)
#define ABS_ERR 1e-16
void CellCalculatorCompTest::compareAllParticles()
{
    auto it_basic = m_con_basic->createSPIterator();
    while(!it_basic->isEnd()) {
        Particle *p_basic = it_basic->deref();
        auto it_cell  = m_con_outflow->createSPIterator();
        while(p_basic->getID() != it_cell->deref()->getID()) {
            if(it_cell->isEnd())
                throw std::runtime_error("error: cell_cont contains less elements than basic_cont");
            it_cell->increment();
        }
        Particle *p_cell  = it_cell->deref();

        // positions
        ASSERT_NEAR(p_basic->getPos().x, p_cell->getPos().x, ABS_ERR);
        ASSERT_NEAR(p_basic->getPos().y, p_cell->getPos().y, ABS_ERR);
        ASSERT_NEAR(p_basic->getPos().z, p_cell->getPos().z, ABS_ERR);

        // forces
        ASSERT_NEAR(p_basic->getF().x, p_cell->getF().x, ABS_ERR);
        ASSERT_NEAR(p_basic->getF().y, p_cell->getF().y, ABS_ERR);
        ASSERT_NEAR(p_basic->getF().z, p_cell->getF().z, ABS_ERR);

        // velocities
        ASSERT_NEAR(p_basic->getV().x, p_cell->getV().x, ABS_ERR);
        ASSERT_NEAR(p_basic->getV().y, p_cell->getV().y, ABS_ERR);
        ASSERT_NEAR(p_basic->getV().z, p_cell->getV().z, ABS_ERR);

        it_basic->increment();
    }
}
