#pragma once

class Thermostat
{
public:
    Thermostat() = default;
    virtual ~Thermostat() = default;

    virtual double getTemperature() = 0;
    virtual void setTemperature(bool gradual, double tDelta, double tTarget) = 0;

protected:
};

