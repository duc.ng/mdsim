#include "StandardThermo.h"

StandardThermo::StandardThermo(CellContainer *cont,
                               int dimensions,
                               double tInit,
                               bool needsInit)
        : s_cont(cont),
          m_dimensions(dimensions),
          m_boltzmannConstant(1.0), // set boltzmannConstant to 1.0 (dimensionless)
          m_tInit(tInit)
{
    //set variables
    m_numberParticle = s_cont->size();

    //if no velocities are set, initialize with MaxwellBoltzmann
    if(needsInit) {
        //initialize particles with Brownian motion (for non-zero temperature)
        auto it = s_cont->createSPIterator();
        while(!it->isEnd()) {
            auto p = it->deref();
            double m = (*s_cont->getGeneralInfo())[p->getType()].mass;
            MaxwellBoltzmannDistribution(*p, sqrt(m_tInit / m), m_dimensions);
            it->increment();
        }
    }
}


double StandardThermo::getTemperature()
{
    //create Iterator
    auto it = s_cont->createSPIterator();

    //calculate kinetic energy
    double eKin = 0;
    while(!it->isEnd()) {
        auto p = it->deref();
        glm::dvec3 v = p->getV();
        double m = (*s_cont->getGeneralInfo())[p->getType()].mass;
        eKin = eKin + (m * glm::dot(v, v) / 2);
        it->increment();
    }

    //return current temperature
    return eKin * 2 / (m_dimensions * m_numberParticle) / m_boltzmannConstant;
}

void StandardThermo::setTemperature(
        bool gradual,
        double tDelta,
        double tTarget)
{
    //get current temperature
    double tCurrent = getTemperature();

    //check for gradual scaling
    if(gradual &&
       tDelta != std::numeric_limits<double>::infinity() && tDelta >= 0 &&
       std::abs(tTarget - tCurrent) > tDelta) {
        if(tTarget > tCurrent) {
            tTarget = tCurrent + tDelta;
        }else {
            tTarget = tCurrent - tDelta;
        }
    }

    //calculate scaling factor and update V
    double beta = sqrt(tTarget / tCurrent);
    auto it = s_cont->createSPIterator();
    while(!it->isEnd()) {
        auto p = it->deref();
        p->setV(p->getV() * beta);
        it->increment();
    }
}


