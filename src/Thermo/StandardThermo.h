#pragma once

#include "Thermostat.h"
#include "utils/MaxwellBoltzmannDistribution.h"
#include "Container/CellContainer.h"

class StandardThermo : public Thermostat
{
public:
    StandardThermo(CellContainer *cont,
                   int dimensions,
                   double tInit,
                   bool needsInit);

    ~StandardThermo() = default;

    double getTemperature() override ;
    void setTemperature(bool gradual, double tDelta, double tTarget) override ;


private:
    CellContainer *s_cont;
    int m_dimensions;
    double m_boltzmannConstant;
    int m_numberParticle;
    double m_tInit;
};
