#pragma once

#include "utils/MaxwellBoltzmannDistribution.h"
#include "Container/ParallelCellCont.h"
#include "Thermostat.h"

class ParallelThermo : public Thermostat
{
public:
    ParallelThermo(ParallelCellCont *cont,
                   int dimensions,
                   double tInit,
                   bool needsInit);
    ~ParallelThermo() = default;

    double getTemperature() override;
    void setTemperature(bool gradual, double tDelta, double tTarget) override;


private:
    ParallelCellCont *s_cont;
    int m_dimensions;
    double m_boltzmannConstant;
    int m_numberParticle;
    double m_tInit;
};
