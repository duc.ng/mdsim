#include "Simulator.h"

//LOG4CXX::LoggerPtr Simulator::loggerLOG4CXX::Logger::getLogger("SimulatorLogger"));

Simulator::Simulator(int argc, char *argv[])
        : m_cont(nullptr), m_partCalc(nullptr)
{
    // create Reader for .xml file and read the passed in file
    Reader reader;
    reader.readArgs(argv[1]);
    m_args = reader.getArgs();

    /*LOG4CXX_DEBUG(logger, "this is the output basename:  " << m_args->output_base_name);
    LOG4CXX_DEBUG(logger, "this is the output frequency: " << m_args->output_freq);
    LOG4CXX_DEBUG(logger, "simulation end time:          " << m_args->t_end);
    LOG4CXX_DEBUG(logger, "this is delta:                " << m_args->delta_t);
    LOG4CXX_DEBUG(logger, "write output files enabled?   " << m_args->output_enabled);
    LOG4CXX_DEBUG(logger, "visualization enabled?        " << m_args->vis_enabled);
    LOG4CXX_DEBUG(logger, "file containing cuboid data:  " << m_args->input_filename);
    LOG4CXX_DEBUG(logger, "reading program arguments from XML file was successful!");*/

    // create new reader to read the file (past over the command line) and dump the data in our particleContainer
    m_cont = reader.readFile(m_args->input_filename);

    // create simple particle calculator
    m_partCalc = createCalculator();

    // create new Thermostat
    if(m_args->thermostat_enabled) {
        #ifdef _OPENMP
        m_thermostat = std::make_unique<ParallelThermo>(
                (ParallelCellCont *)m_cont.get(),
                m_args->extra_thermo->num_dim_of_sim,
                m_args->extra_thermo->temp_init,
                m_args->extra_thermo->needs_init);
        #else
        m_thermostat = std::make_unique<StandardThermo>(
                (CellContainer *)m_cont.get(),
                m_args->extra_thermo->num_dim_of_sim,
                m_args->extra_thermo->temp_init,
                m_args->extra_thermo->needs_init);
        #endif
    }

    // the forces are needed to calculate x, but are not given in the input file.
    m_partCalc->calculateF();

    // create framework for real-time visualization
    if(m_args->vis_enabled) {
        /*auto color_range = std::make_pair<double, double>(
                m_args->vis_color_range_min,
                m_args->vis_color_range_max);
        m_fr = std::make_unique<Framework>(
                800, 600, "MolSim",
                "../../vis/res/instanced_shader.vert",
                "../../vis/res/instanced_shader.frag",
                glm::vec3(0.0f, 0.0f, 30.0f), 1.0,
                m_cont.get(), std::move(color_range));

        m_fr->start_main_loop();*/
    }
}

Simulator::~Simulator()
{
}

std::shared_ptr<ParticleCalculator> Simulator::createCalculator()
{
    // return BasicContainer or CellContainer accordingly
    if(m_args->algType == AlgType::DIRECT_SUM) {
        if(m_args->calculator_name == "AstroCalculator")
            return std::make_shared<AstroCalculator>((BasicContainer *)m_cont.get(), m_args->delta_t);
        else if(m_args->calculator_name == "MolecularCalculator")
            return std::make_shared<MolecularCalculator>((BasicContainer *)m_cont.get(), m_args->delta_t);
    }else if(m_args->algType == AlgType::LINKED_CELL) {
        if(m_args->calculator_name == "CellCalculator") {
            #ifdef _OPENMP
            return std::make_shared<PCellCalculator>((ParallelCellCont *)m_cont.get(), m_args->delta_t,
                    m_args->gravity_enabled, m_args->gravity, m_args->gravity_axis);
            #else
            return std::make_shared<CellCalculator>((CellContainer *) m_cont.get(), m_args->delta_t,
                    m_args->gravity_enabled, m_args->gravity, m_args->gravity_axis);
            #endif
        }else if(m_args->calculator_name == "MembraneCalculator") {
            #ifdef _OPENMP
            return std::make_shared<MembraneCalculator>((ParallelCellCont *) m_cont.get(), m_args->delta_t,
                    m_args->gravity_enabled, m_args->gravity, m_args->gravity_axis,
                    m_args->k, m_args->r_0, m_args->x_length);
            #endif
        }
    }
    throw std::runtime_error("error: invalid algorithm type and/or calculator_name in arguments file");
}



void Simulator::main_loop()
{
    //LOG4CXX_DEBUG(logger, "Entering application.")
    //LOG4CXX_INFO(logger, "Hello from MolSim for PSE!")

    double current_time = 0;
    int iteration = 0;

    // for this loop, we assume: current pos, current f and current v are known
    while(current_time < m_args->t_end) {
        // calculate new x
        m_partCalc->calculatePos();

        // calculate new f
        m_partCalc->calculateF();

        //f_z force
        if (m_args->membrane_enabled && current_time<150){
            m_partCalc->calculateFZ(m_args->fz_up);
        }

        // calculate new v
        m_partCalc->calculateV();


        //temperature regulation
        if(m_args->thermostat_enabled) {
            if(iteration % m_args->extra_thermo->n_thermostat == 0)
                m_thermostat->setTemperature(
                        m_args->extra_thermo->gradual,
                        m_args->extra_thermo->temp_delta,
                        m_args->extra_thermo->temp_target);
        }

        // specify the output frequency by changing the value in the xml file
        if(iteration % m_args->output_freq == 0) {
            if(m_args->output_enabled)
                plotParticles(iteration);
            /*if(m_args->vis_enabled)
                m_fr->updateInstAttribs();*/

            // also rebalance load balancing tree
            #ifdef _OPENMP
            (std::dynamic_pointer_cast<ParallelCellCont>(m_cont))->rebalanceTree();
            #endif
        }

        //LOG4CXX_DEBUG(logger, "Iteration " << iteration << " finished.");
        current_time += m_args->delta_t;
        iteration++;
    }



    // export particle container if flag is set to true
    if(m_args->export_enabled) {
        //LOG4CXX_INFO(logger, "Simulation data is being exported...");

        Writer ExportWriter(m_cont->size(), FileType::TXT);
        ExportWriter.initExportFile(m_args->import_export_fileURI);
        // append number of particles to that file
        ExportWriter.appendLineToFile(std::to_string(m_cont->size()));

        // create single iterator, iterate over all particles and export them line by line
        auto it = m_cont->createSPIterator();
        while(!it->isEnd()) {
            ExportWriter.appendLineToFile(it->deref()->toTidyString());
            it->increment();
        }
        //LOG4CXX_INFO(logger, "Successfully exported all particles to file " << m_args->import_export_fileURI);
    }

    if(m_args->output_enabled) {/*LOG4CXX_INFO(logger, "output written. Terminating...");*/ }
    //LOG4CXX_INFO(logger, "Exiting application.");

    /*if(m_args->vis_enabled) { m_fr->join(); }*/
}


void Simulator::plotParticles(int iteration)
{
    Writer writer(m_cont->size(), FileType::VTK);

    auto it = m_cont->createSPIterator();
    while(!it->isEnd()) {
        Particle *p = it->deref();
        writer.plotParticle(*p);
        it->increment();
    }

    writer.writeFile(m_args->output_base_name, iteration);
}

