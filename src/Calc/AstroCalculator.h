#pragma once

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/norm.hpp>
//#include <log4cxx/logger.h>

#include "ParticleCalculator.h"
#include "Container/BasicContainer.h"


/**
 * Derived class of ParticleCalculator for simple particle calculations in worksheet1.
 */
class AstroCalculator : public ParticleCalculator
{
public:
    AstroCalculator(BasicContainer *cont, double delta_t);
    ~AstroCalculator() override = default;

    void calculatePos() override;
    void calculateF() override;
    void calculateV() override;

private:

    /** Basic Container, where all particles are saved. */
    BasicContainer *s_cont;
    std::unique_ptr<BasicContainer::SPIteratorBasic> m_sp_it;
    std::unique_ptr<BasicContainer::NPIteratorBasic> m_np_it;
};
