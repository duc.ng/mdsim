#pragma once

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/norm.hpp>
#include <omp.h>
#include <algorithm>
#include <random>

#include "ParticleCalculator.h"
#include "Container/ParallelCellCont.h"


class MembraneCalculator : public ParticleCalculator
{
public:
    MembraneCalculator(ParallelCellCont *cont, double delta_t,
                       bool gravity_enabled, double gravity, int gravity_axis,
                       double k, double r_0, int x_length);
    ~MembraneCalculator() override;

    void calculatePos() override;
    void calculateF() override;
    void calculateV() override;
    void calculateFZ(double fz) override;


    glm::dvec3 calcHarmonicP(int id1, int id2, glm::dvec3 pos1,glm::dvec3 pos2, double r_0, double k, double sigma, double epsilon);
    void reshuffle()
    {
        std::shuffle(m_rand_cell_inds.begin(), m_rand_cell_inds.end(), std::mt19937(std::random_device()()));
    }

private:
    int isNeighborParticle (int index1, int index2);

    void precalcSigmaEpsilon();

    void resetToGravity(PPartPT *partPT, PPartVFID *partVFID);
    void handleGhostParticles(PPartPT *partPT, PPartVFID *partVFID);
    glm::dvec3 evalGhostPos(const glm::dvec3 &pos, GhostLoc loc);

    static double calcSquareDist(const glm::dvec3 &pos1, const glm::dvec3 &pos2, const glm::dvec3 &shiftOn2);


private:
    /** Cell Container, where all particles are saved. */
    ParallelCellCont *s_cont;
    std::vector<int> m_rand_cell_inds;
    std::vector<omp_lock_t> m_locks;

    /** Input domain variables. */
    double m_cutoff_sq;
    glm::dvec3 m_left_corner;
    glm::dvec3 m_domain_size;


    /** Input gravity variables. */
    bool m_isGravity;
    int m_gravity_axis;
    double m_gravity;

    /** Mixed variables */
    std::vector<double> m_mixed_sigmas;
    std::vector<double> m_mixed_epsilons;

    /** Mixed variables */
    double m_k;
    double m_r_0;
    int m_x_length;

};
