#include "MolecularCalculator.h"

/**
 * Constructor of MolecularCalculator class.
 *
 * @param cont Container with particles
 * @param delta_t input variable
 */
MolecularCalculator::MolecularCalculator(BasicContainer *cont, double delta_t)
    : ParticleCalculator::ParticleCalculator(cont->getGeneralInfo(), delta_t),
      s_cont(cont)
{
    m_sigma   = (*s_general_info)[0].sigma;
    m_epsilon = (*s_general_info)[0].epsilon;

    m_sp_it = s_cont->createSPIterator();
    m_np_it = s_cont->createNPIterator();
}


/**
 * Update x,y,z position of all (selected) particles with Velocity-Störmer-Verlet-Algorithm.
 */
void MolecularCalculator::calculatePos()
{
    m_sp_it->resetIterator();
    while (!m_sp_it->isEnd()) {
        Particle *p = m_sp_it->deref();

        // set new position of particle
        p->setPos(ParticleCalculator::posStoermVerletAlg(*p));

        m_sp_it->increment();
    }
}


/**
 * Update force of all (selected) particles with use of Lennard-Jones-Potential.
 */
void MolecularCalculator::calculateF()
{
    //backup current force in oldF
    m_sp_it->resetIterator();
    while (!m_sp_it->isEnd()) {
        Particle *p = m_sp_it->deref();
        p->setOldF(p->getF());
        p->setF(glm::dvec3(0.0f));

        m_sp_it->increment();
    }

    //compute new force (newtonsch)
    m_np_it->resetIterator();
    while (!m_np_it->isEnd()) {
        Particle *p1 = m_np_it->deref().first;
        Particle *p2 = m_np_it->deref().second;

        // calculate Lennard-Jones-Potential
        glm::dvec3 f_ij = ParticleCalculator::calcLennardJonesFij(p1->getPos(), p2->getPos(), m_sigma, m_epsilon);

        // sum up all forces acting on particle i
        p1->setF(p1->getF() + f_ij);
        // and sum up negated on particle j (3. Newton's Law)
        p2->setF(p2->getF() - f_ij);

        m_np_it->increment();
    }
}


/**
 * Update velocity of all (selected) particles with Velocity-Störmer-Verlet-Algorithm.
 */
void MolecularCalculator::calculateV()
{
    m_sp_it->resetIterator();
    while (!m_sp_it->isEnd()) {
        Particle *p = m_sp_it->deref();

        // set new velocity of particle
        p->setV(ParticleCalculator::veloStoermVerletAlg(*p));

        m_sp_it->increment();
    }
}
