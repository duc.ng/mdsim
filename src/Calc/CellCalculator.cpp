#include "CellCalculator.h"

/**
 * Constructor of CellCalculator class.
 *
 * @param cont Container with particles
 * @param delta_t input variable
 * @param gravity_enabled input variable
 * @param gravity input variable
 * @param gravity_axis input variable
 */
CellCalculator::CellCalculator(CellContainer *cont, double delta_t,
                               bool gravity_enabled, double gravity, int gravity_axis)
        : ParticleCalculator::ParticleCalculator(cont->getGeneralInfo(), delta_t),
          s_cont(cont), m_isGravity(gravity_enabled), m_gravity(gravity), m_gravity_axis(gravity_axis)
{
    // get all parameters
    m_left_corner = s_cont->getLeftCorner();
    m_domain_size = s_cont->getDomainSize();
    s_general_info = s_cont->getGeneralInfo();

    // create iterators
    m_sp_it = s_cont->createSPIterator();
    m_np_it = s_cont->createNPIterator();

    // pre-calculate mixed sigmas / epsilons
    precalcSigmaEpsilon();
}


/**
 * helper function to calculate mixed sigma and mixed epsilom
 */
void CellCalculator::precalcSigmaEpsilon()
{
    for(GeneralParticleInfo g1 : *s_general_info) {
        for(GeneralParticleInfo g2 : *s_general_info) {
            m_mixed_sigmas.push_back((g1.sigma + g2.sigma) / 2.0);
            m_mixed_epsilons.push_back(std::sqrt(g1.epsilon * g2.epsilon));
        }
    }
}

/**
 * calculate next position step with Velocity-Störmer-Verlet-Algorithm
 */
void CellCalculator::calculatePos()
{
    m_sp_it->resetIterator();
    while(!m_sp_it->isEnd()) {
        Particle *p = m_sp_it->deref();

        // set new position of particle
        p->setPos(ParticleCalculator::posStoermVerletAlg(*p));

        m_sp_it->increment();
    }

    // after that, update all cells
    s_cont->updateCells();
}

/**
 * calculate next force step via Lennard-Jones-Potential
 * -> linked cell approach (only interact with particles in cells directly around particle)
 * -> use of Newtons 3. Law
 * -> sigmas and epsilons determined by Lorentz-Berthelot mixing rule
 * -> periodic and reflecting boundaries taken into account
 */
void CellCalculator::calculateF()
{
    // go through particle list, reset/backup forces and calculate ghostParticle forces
    m_sp_it->resetIterator();
    while(!m_sp_it->isEnd()) {
        // backup force in oldF
        Particle *p = m_sp_it->deref();
        p->setOldF(p->getF());

        // handle gravity
        resetToGravity(m_sp_it->deref());

        // if in boundary cell -> check ghost particles
        if(m_sp_it->isInBoundary())
            handleGhostParticles(p, m_sp_it->getGhostInfo());

        m_sp_it->increment();
    }

    //compute new force (newtonsch)
    m_np_it->resetIterator();
    while(!m_np_it->isEnd()) {
        Particle *p1 = m_np_it->deref().first;
        Particle *p2 = m_np_it->deref().second;

        // we must only apply shift to p2 for this is the one which stems from dyn_it (iterating through hull)
        glm::dvec3 pos1 = p1->getPos();
        glm::dvec3 pos2 = p2->getPos() + m_np_it->getHullCellShift();

        // get mixed sigma and epsilon
        double sigma   = m_mixed_sigmas  [p1->getType() * s_general_info->size() + p2->getType()];
        double epsilon = m_mixed_epsilons[p1->getType() * s_general_info->size() + p2->getType()];

        // calculate Lennard-Jones-Potential
        glm::dvec3 f_ij = ParticleCalculator::calcLennardJonesFij(pos1, pos2, sigma, epsilon);

        // sum up all forces acting on particle i
        p1->setF(p1->getF() + f_ij);
        // and sum up negated on particle j (3. Newton's Law)
        p2->setF(p2->getF() - f_ij);

        // don't forget to increment!
        m_np_it->increment();
    }
}

/**
 * calculate next velocity step with Velocity-Störmer-Verlet-Algorithm
 */
void CellCalculator::calculateV()
{
    m_sp_it->resetIterator();
    while(!m_sp_it->isEnd()) {
        Particle *p = m_sp_it->deref();

        // set new velocity of particle
        p->setV(ParticleCalculator::veloStoermVerletAlg(*p));

        m_sp_it->increment();
    }
}

/**
 * add gravity to particle force if needed, else reset to zero
 */
void CellCalculator::resetToGravity(Particle *p)
{
    if(m_isGravity) {
        // if gravity exists -> do gravity on y-axis
        auto vec = glm::dvec3(0.0);
        vec[m_gravity_axis] = (*s_general_info)[p->getType()].mass * m_gravity;
        p->setF(vec);
    }else {
        // else reset current force to zero
        p->setF(glm::dvec3(0.0f));
    }
}

/**
 * create all necessary ghost particle positions and calculate force influence of them on particle
 */
void CellCalculator::handleGhostParticles(Particle *p, const std::array<bool, 6> &ghostInfo)
{
    for(int i = 0; i < 6; i++) {
        if(ghostInfo[i]) {
            // if ghostParticle exists for this direction
            // -> evaluate ghostPos, sigma, epsilon and calculate Lennard-Jones-Potential
            double sigma   = (*s_general_info)[p->getType()].sigma;
            double epsilon = (*s_general_info)[p->getType()].epsilon;
            glm::dvec3 ghostPos = evalGhostPos(p->getPos(), static_cast<GhostLoc>(i));
            p->setF(p->getF() + ParticleCalculator::calcLennardJonesFij(p->getPos(), ghostPos, sigma, epsilon));
        }
    }
}

/**
 * evaluate the position of the ghost particle on basis of actual particle position and location enum of ghost
 */
glm::dvec3 CellCalculator::evalGhostPos(const glm::dvec3 &pos, GhostLoc loc)
{
    switch(loc) {
        case GhostLoc::GHOST_LEFT:
            return glm::dvec3(2.0 * (m_left_corner.x) - pos.x, pos.y, pos.z);
        case GhostLoc::GHOST_RIGHT:
            return glm::dvec3(2.0 * (m_left_corner.x + m_domain_size.x) - pos.x, pos.y, pos.z);
        case GhostLoc::GHOST_DOWN:
            return glm::dvec3(pos.x, 2.0 * (m_left_corner.y) - pos.y, pos.z);
        case GhostLoc::GHOST_UP:
            return glm::dvec3(pos.x, 2.0 * (m_left_corner.y + m_domain_size.y) - pos.y, pos.z);
        case GhostLoc::GHOST_FRONT:
            return glm::dvec3(pos.x, pos.y, 2.0 * (m_left_corner.z) - pos.z);
        case GhostLoc::GHOST_BACK:
            return glm::dvec3(pos.x, pos.y, 2.0 * (m_left_corner.z + m_domain_size.z) - pos.z);
        default:
            throw std::runtime_error("error: invalid GhostLoc!");
    }
}

