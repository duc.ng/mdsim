#pragma once

#include "Container/ParticleContainer.h"

/**
 * Base class for all particle calculator classes.
 */
class ParticleCalculator
{
public:
    ParticleCalculator(std::vector<GeneralParticleInfo> *general_info, double delta_t)
            : s_general_info(general_info), m_delta_t(delta_t) {}

    virtual ~ParticleCalculator() = default;

    /**
     * calculate position
     */
    virtual void calculatePos() = 0;

    /**
     * calculate force
     */
    virtual void calculateF() = 0;

    /**
     * calculate velocity
     */
    virtual void calculateV() = 0;

    /**
      * calculate fz_up (for membrane)
      */
    virtual void calculateFZ(double fz){}


protected:

    inline glm::dvec3 posStoermVerletAlg(const Particle &p)
    {
        // Velocity-Störmer-Verlet-Algorithm for position update
        double mass_div = 2.0 * (*s_general_info)[p.getType()].mass;
        return p.getPos() + m_delta_t * p.getV() + m_delta_t * m_delta_t * (p.getF() / mass_div);
    }

    inline glm::dvec3 veloStoermVerletAlg(const Particle &p)
    {
        // Velocity-Störmer-Verlet-Algorithm for velocity update
        double mass_div = 2.0 * (*s_general_info)[p.getType()].mass;
        return p.getV() + m_delta_t * ((p.getOldF() + p.getF()) / mass_div);
    }

    static inline glm::dvec3 calcLennardJonesFij(const glm::dvec3 &pos_i,
                                                 const glm::dvec3 &pos_j,
                                                 double sigma, double epsilon)
    {
        /* force between two particles via Lennard-Jones
         * optimizations:
         * -> don't use std::pow   -> unroll exponentiation
         * -> don't use l2Norm     -> use dot-product with self
         */
        glm::dvec3 pos_dist = pos_i - pos_j;
        double inv_delta_sqnorm = 1 / glm::dot(pos_dist, pos_dist);
        double preambel = (24 * epsilon) * inv_delta_sqnorm;
        double tmp = sigma * sigma * inv_delta_sqnorm;
        tmp = tmp * tmp * tmp;
        double intermediate = tmp - 2 * tmp * tmp;
        intermediate *= preambel;

        return intermediate * (pos_j - pos_i);
    }

    #ifdef _OPENMP
    static inline glm::dvec3 calcLennardJonesFij_simd(const glm::dvec3 &pos_i,
                                                      const glm::dvec3 &pos_j,
                                                      double sigma, double epsilon)
    {
        /* force between two particles via Lennard-Jones
         * optimizations:
         * -> don't use std::pow   -> unroll exponentiation
         * -> don't use l2Norm     -> use dot-product with self
         */
        glm::dvec3 pos_dist;
        #pragma omp simd
        for(int i = 0; i < 3; i++)
            pos_dist[i] = pos_i[i] - pos_j[i];

        double sqnorm = 0;
        #pragma omp simd reduction (+:sqnorm)
        for(int i = 0; i < 3; i++)
            sqnorm += pos_dist[i] * pos_dist[i];

        double inv_delta_sqnorm = 1 / sqnorm;
        double preambel = (24 * epsilon) * inv_delta_sqnorm;
        double tmp = sigma * sigma * inv_delta_sqnorm;
        tmp = tmp * tmp * tmp;
        double intermediate = tmp - 2 * tmp * tmp;
        intermediate *= preambel;

        glm::dvec3 result;
        #pragma omp simd
        for(int i = 0; i < 3; i++)
            result[i] = intermediate * (pos_j[i] - pos_i[i]);

        return result;
    }
    #endif


protected:
    //static log4cxx::LoggerPtr logger;

    /** General input data to create particles. */
    std::vector<GeneralParticleInfo> *s_general_info;

    /** time step */
    double m_delta_t;
};
