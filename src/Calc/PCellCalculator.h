#pragma once

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/norm.hpp>
#include <omp.h>

#include "ParticleCalculator.h"
#include "Container/ParallelCellCont.h"


class PCellCalculator : public ParticleCalculator
{
public:
    PCellCalculator(ParallelCellCont *cont, double delta_t,
                    bool gravity_enabled, double gravity, int gravity_axis);
    ~PCellCalculator() override;

    void calculatePos() override;
    void calculateF() override;
    void calculateV() override;

private:
    void precalcSigmaEpsilon();

#ifdef _OPENMP
    inline void performLJStep(PPartVFID *fix_vfid, PPartPT *fix_pt,
                              PPartVFID *dyn_vfid, PPartPT *dyn_pt, PHullCell *hull_cell)
    {
        // get mixed sigma and epsilon
        double sigma = m_mixed_sigmas[fix_pt->type * s_general_info->size() + dyn_pt->type];
        double epsilon = m_mixed_epsilons[fix_pt->type * s_general_info->size() + dyn_pt->type];

        // calculate Lennard-Jones-Potential
        glm::dvec3 f_ij = ParticleCalculator::calcLennardJonesFij(
                fix_pt->pos,
                dyn_pt->pos + hull_cell->shift,
                sigma, epsilon);

        omp_set_lock(&m_locks[fix_vfid->id]);
        fix_vfid->force += f_ij;
        omp_unset_lock(&m_locks[fix_vfid->id]);

        omp_set_lock(&m_locks[dyn_vfid->id]);
        dyn_vfid->force -= f_ij;
        omp_unset_lock(&m_locks[dyn_vfid->id]);
    }

    void resetToGravity(PPartPT *partPT, PPartVFID *partVFID);
    void handleGhostParticles(PPartPT *partPT, PPartVFID *partVFID);
    glm::dvec3 evalGhostPos(const glm::dvec3 &pos, GhostLoc loc);

    static inline double calcSquareDist_simd(const glm::dvec3 &pos1,
                                             const glm::dvec3 &pos2,
                                             const glm::dvec3 &shiftOn2)
    {
        // distance^2 = (xA - xB)^2 + (yA - yB)^2 + (zA - zB)^2
        glm::dvec3 delta;
        #pragma omp simd
        for(int i=0; i<3; i++)
            delta[i] = pos1[i] - (pos2[i] + shiftOn2[i]);

        double sqnorm = 0;
        #pragma omp simd reduction (+:sqnorm)
        for(int i=0; i<3; i++)
            sqnorm += delta[i]*delta[i];

        return sqnorm;
    }
#endif


private:
    /** Cell Container, where all particles are saved. */
    ParallelCellCont *s_cont;
    std::vector<omp_lock_t> m_locks;

    /** Input domain variables. */
    double m_cutoff_sq;
    glm::dvec3 m_left_corner;
    glm::dvec3 m_domain_size;


    /** Input gravity variables. */
    bool m_isGravity;
    int m_gravity_axis;
    double m_gravity;

    /** Mixed variables */
    std::vector<double> m_mixed_sigmas;
    std::vector<double> m_mixed_epsilons;
};
