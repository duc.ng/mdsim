#include "PCellCalculator.h"
#ifdef _OPENMP

PCellCalculator::PCellCalculator(ParallelCellCont *cont, double delta_t,
                                           bool gravity_enabled, double gravity, int gravity_axis)
        : ParticleCalculator(&cont->m_general_info, delta_t),
          s_cont(cont), m_isGravity(gravity_enabled), m_gravity_axis(gravity_axis), m_gravity(gravity)
{
    // get all parameters
    m_left_corner = s_cont->m_left_corner;
    m_domain_size = s_cont->m_domain_size;
    s_general_info = &s_cont->m_general_info;
    m_cutoff_sq = s_cont->m_cut_off * s_cont->m_cut_off;

    // pre-calculate mixed sigmas / epsilons
    precalcSigmaEpsilon();

    // init Kd-Tree structure of ParallelContainer
    s_cont->initKdTree(omp_get_max_threads());

    // init locks
    m_locks.resize((unsigned) s_cont->m_num_parts, {});
    for(auto &lock : m_locks)
        omp_init_lock(&lock);
}

PCellCalculator::~PCellCalculator()
{
    // destroy locks
    for(int i = 0; i < m_locks.size(); i++)
        omp_destroy_lock(&m_locks[i]);
}

/**
 * helper function to calculate mixed sigma and mixed epsilom
 */
void PCellCalculator::precalcSigmaEpsilon()
{
    for(GeneralParticleInfo g1 : *s_general_info) {
        for(GeneralParticleInfo g2 : *s_general_info) {
            m_mixed_sigmas.push_back((g1.sigma + g2.sigma) / 2.0);
            m_mixed_epsilons.push_back(std::sqrt(g1.epsilon * g2.epsilon));
        }
    }
}


void PCellCalculator::calculatePos()
{
    #pragma omp parallel default(none)
    {
        int leaf_ind = omp_get_thread_num();
        // only iterate over thread-own set of cells
        std::vector<PCell *> &cell_vec = (*s_cont->s_leaf_cells)[leaf_ind];
        for(auto cell : cell_vec) {
            for(int j = 0; j < cell->num_parts; j++) {
                PPartPT *partPT = &cell->parts_pt[j];
                PPartVFID *partVFID = &cell->parts_vfid[j];

                // set new position of particle (Velocity-Stoermer-Verlet Algorithm)
                // FIXME: quite high error compared to ParticleCalculator (difference: += vs + operator)
                double mass_div = 2.0 * (*s_general_info)[partPT->type].mass;
                partPT->pos += m_delta_t * partVFID->velo + m_delta_t * m_delta_t * (partVFID->force / mass_div);
            }
        }
    }

    // after that, update all cells
    s_cont->updateCells();
}


void PCellCalculator::calculateF()
{
    // go through particle list, reset/backup forces and calculate ghostParticle forces
    #pragma omp parallel default(none)
    {
        int leaf_ind = omp_get_thread_num();
        // only iterate over thread-own set of cells
        std::vector<PCell *> cell_vec = (*s_cont->s_leaf_cells)[leaf_ind];
        for(auto cell : cell_vec) {

            for(int j = 0; j < cell->num_parts; j++) {
                PPartVFID *partVFID = &cell->parts_vfid[j];
                PPartPT *partPT = &cell->parts_pt[j];

                // backup force in oldF
                partVFID->old_f = partVFID->force;

                // handle gravity
                resetToGravity(partPT, partVFID);

                // if in boundary cell -> check ghost particles
                if(cell->isBoundary)
                    handleGhostParticles(partPT, partVFID);
            }
        }
    }

    #pragma omp parallel default(none)
    {
        int leaf_ind = omp_get_thread_num();
        // only iterate over thread-own set of cells
        std::vector<PCell *> cell_vec = (*s_cont->s_leaf_cells)[leaf_ind];
        for(PCell *cell : cell_vec) {

            // iterating over all fixed particles in current cell
            for(int fix_ind = 0; fix_ind < cell->num_parts; fix_ind++) {
                PPartPT *fix_pt = &cell->parts_pt[fix_ind];

                // perform LJ-Step on same cell and all neighboring pairs in newt_hull
                for(int dyn_ind = fix_ind + 1; dyn_ind < cell->num_parts; dyn_ind++) {
                    PPartPT *dyn_pt = &cell->parts_pt[dyn_ind];
                    PHullCell *hull_cell = &cell->newt_hull[0];

                    // only care if in distance of cutoff_radius
                    double dist_sq = calcSquareDist_simd(fix_pt->pos, dyn_pt->pos, hull_cell->shift);
                    if(dist_sq <= m_cutoff_sq) {
                        // now perform actual Lennard-Jones calculation step
                        performLJStep(&cell->parts_vfid[fix_ind], fix_pt,
                                      &hull_cell->cell->parts_vfid[dyn_ind], dyn_pt,
                                      hull_cell);
                    }
                }

                // pairing with all hull_cells in newt_hull ...
                for(int hc_ind = 1; hc_ind < cell->hull_size; hc_ind++) {
                    PHullCell *hull_cell = &cell->newt_hull[hc_ind];
                    // ... and all particles in each hull_cell
                    for(int dyn_ind = 0; dyn_ind < hull_cell->cell->num_parts; dyn_ind++) {
                        PPartPT *dyn_pt = &hull_cell->cell->parts_pt[dyn_ind];

                        // only care if in distance of cutoff_radius
                        double dist_sq = calcSquareDist_simd(fix_pt->pos, dyn_pt->pos, hull_cell->shift);
                        if(dist_sq <= m_cutoff_sq) {
                            // now perform actual Lennard-Jones calculation step
                            performLJStep(&cell->parts_vfid[fix_ind], fix_pt,
                                          &hull_cell->cell->parts_vfid[dyn_ind], dyn_pt,
                                          hull_cell);
                        }
                    }
                }
            }
        }
    }
}


void PCellCalculator::calculateV()
{

    #pragma omp parallel default(none)
    {
        int leaf_ind = omp_get_thread_num();
        // only iterate over thread-own set of cells
        std::vector<PCell *> &cell_vec = (*s_cont->s_leaf_cells)[leaf_ind];
        for(auto cell : cell_vec) {
            for(int j = 0; j < cell->num_parts; j++) {
                PPartPT *partPT = &cell->parts_pt[j];
                PPartVFID *partVFID = &cell->parts_vfid[j];

                // set new position of particle (Velocity-Stoermer-Verlet Algorithm)
                double mass_div = 2.0 * (*s_general_info)[partPT->type].mass;
                partVFID->velo += m_delta_t * ((partVFID->old_f + partVFID->force) / mass_div);
            }
        }
    }
}



/**
 * add gravity to particle force if needed, else reset to zero
 */
void PCellCalculator::resetToGravity(PPartPT *partPT, PPartVFID *partVFID)
{
    if(m_isGravity) {
        // if gravity exists -> do gravity on y-axis
        auto vec = glm::dvec3(0.0);
        vec[m_gravity_axis] = (*s_general_info)[partPT->type].mass * m_gravity;
        partVFID->force = vec;
    }else {
        // else reset current force to zero
        partVFID->force = glm::dvec3(0.0f);
    }
}

/**
 * create all necessary ghost particle positions and calculate force influence of them on particle
 */
void PCellCalculator::handleGhostParticles(PPartPT *partPT, PPartVFID *partVFID)
{
    for(int i = 0; i < 6; i++) {
        if(partVFID->isGhost[i]) {
            // if ghostParticle exists for this direction
            // -> evaluate ghostPos, sigma, epsilon and calculate Lennard-Jones-Potential
            double sigma = (*s_general_info)[partPT->type].sigma;
            double epsilon = (*s_general_info)[partPT->type].epsilon;
            glm::dvec3 ghostPos = evalGhostPos(partPT->pos, static_cast<GhostLoc>(i));
            partVFID->force += ParticleCalculator::calcLennardJonesFij(partPT->pos, ghostPos, sigma, epsilon);
        }
    }
}

/**
 * evaluate the position of the ghost particle on basis of actual particle position and location enum of ghost
 */
glm::dvec3 PCellCalculator::evalGhostPos(const glm::dvec3 &pos, GhostLoc loc)
{
    switch(loc) {
        case GhostLoc::GHOST_LEFT:
            return glm::dvec3(2.0 * (m_left_corner.x) - pos.x, pos.y, pos.z);
        case GhostLoc::GHOST_RIGHT:
            return glm::dvec3(2.0 * (m_left_corner.x + m_domain_size.x) - pos.x, pos.y, pos.z);
        case GhostLoc::GHOST_DOWN:
            return glm::dvec3(pos.x, 2.0 * (m_left_corner.y) - pos.y, pos.z);
        case GhostLoc::GHOST_UP:
            return glm::dvec3(pos.x, 2.0 * (m_left_corner.y + m_domain_size.y) - pos.y, pos.z);
        case GhostLoc::GHOST_FRONT:
            return glm::dvec3(pos.x, pos.y, 2.0 * (m_left_corner.z) - pos.z);
        case GhostLoc::GHOST_BACK:
            return glm::dvec3(pos.x, pos.y, 2.0 * (m_left_corner.z + m_domain_size.z) - pos.z);
        default:
            throw std::runtime_error("error: invalid GhostLoc!");
    }
}

#endif
