#include "MembraneCalculator.h"
#ifdef _OPENMP

MembraneCalculator::MembraneCalculator(ParallelCellCont *cont, double delta_t,
                                       bool gravity_enabled, double gravity, int gravity_axis,
                                       double k, double r_0, int x_length)
        : ParticleCalculator(&cont->m_general_info, delta_t),
          s_cont(cont), m_isGravity(gravity_enabled), m_gravity_axis(gravity_axis), m_gravity(gravity),
          m_k(k), m_r_0(r_0),m_x_length(x_length)
{
    // get all parameters
    m_left_corner = s_cont->m_left_corner;
    m_domain_size = s_cont->m_domain_size;
    s_general_info = &s_cont->m_general_info;
    m_cutoff_sq = s_cont->m_cut_off * s_cont->m_cut_off;

    // pre-calculate mixed sigmas / epsilons
    precalcSigmaEpsilon();

    // init locks
    m_locks.resize((unsigned)s_cont->m_num_parts, {0});
    for(int i=0; i<m_locks.size(); i++) {
        omp_init_lock(&m_locks[i]);
    }

    // init Kd-Tree structure of ParallelContainer
    s_cont->initKdTree(omp_get_max_threads());
}

MembraneCalculator::~MembraneCalculator()
{
    // destroy locks
    for(int i=0; i<m_locks.size(); i++)
        omp_destroy_lock(&m_locks[i]);
}


/**
 * helper function to calculate mixed sigma and mixed epsilom
 */
void MembraneCalculator::precalcSigmaEpsilon()
{
    for(GeneralParticleInfo g1 : *s_general_info) {
        for(GeneralParticleInfo g2 : *s_general_info) {
            m_mixed_sigmas.push_back((g1.sigma + g2.sigma) / 2.0);
            m_mixed_epsilons.push_back(std::sqrt(g1.epsilon * g2.epsilon));
        }
    }
}


void MembraneCalculator::calculatePos()
{
    #pragma omp parallel default(none)
    {
        int leaf_ind = omp_get_thread_num();
        // only iterate over thread-own set of cells
        std::vector<PCell *> &cell_vec = (*s_cont->s_leaf_cells)[leaf_ind];
        for(auto cell : cell_vec) {
            for(int j = 0; j < cell->num_parts; j++) {
                PPartPT *partPT = &cell->parts_pt[j];
                PPartVFID *partVFID = &cell->parts_vfid[j];

                // set new position of particle
                // FIXME: quite high error compared to ParticleCalculator (difference: += vs + operator)
                double mass_div = 2.0 * (*s_general_info)[partPT->type].mass;
                partPT->pos += m_delta_t * partVFID->velo + m_delta_t * m_delta_t * (partVFID->force / mass_div);
            }
        }
    }

    // after that, update all cells
    s_cont->updateCells();
}

/**
 * calculate next force step via Lennard-Jones-Potential
 * -> linked cell approach (only interact with particles in cells directly around particle)
 * -> use of Newtons 3. Law
 * -> sigmas and epsilons determined by Lorentz-Berthelot mixing rule
 * -> periodic and reflecting boundaries taken into account
 */
void MembraneCalculator::calculateF()
{
    // go through particle list, reset/backup forces and calculate ghostParticle forces
    #pragma omp parallel default(none)
    {
        int leaf_ind = omp_get_thread_num();
        // only iterate over thread-own set of cells
        std::vector<PCell *> cell_vec = (*s_cont->s_leaf_cells)[omp_get_thread_num()];
        for(auto cell : cell_vec) {

            for(int j = 0; j < cell->num_parts; j++) {
                PPartVFID *partVFID = &cell->parts_vfid[j];
                PPartPT *partPT = &cell->parts_pt[j];

                // backup force in oldF
                partVFID->old_f = partVFID->force;

                // handle gravity
                resetToGravity(partPT, partVFID);

                // if in boundary cell -> check ghost particles
                if(cell->isBoundary)
                    handleGhostParticles(partPT, partVFID);

            }
        }
    }

    //compute new force (newtonsch)
    #pragma omp parallel default(none)
    {
        int leaf_ind = omp_get_thread_num();
        // only iterate over thread-own set of cells
        std::vector<PCell *> cell_vec = (*s_cont->s_leaf_cells)[leaf_ind];
        for(PCell *cell : cell_vec) {

            // iterating over all fixed particles in current cell
            for(int fix_ind = 0; fix_ind < cell->num_parts; fix_ind++) {
                PPartPT *fix_pt = &cell->parts_pt[fix_ind];

                // first perform LJ-Step on all pairs in same cell
                for(int dyn_ind = fix_ind + 1; dyn_ind < cell->num_parts; dyn_ind++) {
                    PPartPT *dyn_pt = &cell->parts_pt[dyn_ind];
                    PPartVFID *partVFID_fix = &cell->parts_vfid[fix_ind];
                    PPartVFID *partVFID_dyn = &cell->parts_vfid[dyn_ind];

                    // get mixed sigma and epsilon
                    double sigma   = m_mixed_sigmas  [fix_pt->type * s_general_info->size() + dyn_pt->type];
                    double epsilon = m_mixed_epsilons[fix_pt->type * s_general_info->size() + dyn_pt->type];

                    // calculate Lennard-Jones-Potential + HarmonicPotential
                    glm::dvec3 f_ij = calcHarmonicP(
                            partVFID_fix->id,partVFID_dyn->id,
                            fix_pt->pos,
                            dyn_pt->pos + cell->newt_hull[0].shift,
                            m_r_0,m_k,sigma,epsilon);


                    omp_set_lock(&m_locks[partVFID_fix->id]);
                    partVFID_fix->force += f_ij;
                    omp_unset_lock(&m_locks[partVFID_fix->id]);

                    omp_set_lock(&m_locks[partVFID_dyn->id]);
                    partVFID_dyn->force -= f_ij;
                    omp_unset_lock(&m_locks[partVFID_dyn->id]);
                }

                // then perform LJ-Step on all neighboring pairs in newt_hull
                // pairing with all hull_cells in newt_hull ...
                for(int hc_ind = 1; hc_ind < cell->hull_size; hc_ind++) {
                    PHullCell *hull_cell = &cell->newt_hull[hc_ind];
                    // ... and all particles in each hull_cell
                    for(int dyn_ind = 0; dyn_ind < hull_cell->cell->num_parts; dyn_ind++) {
                        PPartPT *dyn_pt = &hull_cell->cell->parts_pt[dyn_ind];
                        PPartVFID *partVFID_fix = &cell->parts_vfid[fix_ind];
                        PPartVFID *partVFID_dyn = &hull_cell->cell->parts_vfid[dyn_ind];

                        // only care if in distance of cutoff_radius
                        double dist_sq = calcSquareDist(fix_pt->pos, dyn_pt->pos, hull_cell->shift);
                        if(dist_sq <= m_cutoff_sq) {

                            // get mixed sigma and epsilon
                            double sigma   = m_mixed_sigmas  [fix_pt->type * s_general_info->size() + dyn_pt->type];
                            double epsilon = m_mixed_epsilons[fix_pt->type * s_general_info->size() + dyn_pt->type];

                            // calculate Lennard-Jones-Potential
                            glm::dvec3 f_ij = calcHarmonicP(
                                    partVFID_fix->id,partVFID_dyn->id,
                                    fix_pt->pos,
                                    dyn_pt->pos + cell->newt_hull[0].shift,
                                    m_r_0,m_k,sigma,epsilon);

                            omp_set_lock(&m_locks[partVFID_fix->id]);
                            partVFID_fix->force += f_ij;
                            omp_unset_lock(&m_locks[partVFID_fix->id]);

                            omp_set_lock(&m_locks[partVFID_dyn->id]);
                            partVFID_dyn->force -= f_ij;
                            omp_unset_lock(&m_locks[partVFID_dyn->id]);
                        }

                    }
                }

            }
        }
    }
}

/**
 * some particles will get "pulled up" by variable Fz
 * -> ID's are correct only on cuboid with n=50x50
 */
void MembraneCalculator::calculateFZ(double fz)
{
    for(int leaf_ind = 0; leaf_ind < omp_get_max_threads(); leaf_ind++) {
        // only iterate over thread-own set of cells
        std::vector<PCell *> &cell_vec = (*s_cont->s_leaf_cells)[leaf_ind];
        for(auto cell : cell_vec) {
            for(int j = 0; j < cell->num_parts; j++) {
                PPartVFID *partVFID = &cell->parts_vfid[j];

                //"pull" coordinates: (17/24),(17/25),(18/24),(18/25)
                if(partVFID->id == 1217 || partVFID->id == 1218 || partVFID->id == 1267 || partVFID->id == 1268) {
                    glm::dvec3 factor = {0, 0, fz};

                    // backup force in oldF
                    //partVFID->old_f = partVFID->force;

                    //update new force
                    partVFID->force += factor;
                }
            }
        }
    }
}

/**
 * calculates Harmonic Potential between two particles
 */
glm::dvec3 MembraneCalculator::calcHarmonicP(int id1, int id2, glm::dvec3 pos1,glm::dvec3 pos2, double r_0, double k, double sigma, double epsilon){

    //init vatiables
    glm::dvec3 f_ij = {0,0,0};
    double distance = glm::l2Norm(pos1 - pos2);
    //sqrt(glm::dot(pos1 - pos2, pos1 - pos2));

    //if both particles are vertical or diagonal neighbors, do calculations
    switch(isNeighborParticle(id1,id2)) {
        case 1:
            f_ij = f_ij + k*(distance-r_0)*(pos2 - pos1);
            break;
        case 2:
            f_ij = f_ij + k*(distance-sqrt(2)*r_0)*(pos2 - pos1);
            break;
        default:;
    }

    // apply Lennard-Jones-Potential, if distance is lower than 2^(1/6)*sigma
    if (distance < (1.122462048309372981433533049*sigma)){
        f_ij = f_ij + ParticleCalculator::calcLennardJonesFij(pos1, pos2, sigma, epsilon);
    }
    return f_ij;
}


/**
 * returns 1 for direct Neighbor and 2 for diagonal neighbor * - works only on cuboids in 2D
 * - verifies a max. of 4 direct particle neighbors in 2D
 */
int MembraneCalculator::isNeighborParticle (int index1, int index2)
{
    //set variables
    int delta;
    int smallNumber;
    int bigNumber;

    //determine which index is bigger and calculate delta
    if (index1>index2){
        bigNumber = index1;
        smallNumber = index2;
    } else {
        bigNumber = index2;
        smallNumber = index1;
    }
    delta = bigNumber - smallNumber;

    //direct neighbor
    if ((delta == 1 && bigNumber % m_x_length != 0) || (delta == m_x_length)){
        return 1;
    }

    //diagonal neighbor
    if (delta == m_x_length+1 || delta == m_x_length-1){
        return 2;
    }

    //no neighbors
    return 999999;
}


void MembraneCalculator::calculateV()
{
    #pragma omp parallel default(none)
    {
        int leaf_ind = omp_get_thread_num();
        // only iterate over thread-own set of cells
        std::vector<PCell *> &cell_vec = (*s_cont->s_leaf_cells)[leaf_ind];
        for(auto cell : cell_vec) {
            for(int j = 0; j < cell->num_parts; j++) {
                PPartPT *partPT = &cell->parts_pt[j];
                PPartVFID *partVFID = &cell->parts_vfid[j];

                // set new position of particle (Velocity-Stoermer-Verlet Algorithm)
                double mass_div = 2.0 * (*s_general_info)[partPT->type].mass;
                partVFID->velo += m_delta_t * ((partVFID->old_f + partVFID->force) / mass_div);
            }
        }
    }
}



double MembraneCalculator::calcSquareDist(const glm::dvec3 &pos1,
                                            const glm::dvec3 &pos2,
                                            const glm::dvec3 &shiftOn2)
{
    // distance^2 = (xA - xB)^2 + (yA - yB)^2 + (zA - zB)^2
    glm::dvec3 delta = pos1 - (pos2 + shiftOn2);
    return delta.x*delta.x + delta.y*delta.y + delta.z*delta.z;
}


/**
 * add gravity to particle force if needed, else reset to zero
 */
void MembraneCalculator::resetToGravity(PPartPT *partPT, PPartVFID *partVFID)
{
    if(m_isGravity) {
        // if gravity exists -> do gravity on y-axis
        auto vec = glm::dvec3(0.0);
        vec[m_gravity_axis] = (*s_general_info)[partPT->type].mass * m_gravity;
        partVFID->force = vec;
    } else {
        // else reset current force to zero
        partVFID->force = glm::dvec3(0.0f);
    }
}

/**
 * create all necessary ghost particle positions and calculate force influence of them on particle
 */
void MembraneCalculator::handleGhostParticles(PPartPT *partPT, PPartVFID *partVFID)
{
    for(int i = 0; i < 6; i++) {
        if(partVFID->isGhost[i]) {
            // if ghostParticle exists for this direction
            // -> evaluate ghostPos, sigma, epsilon and calculate Lennard-Jones-Potential
            double sigma = (*s_general_info)[partPT->type].sigma;
            double epsilon = (*s_general_info)[partPT->type].epsilon;
            glm::dvec3 ghostPos = evalGhostPos(partPT->pos, static_cast<GhostLoc>(i));

            // apply Lennard-Jones-Potential, if distance is lower than 2^(1/6)*sigma (membrane)
            glm::dvec3 lj = {0,0,0};
            double distance = glm::l2Norm(partPT->pos - ghostPos);
            if (distance < (1.122462048309372981433533049*sigma)){
                lj = ParticleCalculator::calcLennardJonesFij(partPT->pos, ghostPos, sigma, epsilon);
            }
            partVFID->force += lj;
        }
    }
}

/**
 * evaluate the position of the ghost particle on basis of actual particle position and location enum of ghost
 */
glm::dvec3 MembraneCalculator::evalGhostPos(const glm::dvec3 &pos, GhostLoc loc)
{
    switch(loc) {
        case GhostLoc::GHOST_LEFT:
            return glm::dvec3(2.0 * (m_left_corner.x) - pos.x, pos.y, pos.z);
        case GhostLoc::GHOST_RIGHT:
            return glm::dvec3(2.0 * (m_left_corner.x + m_domain_size.x) - pos.x, pos.y, pos.z);
        case GhostLoc::GHOST_DOWN:
            return glm::dvec3(pos.x, 2.0 * (m_left_corner.y) - pos.y, pos.z);
        case GhostLoc::GHOST_UP:
            return glm::dvec3(pos.x, 2.0 * (m_left_corner.y + m_domain_size.y) - pos.y, pos.z);
        case GhostLoc::GHOST_FRONT:
            return glm::dvec3(pos.x, pos.y, 2.0 * (m_left_corner.z) - pos.z);
        case GhostLoc::GHOST_BACK:
            return glm::dvec3(pos.x, pos.y, 2.0 * (m_left_corner.z + m_domain_size.z) - pos.z);
        default:
            throw std::runtime_error("error: invalid GhostLoc!");
    }
}

#endif
