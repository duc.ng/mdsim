#pragma once

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/norm.hpp>

#include "ParticleCalculator.h"
#include "Container/BasicContainer.h"

/**
 * Derived class of ParticleCalculator for particle calculations in worksheet2.
 * Worksheet2 portrayed a collision of 2D-particle-cuboids.
 */
class MolecularCalculator : public ParticleCalculator
{
public:
    MolecularCalculator(BasicContainer *cont, double delta_t);
    ~MolecularCalculator() override = default;

    void calculatePos() override;
    void calculateF() override;
    void calculateV() override;

private:

    /** Basic Container, where all particles are saved. */
    BasicContainer *s_cont;
    std::unique_ptr<BasicContainer::SPIteratorBasic> m_sp_it;
    std::unique_ptr<BasicContainer::NPIteratorBasic> m_np_it;

    /** Input variables. */
    double m_sigma, m_epsilon;
};

