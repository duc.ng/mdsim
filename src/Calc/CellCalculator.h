#pragma once

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/norm.hpp>

#include "ParticleCalculator.h"
#include "Container/CellContainer.h"


/**
 * Derived class of ParticleCalculator for particle calculations in worksheet3 and worksheet3.
 * Calculator uses linked cell algorithm and considers different type of boundary conditions.
 */
class CellCalculator : public ParticleCalculator
{
public:
    CellCalculator(CellContainer *cont, double delta_t,
                   bool gravity_enabled, double gravity, int gravity_axis);
    ~CellCalculator() override = default;

    void calculatePos() override;
    void calculateF() override;
    void calculateV() override;

private:
    void precalcSigmaEpsilon();

    void resetToGravity(Particle *p);
    void handleGhostParticles(Particle *p, const std::array<bool, 6> &ghostInfo);
    glm::dvec3 evalGhostPos(const glm::dvec3 &pos, GhostLoc loc);

private:

    /** Cell Container, where all particles are saved. */
    CellContainer *s_cont;
    std::unique_ptr<CellContainer::SPIteratorCell> m_sp_it;
    std::unique_ptr<CellContainer::NPIteratorCell> m_np_it;

    /** Input domain variables. */
    glm::dvec3 m_left_corner;
    glm::dvec3 m_domain_size;

    /** Input gravity variables. */
    bool m_isGravity;
    double m_gravity;
    int m_gravity_axis;

    /** Mixed variables */
    std::vector<double> m_mixed_sigmas;
    std::vector<double> m_mixed_epsilons;
};

