#include "AstroCalculator.h"

/**
 * Constructor of AstroleCalculator class.
 *
 * @param cont Container with particles
 * @param delta_t input variable
 */
AstroCalculator::AstroCalculator(BasicContainer *cont, double delta_t)
    : ParticleCalculator::ParticleCalculator(cont->getGeneralInfo(), delta_t),
      s_cont(cont)
{
    m_sp_it = s_cont->createSPIterator();
    m_np_it = s_cont->createNPIterator();
}

/**
 * Update x,y,z position of all (selected) particles with Velocity-Störmer-Verlet-Algorithm.
 */
void AstroCalculator::calculatePos()
{
    m_sp_it->resetIterator();
    while (!m_sp_it->isEnd()) {
        Particle *p = m_sp_it->deref();

        // set new position of particle
        p->setPos(ParticleCalculator::posStoermVerletAlg(*p));

        m_sp_it->increment();
    }
}

/**
 * Update force of all (selected) particles with use of Newtons Third Law.
 */
void AstroCalculator::calculateF()
{
    //backup current force in oldF
    m_sp_it->resetIterator();
    while (!m_sp_it->isEnd()) {
        Particle *p = m_sp_it->deref();
        p->setOldF(p->getF());
        p->setF(glm::dvec3(0.0f));

        m_sp_it->increment();
    }

    //compute new force (newtonsch)
    m_np_it->resetIterator();
    while (!m_np_it->isEnd()) {
        Particle *p1 = m_np_it->deref().first;
        Particle *p2 = m_np_it->deref().second;

        // simple force calculation from slides (force between two particles)
        glm::dvec3 deltaVec1 = p1->getPos() - p2->getPos();
        glm::dvec3 deltaVec2 = p2->getPos() - p1->getPos();
        glm::dvec3 f_ij = (((*s_general_info)[p1->getType()].mass * (*s_general_info)[p2->getType()].mass) /
                           (pow(glm::l2Norm(deltaVec1), 3))) * deltaVec2;

        // sum up all forces acting on particle i
        p1->setF(p1->getF() + f_ij);
        // and sum up negated on particle j (3. Newton's Law)
        p2->setF(p2->getF() - f_ij);

        m_np_it->increment();
    }
}


/**
 * Update velocity of all (selected) particles with Velocity-Störmer-Verlet-Algorithm.
 */
void AstroCalculator::calculateV()
{
    m_sp_it->resetIterator();
    while (!m_sp_it->isEnd()) {
        Particle *p = m_sp_it->deref();

        // set new velocity of particle
        p->setV(ParticleCalculator::veloStoermVerletAlg(*p));

        m_sp_it->increment();
    }
}

