/*
 * Particle.h
 *
 *  Created on: 23.02.2010
 *      Author: eckhardw
 */

#pragma once

#include <sstream>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/glm.hpp>
#include <glm/ext.hpp>
//#include <log4cxx/logger.h>

class CellCalculatorNoIter;

class Particle
{
public:
    explicit Particle(int type = 0);

    Particle(const Particle &other);

    Particle(
            // for visualization, we need always 3 coordinates
            // -> in case of 2d, we use only the first and the second
            const glm::dvec3& x_arg, const glm::dvec3& v_arg, int type_arg, int id_arg);

    // Assignment 4.3
    Particle(
            const glm::dvec3& x_arg,
            const glm::dvec3& v_arg,
            const glm::dvec3& f_arg,
            const glm::dvec3& old_f_arg,
            int type_arg,
            int id_arg);


    virtual ~Particle();

    friend class CellCalculatorNoIter;

    const glm::dvec3 &getPos() const { return m_pos; }
    void setPos(const glm::dvec3 &pos) { this->m_pos = pos; }

    const glm::dvec3 &getV() const { return m_velo; }
    void setV(const glm::dvec3 &velo) { this->m_velo = velo; }

    const glm::dvec3 &getF() const { return m_force; }
    void setF(const glm::dvec3 &force) { this->m_force = force; }

    const glm::dvec3 &getOldF() const { return m_old_f; }
    void setOldF(const glm::dvec3 &old_f) { this->m_old_f = old_f; }

    int32_t getType() const { return m_type; }
    int32_t getID() const { return m_id; }

    bool operator==(Particle &other);

    std::string toString();
    std::string toTidyString();

private:
    /** the position of the particle */
    glm::dvec3 m_pos;

    /** the velocity of the particle */
    glm::dvec3 m_velo;

    /** the force effective on this particle */
    glm::dvec3 m_force;

    /** the force which was effective on this particle */
    glm::dvec3 m_old_f;

    /** type of the particle. Use it for whatever you want (e.g. to separate
     * molecules belonging to different bodies, matters, and so on)
     */
    int32_t m_type;

    /** (hopefully) unique identifier of particle */
    int32_t m_id;

protected:
    //static log4cxx::LoggerPtr logger;
};

std::ostream &operator<<(std::ostream &stream, Particle &p);
