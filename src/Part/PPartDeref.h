#pragma once

#include <glm/glm.hpp>
#include <array>

struct PPartPT
{
    PPartPT(const glm::dvec3 &pos_, int32_t type_)
            : pos(pos_), type(type_) {}

    glm::dvec3 pos;
    int32_t type;
};

struct PPartVFID
{
    PPartVFID(const glm::dvec3 &velo_, const glm::dvec3 &old_f_, const glm::dvec3 &force_,
              int32_t id_, const std::array<bool, 6> &isGhost_)
            : velo(velo_), old_f(old_f_), force(force_), id(id_), isGhost(isGhost_) {}

    glm::dvec3 velo;
    glm::dvec3 old_f;
    glm::dvec3 force;
    int32_t id;
    std::array<bool, 6> isGhost;
    // in array: here alignment pad of 6 Bytes
};

struct PDeref
{
    PPartPT *partPT;
    PPartVFID *partVFID;
};
