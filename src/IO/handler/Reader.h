#pragma once

#include "IO/impl/FiletypeTXT.h"
#include "IO/impl/FiletypeGXT.h"
#include "IO/impl/FiletypeXML.h"
#include "IO/impl/FiletypeVTK.h"
#include "Container/BasicContainer.h"
#include "Container/ParallelCellCont.h"
#include "../../../XMLrelated/schemes/args.hxx"


enum AlgType {
    DIRECT_SUM,
    LINKED_CELL
};

struct ExtraThermostat {
    double temp_init;
    int n_thermostat;
    bool needs_init;
    int num_dim_of_sim = 0; // optional
    double temp_target; // default: = temp_init
    bool gradual;
    double temp_delta = std::numeric_limits<double>::infinity();   // default: = infinity
};

struct ExtraLinkedCell {
    glm::dvec3 center;
    glm::dvec3 domain_size;
    double cutoff_radius;
    std::array<BoundType, 6> bound_conds;
};

struct ArgsStruct {
    bool output_enabled;
    std::string output_base_name; // optional

    bool vis_enabled;
    float vis_color_range_min = 0; // optional
    float vis_color_range_max = 0; // optional
    unsigned output_freq;

    double t_end; double delta_t;
    std::string calculator_name;
    std::string input_filename;

    bool import_enabled;
    bool export_enabled;
    std::string import_export_fileURI;

    bool gravity_enabled;
    double gravity   = 0;  // optional
    int gravity_axis = 0;  // optional

    bool thermostat_enabled;
    std::shared_ptr<ExtraThermostat> extra_thermo = nullptr;

    AlgType algType;
    std::shared_ptr<ExtraLinkedCell> extra_lc = nullptr;

    // A5T1
    bool membrane_enabled;
    double fz_up;
    double k;
    double r_0;
    int x_length;
};


class Reader
{
public:
    Reader();
    ~Reader() = default;

    /**
     * read the specified file into the provided container
     *
     * @param particles
     * @param filename
     */
    std::shared_ptr<ParticleContainer> readFile(const std::string &filename);

    /**
    * read in the passed in params for Simulator from the provided arguments-file
    *
    * @param: name the uri of the file, that contains the configuration and arguments
    */
    void readArgs(const std::string &name);

    /**
     * getter for argument struct
     * @note readArgs() must be called before
     */
     std::shared_ptr<ArgsStruct> getArgs() { return m_args; }

private:
    static BoundType evalXMLBoundType(const std::string &xmlBoundType);

    std::unique_ptr<IOImplementation> m_io;
    std::shared_ptr<ArgsStruct> m_args;

protected:
    //static log4cxx::LoggerPtr logger;
};
