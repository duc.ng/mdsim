#include "Writer.h"

//log4cxx::LoggerPtr Writer::logger(log4cxx::Logger::getLogger("loggerWriter"));

Writer::Writer(unsigned num_parts, FileType type)
        : m_num_parts(num_parts)
{
    //LOG4CXX_DEBUG(logger, "Hello from Writer!");

    //choose implementation
    if(type == FileType::VTK) {
        m_io = std::make_unique<FiletypeVTK>(m_num_parts);
    }else if(type == FileType::XYZ) {
        m_io = std::make_unique<FiletypeXYZ>(m_num_parts);
    }else if(type == FileType::TXT) {
        m_io = std::make_unique<FiletypeTXT>(m_num_parts);
    }else {
        throw std::runtime_error("no valid output format found for Writer!");
    }
}


void Writer::plotParticle(const Particle &p)
{
    m_io->plotParticle(p);
}

void Writer::writeFile(const std::string &filename, int iteration)
{
    m_io->writeFile(filename, iteration);
}


/** Assignment 4.3: creates and inits a file by adding explanatory comments for exporting particles as TXT. */
void Writer::initExportFile(const std::string &filename)
{
    m_io->initExportFile(filename);
}


/** Assignment 4.3: appends a line to a specified TXT output file. */
void Writer::appendLineToFile(const std::string &line)
{
    m_io->appendLineToFile(line);
}

