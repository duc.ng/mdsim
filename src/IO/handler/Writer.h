#pragma once

#include "IO/impl/FiletypeVTK.h"
#include "IO/impl/FiletypeXYZ.h"
#include "IO/impl/FiletypeTXT.h"


enum FileType{
    VTK,
    XYZ,
    TXT
};

class Writer
{
public:
    Writer(unsigned num_parts, FileType type);
    ~Writer() = default;

    /**
    * plot type, mass, position, velocity and force of a particle.
    *
    * @param particle
    */
    void plotParticle(const Particle &p);

    /**
    * writes the final output file.
    *
    * @param filename the base name of the file to be written.
    * @param iteration the number of the current iteration,
    *        which is used to generate an unique filename
    */
    void writeFile(const std::string &filename, int iteration);


    /** creates and inits a file by adding explanatory comments for exporting particles as TXT. */
    void initExportFile(const std::string &filename);
    /** appends a line to a specified Export output file. */
    void appendLineToFile(const std::string &line);

private:
    unsigned m_num_parts;

    std::unique_ptr<IOImplementation> m_io;

protected:
    //static log4cxx::LoggerPtr logger;
};
