#include "Reader.h"

//log4cxx::LoggerPtr Reader::logger(log4cxx::Logger::getLogger("loggerReader"));

Reader::Reader()
{
    //LOG4CXX_DEBUG(logger, "Hello from Reader!");
}

std::shared_ptr<ParticleContainer> Reader::readFile(const std::string &filename)
{
    //LOG4CXX_DEBUG(logger, "received request to READ the following file: " << filename);

    //extract file extension
    int indexDot = filename.find_last_of('.');
    std::string extension = filename.substr(indexDot + 1);

    //choose implementation
    if(extension == "txt") {
        m_io = std::make_unique<FiletypeTXT>();
    }else if(extension == "gxt") {
        m_io = std::make_unique<FiletypeGXT>();
    }else if(extension == "xml") {
        m_io = std::make_unique<FiletypeXML>();
    }else if(extension == "vtu") {
        m_io = std::make_unique<FiletypeVTK>();
    }else {
        throw std::runtime_error("no valid input format found for Reader!");
    }

    // readFile to get particle vector and general info struct
    std::unique_ptr<ReadResult> readResult = m_io->readFile(filename);

    // import file if enabled and combine the two lists into one
    if(m_args->import_enabled) {
        //LOG4CXX_INFO(logger, "Simulation data is being imported...");

        m_io = std::make_unique<FiletypeTXT>(); // hard code TXT filetype
        std::unique_ptr <ReadResult> readResultImported = m_io->readFile(m_args->import_export_fileURI);
        auto final = std::make_unique<ReadResult>();

        // preallocate memory for particle vector and copy particles over
        final->particles.reserve(readResult->particles.size() + readResultImported->particles.size());
        final->particles.insert(final->particles.end(), readResult->particles.begin(), readResult->particles.end());
        final->particles.insert(final->particles.end(),
                                readResultImported->particles.begin(),
                                readResultImported->particles.end());

        // preallocate memory for general_info struct and copy info over
        final->general_info.reserve(readResult->general_info.size());
        final->general_info.insert(final->general_info.end(),
                                   readResult->general_info.begin(),
                                   readResult->general_info.end());

        // set readResult to new 'final' readResult (unique_ptr thus std::move)
        readResult = std::move(final);

        //LOG4CXX_INFO(logger, "Successfully imported all particles from file " << m_args->import_export_fileURI);
    }

    // return BasicContainer or CellContainer accordingly
    if(m_args->algType == AlgType::DIRECT_SUM) {
        return std::make_shared<BasicContainer>(
                std::move(readResult->particles),
                std::move(readResult->general_info));
    }else if(m_args->algType == AlgType::LINKED_CELL) {
        #ifdef _OPENMP
        return std::make_shared<ParallelCellCont>(
                readResult->particles,
                std::move(readResult->general_info),
                m_args->extra_lc->center, m_args->extra_lc->domain_size,
                m_args->extra_lc->bound_conds, m_args->extra_lc->cutoff_radius, 10);
        #else
        return std::make_shared<CellContainer>(
                std::move(readResult->particles),
                std::move(readResult->general_info),
                m_args->extra_lc->center, m_args->extra_lc->domain_size,
                m_args->extra_lc->bound_conds, m_args->extra_lc->cutoff_radius, 10);
        #endif
    }else {
        throw std::runtime_error("error: invalid algorithm type in arguments file");
    }
}


void Reader::readArgs(const std::string &name)
{
    //LOG4CXX_DEBUG(logger, "Hello from readArgs()! This is the filename: " << name  << ".");

    // initialize pointer to access XSD based XML data binding
    std::unique_ptr <eingabe_t> xml_input;
    xml_input = eingabe(name);
    m_args = std::make_shared<ArgsStruct>();

    // vtk output
    m_args->output_enabled = xml_input->config().out_enabled();
    m_args->output_base_name = xml_input->config().output_basename();

    // in-situ vis
    m_args->vis_enabled = xml_input->config().vis_enabled();
    m_args->vis_color_range_min = xml_input->config().vis_color_range_min();
    m_args->vis_color_range_max = xml_input->config().vis_color_range_max();

    // misc
    m_args->output_freq = xml_input->config().output_frequency();
    m_args->t_end = xml_input->config().t_end();
    m_args->delta_t = xml_input->config().delta_t();
    m_args->calculator_name = xml_input->config().calculator_name();
    m_args->input_filename = xml_input->config().eingabefile();

    m_args->import_enabled = xml_input->config().import_simulation_data();
    m_args->export_enabled = xml_input->config().export_simulation_data();
    m_args->import_export_fileURI = xml_input->config().import_export_fileURI();

    // gravity
    m_args->gravity_enabled = xml_input->config().gravity_enabled();
    m_args->gravity = xml_input->config().gravity_constant();
    if(xml_input->config().gravity_axis() == "x") {
        m_args->gravity_axis = 0;
    }else if(xml_input->config().gravity_axis() == "y") {
        m_args->gravity_axis = 1;
    }else if(xml_input->config().gravity_axis() == "z") {
        m_args->gravity_axis = 2;
    }else {
        throw std::runtime_error("error: invalid gravity axis given!");
    }

    // thermostat
    m_args->thermostat_enabled = xml_input->config().thermostat_enabled();
    if(m_args->thermostat_enabled) {
        if(!xml_input->extra_thermostat().present())
            throw std::runtime_error("error: no extra_thermostat present in arguments XML!");
        m_args->extra_thermo = std::make_shared<ExtraThermostat>();
        m_args->extra_thermo->temp_init = xml_input->extra_thermostat()->temp_init();
        m_args->extra_thermo->n_thermostat = xml_input->extra_thermostat()->n_thermostat();
        m_args->extra_thermo->needs_init = xml_input->extra_thermostat()->needs_init();
        m_args->extra_thermo->num_dim_of_sim = xml_input->extra_thermostat()->num_dim_of_sim();
        if(xml_input->extra_thermostat()->temp_target().present())
            m_args->extra_thermo->temp_target = xml_input->extra_thermostat()->temp_target().get();
        else
            m_args->extra_thermo->temp_target = m_args->extra_thermo->temp_init;

        m_args->extra_thermo->gradual = xml_input->extra_thermostat()->gradual();
        if(m_args->extra_thermo->gradual) {
            if(!xml_input->extra_thermostat()->temp_delta().present())
                throw std::runtime_error("error: temp_delta not present in arguments XML!");
            m_args->extra_thermo->temp_delta = xml_input->extra_thermostat()->temp_delta().get();
        }
    }

    // algorithm
    if(xml_input->config().algorithm() == "direct_sum") {
        // if direct_sum -> set and do nothing
        m_args->algType = AlgType::DIRECT_SUM;
    }else if(xml_input->config().algorithm() == "linked_cell") {
        if(!xml_input->extra_linked_cell().present())
            throw std::runtime_error("error: no extra_linked_cell present in arguments XML!");
        // if linked_cell and extra_linked_cell present -> set and read extra_linked_cell
        m_args->algType = AlgType::LINKED_CELL;
        m_args->extra_lc = std::make_shared<ExtraLinkedCell>();
        m_args->extra_lc->center = glm::dvec3(
                xml_input->extra_linked_cell()->center_x(),
                xml_input->extra_linked_cell()->center_y(),
                xml_input->extra_linked_cell()->center_z()
        );
        m_args->extra_lc->domain_size = glm::dvec3(
                xml_input->extra_linked_cell()->domain_x(),
                xml_input->extra_linked_cell()->domain_y(),
                xml_input->extra_linked_cell()->domain_z()
        );
        m_args->extra_lc->cutoff_radius = xml_input->extra_linked_cell()->cutoffradius();
        m_args->extra_lc->bound_conds[GhostLoc::GHOST_LEFT] =
                evalXMLBoundType(xml_input->extra_linked_cell()->left_bound_cond());
        m_args->extra_lc->bound_conds[GhostLoc::GHOST_RIGHT] =
                evalXMLBoundType(xml_input->extra_linked_cell()->right_bound_cond());
        m_args->extra_lc->bound_conds[GhostLoc::GHOST_DOWN] =
                evalXMLBoundType(xml_input->extra_linked_cell()->down_bound_cond());
        m_args->extra_lc->bound_conds[GhostLoc::GHOST_UP] =
                evalXMLBoundType(xml_input->extra_linked_cell()->up_bound_cond());
        m_args->extra_lc->bound_conds[GhostLoc::GHOST_FRONT] =
                evalXMLBoundType(xml_input->extra_linked_cell()->front_bound_cond());
        m_args->extra_lc->bound_conds[GhostLoc::GHOST_BACK] =
                evalXMLBoundType(xml_input->extra_linked_cell()->back_bound_cond());
    }

    // A5T1
    m_args->membrane_enabled = xml_input->config().membrane_enabled();
    m_args->fz_up = xml_input->config().fz_up();
    m_args->k = xml_input->config().k();
    m_args->r_0 = xml_input->config().r_0();
    m_args->x_length = xml_input->config().x_length();

}

BoundType Reader::evalXMLBoundType(const std::string &xmlBoundType)
{
    if(xmlBoundType == "outflow")
        return BoundType::OUTFLOW_BOUND;
    else if(xmlBoundType == "reflect")
        return BoundType::REFLECT_BOUND;
    else if(xmlBoundType == "periodic")
        return BoundType::PERIODIC_BOUND;
    else
        throw std::runtime_error("error: invalid boundary type found while parsing XML args!");
}

