#pragma once

//#include <log4cxx/logger.h>

#include <fstream>

#include "Container/ParticleContainer.h"
#include "utils/vtk-unstructured.h"

struct ReadResult {
    std::vector<Particle> particles;
    std::vector<GeneralParticleInfo> general_info;
};

class IOImplementation
{
public:
    IOImplementation() = default;
    virtual ~IOImplementation() = default;

    /**
    * read in a specified file
    */
    virtual std::unique_ptr<ReadResult> readFile(const std::string &filename) = 0;

    /**
    * plot type, mass, position, velocity and force of a particle.
    * @note: initializeOutput() must have been called before.
    */
    virtual void plotParticle(const Particle &p) = 0;

    /**
    * writes the final output file.
    *
    * @param filename the base name of the file to be written.
    * @param iteration the number of the current iteration,
    *        which is used to generate an unique filename
    */
    virtual void writeFile(const std::string &filename, int iteration) = 0;




    /** Assignment 4.3: creates and inits a file by adding explanatory comments for exporting particles as TXT. */
    virtual void initExportFile(const std::string &filename) = 0;
    /** Assignment 4.3: appends a line to a specified TXT output file. */
    virtual void appendLineToFile(std::string line) = 0;

protected:
    //static log4cxx::LoggerPtr logger;
};





