#include "FiletypeTXT.h"

std::unique_ptr<ReadResult> FiletypeTXT::readFile(const std::string &filename)
{
    // our particle vector to fill + general info
    auto result = std::make_unique<ReadResult>();
    result->general_info.resize(1);

    // all the values to be read from .gxt file
    glm::dvec3 x = {0, 0, 0};
    glm::dvec3 v = {1, 1, 1};
    double m = 1;
    int num_particles = 0;

    // additional values Assignment 4.3
    glm::dvec3 f = {0, 0, 10};
    glm::dvec3 old_f = {0, 0, 10};
    int type = 0;
    int id = -1;
    bool isImport = false;


    // Read file and save in lines to tmp_string
    std::ifstream input_file(filename);
    std::string tmp_string;
    if(input_file.is_open()) {
        // skip comments at start
        getline(input_file, tmp_string);
        //LOG4CXX_DEBUG(logger, "Read line: " << tmp_string);
        while(tmp_string.empty() || tmp_string[0] == '#') {
            getline(input_file, tmp_string);
            //LOG4CXX_DEBUG(logger, "Read line: " << tmp_string);
        }

        // check if first line is "IMPORTEXPORT"
        if( tmp_string.substr(0, 12) == "IMPORTEXPORT"){
            isImport = true;
            //LOG4CXX_DEBUG(logger, "this is an import!");
            getline(input_file, tmp_string);
        }


        // read num_particles and reserve space in std::vector
        std::istringstream numstream(tmp_string);
        numstream >> num_particles;
        result->particles.reserve(num_particles);
        //LOG4CXX_DEBUG(logger, "Reading " << num_particles << ".");

        // read all particle lines
        getline(input_file, tmp_string);
        //LOG4CXX_DEBUG(logger, "Read line: " << tmp_string);
        for(int i = 0; i < num_particles; i++) {
            std::istringstream datastream(tmp_string);

            for(int j = 0; j < 3; j++)
                datastream >> x[j]; // get x value triple
            for(int j = 0; j < 3; j++)
                datastream >> v[j]; // get v value triple


            // read in additional values if this is an import
            // COORD    VELO    FORCE   old_F   TYPE    ID
            if(isImport){
                for(int j = 0; j < 3; j++)
                    datastream >> f[j]; // get f value triple
                for(int j = 0; j < 3; j++)
                    datastream >> old_f[j]; // get old_f value triple

                datastream >> type; // get type of molecule
                datastream >> id; // get type of molecule
            } else {
                if(datastream.eof()) {
                    //LOG4CXX_ERROR(logger, "Error reading file: eof reached unexpectedly reading from line " << i << "!");
                    exit(-1);
                }
                // get mass value from file and hardcode sigma and epsilon
                datastream >> m;
            }

            // get mass value from file and hardcode sigma and epsilon
            result->general_info[0].mass = m;
            result->general_info[0].sigma = 1.0;
            result->general_info[0].epsilon = 5.0;


            // generate Particle based on the available information
            if(isImport){
                Particle p(x, v, f, old_f, type, id);
                result->particles.push_back(p);
            } else {
               Particle p(x, v, 0, i); // hardcode type to 0
               result->particles.push_back(p);
            }

            getline(input_file, tmp_string);
            //LOG4CXX_DEBUG(logger, "Read line: " << tmp_string);
        }
    } else {
        //LOG4CXX_ERROR(logger, "Error: could not open file " << filename);
        exit(-1);
    }

    // return final particle vector + general info
    return result;
}

void FiletypeTXT::plotParticle(const Particle &p)
{
    throw std::runtime_error("no TXT updating!");
}

void FiletypeTXT::writeFile(const std::string &filename, int iteration)
{
    throw std::runtime_error("no TXT writing!");
}


/** Assignment 4.3: creates and inits a file by adding explanatory comments for exporting particles as TXT. */
void FiletypeTXT::initExportFile(const std::string &filename)
{
    this->TXTfilename = filename;

    std::ofstream out;
    out.open (filename); // filename does contain the extension already!
    out <<
        "# \n" <<
        "# file format:\n" <<
        "# Lines of comment start with '#' and are only allowed at the beginning of the file\n" <<
        "# Empty lines are not allowed.\n" <<
        "# The first line not being a comment has to be one integer, indicating the number of\n" <<
        "# molecule data sets.\n" <<
        "#\n" <<
        "# the first non-empty line marks the number of molecules\n" <<
        "#\n" <<
        "# Molecule data consists of\n" <<
        "# * xyz-coordinates (3 double values)\n" <<
        "# * velocities (3 double values)\n" <<
        "# * forces (3 double values)\n" <<
        "# * forces_old (3 double values)\n" <<
        "# * molecule type\n" <<
        "# * molecule id\n" <<
        "#\n" <<
        "# COORD          VELO        FORCE       old_F       TYPE         ID\n" <<
        "IMPORTEXPORT\n";

    out.close();

    //LOG4CXX_DEBUG(logger, "initialized " << this->TXTfilename << " successfully!");
}

/** Assignment 4.3: appends a line to a specified TXT output file. */
void FiletypeTXT::appendLineToFile(std::string line)
{
    std::ofstream out;
    out.open (this->TXTfilename, std::ios_base::app); // filename does contain the extension already!
    out << line << "\n";
    out.close();

    //LOG4CXX_DEBUG(logger, "appended line to EXPORT file successfully");
}
