#include "ParticleGenerator.h"

//log4cxx::LoggerPtr ParticleGenerator::logger(log4cxx::Logger::getLogger("loggerParticleGenerator"));

ParticleGenerator::ParticleGenerator(std::vector<Particle> *particles)
        : s_particles(particles) {}


int ParticleGenerator::generateCuboid(glm::ivec3 dimensions,
                                      glm::dvec3 x_corner,
                                      glm::dvec3 velo,
                                      int type, int id_offset,
                                      double distance, double bm_factor, int bm_dimension)
{
    //create particles for each cuboid
    for(int z_ind = 0; z_ind < dimensions.z; z_ind++) {
        for(int y_ind = 0; y_ind < dimensions.y; y_ind++) {
            for(int x_ind = 0; x_ind < dimensions.x; x_ind++) {
                glm::dvec3 coord = {(x_ind * distance) + x_corner.x,
                                    (y_ind * distance) + x_corner.y,
                                    (z_ind * distance) + x_corner.z};

                //create particle (emplace particle in list)
                s_particles->emplace_back(coord, velo, type, id_offset++);

                //initialize velocity by Brownian Motion
                MaxwellBoltzmannDistribution(s_particles->back(), bm_factor, bm_dimension);
            }
        }
    }

    return id_offset;
}


//modified Bresenham2 algorithm
int ParticleGenerator::generateSphere(int numOnRadius,
                                       glm::dvec3 center,
                                       glm::dvec3 velo,
                                       int type, int id_offset,
                                       double distance, double bm_factor, int bm_dimension)
{
    //initialize variables for createParticle();
    m_velo = velo;
    m_type = type;
    m_center = center;
    m_distance = distance;
    m_bm_factor = bm_factor;
    m_bm_dimension = bm_dimension;

    //initialize variables for sphere
    int x = 0;
    int y = numOnRadius;
    double f = 1 - numOnRadius;
    double east = 1;
    double southeast = 2 - numOnRadius - numOnRadius;

    //particles in (0,0) + x-axis + y-axis
    s_particles->push_back(createParticle(0, 0, id_offset++));
    for(int i = 1; i <= y; i++) {
        s_particles->push_back(createParticle(x,  i, id_offset++));
        s_particles->push_back(createParticle(i,  x, id_offset++));
        s_particles->push_back(createParticle(x, -i, id_offset++));
        s_particles->push_back(createParticle(-i, x, id_offset++));
    }

    //loop non-axis particles
    while(x < y) {
        //next x,y coordinates
        if(f < 0) {
            f = f + east;
            southeast = southeast + 2;
        }else {
            f = f + southeast;
            y = y - 1;
            southeast = southeast + 4;
        }
        x = x + 1;
        east = east + 2;

        //particles on diagonals
        s_particles->push_back(createParticle( x,  x, id_offset++));
        s_particles->push_back(createParticle(-x,  x, id_offset++));
        s_particles->push_back(createParticle( x, -x, id_offset++));
        s_particles->push_back(createParticle(-x, -x, id_offset++));

        //remaining particles
        for(int i = x + 1; i <= y; i++) {
            s_particles->push_back(createParticle( x,  i, id_offset++));
            s_particles->push_back(createParticle( i,  x, id_offset++));
            s_particles->push_back(createParticle(-x,  i, id_offset++));
            s_particles->push_back(createParticle( i, -x, id_offset++));
            s_particles->push_back(createParticle(-i,  x, id_offset++));
            s_particles->push_back(createParticle( x, -i, id_offset++));
            s_particles->push_back(createParticle(-i, -x, id_offset++));
            s_particles->push_back(createParticle(-x, -i, id_offset++));
        }
    }

    return id_offset;
}

Particle ParticleGenerator::createParticle(int x, int y, int id)
{
    double xCoord = m_center.x + (x * m_distance);
    double yCoord = m_center.y + (y * m_distance);
    Particle p(glm::dvec3(xCoord, yCoord, 0), m_velo, m_type, id);
    MaxwellBoltzmannDistribution(p, m_bm_factor, m_bm_dimension);
    return p;
}
