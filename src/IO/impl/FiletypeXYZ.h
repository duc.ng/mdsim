#pragma once

#include <iomanip>
#include "IOImplementation.h"

class FiletypeXYZ : public IOImplementation
{
public:
    explicit FiletypeXYZ(unsigned size) { m_particles.reserve(size); }
    ~FiletypeXYZ() override = default;

    /**
    * read in a specified file
    *
    * @note: NOT IN USE
    */
    std::unique_ptr<ReadResult> readFile(const std::string &filename) override;

    /**
    * plot type, mass, position, velocity and force of a particle.
    *
    * @note: initializeOutput() must have been called before.
    */
    void plotParticle(const Particle &p) override;

    /**
    * writes the final output file.
    *
    * @param filename the base name of the file to be written.
    * @param iteration the number of the current iteration,
    *        which is used to generate an unique filename
    */
    void writeFile(const std::string &filename, int iteration) override;


    /** Assignment 4.3: creates and inits a file by adding explanatory comments for exporting particles as TXT. */
    void initExportFile(const std::string &filename) override;
    /** Assignment 4.3: appends a line to a specified TXT output file. */
    void appendLineToFile(std::string line) override;

private:
    std::vector<Particle> m_particles;
};


