#pragma once

#include "Container/ParticleContainer.h"
#include "utils/MaxwellBoltzmannDistribution.h"
//#include <log4cxx/logger.h>


class ParticleGenerator
{
public:
    explicit ParticleGenerator(std::vector<Particle> *particles);
    ~ParticleGenerator() = default;

    /**
    * create a cuboid from the passed in params
    *
    */
    int generateCuboid(glm::ivec3 dimensions,
                       glm::dvec3 x_corner,
                       glm::dvec3 velo,
                       int type, int id_offset,
                       double distance, double bm_factor, int bm_dimension);

    /**
    * create a sphere from the passed in params
    */
    int generateSphere(int numOnRadius,
                       glm::dvec3 center,
                       glm::dvec3 velo,
                       int type, int id_offset,
                       double distance, double bm_factor, int bm_dimension);

private:
    Particle createParticle(int x, int y, int id);

private:
    std::vector<Particle> *s_particles;
    //static log4cxx::LoggerPtr logger;

    glm::dvec3 m_velo;
    int m_type;
    glm::dvec3 m_center;
    double m_distance;
    double m_bm_factor;
    int m_bm_dimension;
};

