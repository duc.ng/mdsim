#include "FiletypeGXT.h"

std::unique_ptr<ReadResult> FiletypeGXT::readFile(const std::string &filename)
{
    // particle vector to fill + general info
    auto result = std::make_unique<ReadResult>();
    result->general_info.resize(1);

    // all the values to be read from .gxt file
    glm::dvec3 x_corner = {0, 0, 0};
    glm::dvec3 velo = {1, 1, 1};
    glm::ivec3 dimensions = {1, 1, 1};
    double number_cuboids = 1;

    double brownian_motion = 1;
    double distance = 1;


    // Read file and save in lines to tmp_string
    std::ifstream input_file(filename);
    std::string tmp_string;
    if (input_file.is_open()) {
        // skip comments at start
        do {
            getline(input_file, tmp_string);
            //LOG4CXX_DEBUG(logger, "Read line: " << tmp_string);
        } while (tmp_string.empty() || tmp_string[0] == '#');

        // Read sigma
        readLine(&result->general_info[0].sigma, tmp_string);
        // Read epsilon
        getline(input_file, tmp_string);
        readLine(&result->general_info[0].epsilon, tmp_string);
        // Read mass
        getline(input_file, tmp_string);
        readLine(&result->general_info[0].mass, tmp_string);

        // Read Brownian-Motion factor
        getline(input_file, tmp_string);
        readLine(&brownian_motion, tmp_string);
        // Read h (distance)
        getline(input_file, tmp_string);
        readLine(&distance, tmp_string);
        // Read number of cuboids
        getline(input_file, tmp_string);
        readLine(&number_cuboids, tmp_string);

        // Read cuboid data
        getline(input_file, tmp_string);
        int id_offset = 0;
        ParticleGenerator pg(&result->particles);
        //LOG4CXX_DEBUG(logger, "Read line: " << tmp_string);
        for (int i = 0; i < number_cuboids; i++) {
            std::istringstream datastream(tmp_string);
            for (int j = 0; j < 3; j++)
                datastream >> x_corner[j]; // get lower left front-side corner
            for (int j = 0; j < 3; j++)
                datastream >> velo[j]; // get v value of cuboid

            if (datastream.eof()) {
                //LOG4CXX_ERROR(logger, "Error reading file: eof reached unexpectedly reading from line " << i);
                exit(-1);
            }

            for (int j = 0; j < 3; j++)
                datastream >> dimensions[j]; // get number of particles in every dimension

            // generate the current cuboid
            pg.generateCuboid(dimensions, x_corner, velo, 0, id_offset, distance, brownian_motion, 2);
            id_offset += dimensions.x * dimensions.y * dimensions.z;

            // get next line
            getline(input_file, tmp_string);
            //LOG4CXX_DEBUG(logger, "Read line: " << tmp_string);
        }
    } else {
        //LOG4CXX_ERROR(logger, "Error: could not open file " << filename);
        exit(-1);
    }

    // return particle vector + general info
    return result;
}

void FiletypeGXT::plotParticle(const Particle &p)
{
    throw std::runtime_error("no GXT updating!");
}

void FiletypeGXT::writeFile(const std::string &filename, int iteration)
{
    throw std::runtime_error("no GXT writing!");
}

void FiletypeGXT::readLine(double *target, const std::string &tmp_string)
{
    //LOG4CXX_DEBUG(logger, "Read line: " << tmp_string);
    std::istringstream stream(tmp_string);
    stream >> *target;
    //LOG4CXX_DEBUG(logger, "Reading " << *target << ".");
}

// Assignment 4.3
void FiletypeGXT::initExportFile(const std::string &filename) {}
void FiletypeGXT::appendLineToFile(std::string line) {}
