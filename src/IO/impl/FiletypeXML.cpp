#include "FiletypeXML.h"

std::unique_ptr<ReadResult> FiletypeXML::readFile(const std::string &filename)
{
    // our particle vector to fill + general info
    auto result = std::make_unique<ReadResult>();

    // xml input
    std::unique_ptr<part_data_t> xml_input;
    xml_input = part_data(filename);

    // brownian motion
    double brownian_motion = xml_input->config_data().brownian_motion();
    //LOG4CXX_DEBUG(logger, "Reading brownian_motion: " << brownian_motion << ".");

    // number of dimensions of simulation (2d or 3d)
    int num_dim_of_sim = xml_input->config_data().num_dim_of_sim();
    //LOG4CXX_DEBUG(logger, "Reading num_dim_of_sim: " << num_dim_of_sim << ".");

    // number of particle types (resize vector general_info to what we need)
    unsigned num_particle_types = xml_input->config_data().num_particle_types();
    result->general_info.resize(num_particle_types);
    //LOG4CXX_DEBUG(logger, "Reading num_particle_types: " << num_particle_types << ".");

    // number of objects
    unsigned num_objects = xml_input->config_data().num_objects();
    //LOG4CXX_DEBUG(logger, "Reading num_objects: " << num_objects << ".");

    // number of cuboids
    unsigned num_cuboids = xml_input->config_data().num_cuboids();
    //LOG4CXX_DEBUG(logger, "Reading num_cuboids: " << num_cuboids << ".");

    // number of spheres
    unsigned num_spheres = xml_input->config_data().num_spheres();
    //LOG4CXX_DEBUG(logger, "Reading num_spheres: " << num_spheres << ".");

    // get general information of all particles types
    for(unsigned i=0; i<num_particle_types; i++) {
        result->general_info[i].sigma   = xml_input->general_part_info().at(i).sigma();
        result->general_info[i].epsilon = xml_input->general_part_info().at(i).epsilon();
        result->general_info[i].mass    = xml_input->general_part_info().at(i).mass();
    }

    // set up particle generator to fill in particle list
    ParticleGenerator pg(&result->particles);
    int id_offset = 0;


    /////////////
    // Cuboids //
    /////////////

    for(unsigned i = 0; i < num_cuboids; i++) {
        glm::dvec3 left_corner = {0, 0, 0};
        glm::dvec3 velo = {1, 1, 1};
        glm::ivec3 dimensions = {1, 1, 1};

        // get cuboid stuff to m_general_info
        int type = xml_input->cuboid().at(i).type();

        // distance
        double distance = xml_input->cuboid().at(i).distance();

        // left corner
        left_corner.x = xml_input->cuboid().at(i).left_corner().left_corner_x();
        left_corner.y = xml_input->cuboid().at(i).left_corner().left_corner_y();
        left_corner.z = xml_input->cuboid().at(i).left_corner().left_corner_z();

        // velocity
        velo.x = xml_input->cuboid().at(i).velocity().velo_x();
        velo.y = xml_input->cuboid().at(i).velocity().velo_y();
        velo.z = xml_input->cuboid().at(i).velocity().velo_z();

        // get number of particles in every dimension
        dimensions.x = xml_input->cuboid().at(i).dimensions().dim_x();
        dimensions.y = xml_input->cuboid().at(i).dimensions().dim_y();
        dimensions.z = xml_input->cuboid().at(i).dimensions().dim_z();

        // generate the current cuboid
        id_offset = pg.generateCuboid(
                dimensions, left_corner, velo,
                type, id_offset,
                distance, brownian_motion, num_dim_of_sim);
    }


    /////////////
    // Spheres //
    /////////////

    for(unsigned i = 0; i < num_spheres; i++) {
        // type
        int type = xml_input->sphere_2Dz().at(i).type();
        // distance
        double distance = xml_input->sphere_2Dz().at(i).distance();
        // radius
        int radius = xml_input->sphere_2Dz().at(i).radius();

        // center
        glm::dvec3 center = {0, 0, 0};
        center.x = xml_input->sphere_2Dz().at(i).center().center_x();
        center.y = xml_input->sphere_2Dz().at(i).center().center_y();
        center.z = xml_input->sphere_2Dz().at(i).center().center_z();

        // velocity
        glm::dvec3 velo = {1, 1, 1};
        velo.x = xml_input->sphere_2Dz().at(i).velocity().velo_x();
        velo.y = xml_input->sphere_2Dz().at(i).velocity().velo_y();
        velo.z = xml_input->sphere_2Dz().at(i).velocity().velo_z();

        // generate the current cuboid
        id_offset = pg.generateSphere(
                radius, center, velo,
                type, id_offset,
                distance, brownian_motion, num_dim_of_sim);
    }


    // return final particle vector + general info
    return result;
}


void FiletypeXML::plotParticle(const Particle &p)
{
    throw std::runtime_error("no XML updating!");
}

void FiletypeXML::writeFile(const std::string &filename, int iteration)
{
    throw std::runtime_error("no XML writing!");
}

// Assignment 4.3
void FiletypeXML::initExportFile(const std::string &filename) {}
void FiletypeXML::appendLineToFile(std::string line) {}
