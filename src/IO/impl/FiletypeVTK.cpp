#include <regex>
#include "FiletypeVTK.h"

void FiletypeVTK::initializeOutput(unsigned numParticles)
{
    vtkFile = new VTKFile_t("UnstructuredGrid");

    // per point, we add type, position, velocity and force
    PointData pointData;
    DataArray_t mass(type::Float32, "mass", 1);
    DataArray_t velocity(type::Float32, "velocity", 3);
    DataArray_t forces(type::Float32, "force", 3);
    DataArray_t type(type::Int32, "type", 1);
    pointData.DataArray().push_back(mass);
    pointData.DataArray().push_back(velocity);
    pointData.DataArray().push_back(forces);
    pointData.DataArray().push_back(type);

    CellData cellData; // we don't have cell data => leave it empty

    // 3 coordinates
    Points points;
    DataArray_t pointCoordinates(type::Float32, "points", 3);
    points.DataArray().push_back(pointCoordinates);

    Cells cells; // we don't have cells, => leave it empty
    // for some reasons, we have to add a dummy entry for paraview
    DataArray_t cells_data(type::Float32, "types", 0);
    cells.DataArray().push_back(cells_data);

    PieceUnstructuredGrid_t piece(pointData, cellData, points, cells, numParticles, 0);
    UnstructuredGrid_t unstructuredGrid(piece);
    vtkFile->UnstructuredGrid(unstructuredGrid);
}

std::unique_ptr<ReadResult> FiletypeVTK::readFile(const std::string &filename)
{
    throw std::runtime_error("no VTK reading!");
}

void FiletypeVTK::plotParticle(const Particle &p)
{
    if (vtkFile->UnstructuredGrid().present()) {
        //LOG4CXX_TRACE(logger, "UnstructuredGrid is present");

    } else {
        //LOG4CXX_ERROR(logger, "ERROR: No UnstructuredGrid present");
    }

    PointData::DataArray_sequence &pointDataSequence =
            vtkFile->UnstructuredGrid()->Piece().PointData().DataArray();
    PointData::DataArray_iterator dataIterator = pointDataSequence.begin();

    dataIterator->push_back(1.0);
    //LOG4CXX_TRACE(logger, "Appended mass data in: " << dataIterator->Name());

    dataIterator++;
    dataIterator->push_back(p.getV()[0]);
    dataIterator->push_back(p.getV()[1]);
    dataIterator->push_back(p.getV()[2]);
    //LOG4CXX_TRACE(logger, "Appended velocity data in: " << dataIterator->Name());

    dataIterator++;
    dataIterator->push_back(p.getF()[0]);
    dataIterator->push_back(p.getF()[1]);
    dataIterator->push_back(p.getF()[2]);
    //LOG4CXX_TRACE(logger, "Appended force data in: " << dataIterator->Name());

    
    dataIterator++;
    dataIterator->push_back(p.getType());

    Points::DataArray_sequence &pointsSequence =
            vtkFile->UnstructuredGrid()->Piece().Points().DataArray();
    Points::DataArray_iterator pointsIterator = pointsSequence.begin();
    pointsIterator->push_back(p.getPos()[0]);
    pointsIterator->push_back(p.getPos()[1]);
    pointsIterator->push_back(p.getPos()[2]);
}

void FiletypeVTK::writeFile(const std::string &filename, int iteration)
{
    std::stringstream strstr;
    strstr << filename << "_" << std::setfill('0') << std::setw(10) << iteration << ".vtu";

    std::ofstream file(strstr.str().c_str());
    VTKFile(file, *vtkFile);
    delete vtkFile;
}

// Assignment 4.3
void FiletypeVTK::initExportFile(const std::string &filename) {}
void FiletypeVTK::appendLineToFile(std::string line) {}