#pragma once

#include "IOImplementation.h"
#include "../../../XMLrelated/schemes/args.hxx"
#include "../../../XMLrelated/schemes/part_data.hxx"
#include "utils/MaxwellBoltzmannDistribution.h"
#include "ParticleGenerator.h"
#include "Container/CellContainer.h"

class FiletypeXML : public IOImplementation
{
public:
    FiletypeXML() = default;
    ~FiletypeXML() override = default;

    /**
     * read in the specified XML file; based on Codesynthesis XSD
     *
     * @param particles
     * @param filename
     */
    std::unique_ptr<ReadResult> readFile(const std::string &filename) override;


    /**
    * plot type, mass, position, velocity and force of a particle.
    *
    * @note NOT IN USE
    */
    void plotParticle(const Particle &p) override;

    /**
    * writes the final output file.
    *
    * @note NOT IN USE
    *
    * @param filename the base name of the file to be written.
    * @param iteration the number of the current iteration,
    *        which is used to generate an unique filename
    */
    void writeFile(const std::string &filename, int iteration) override;


    /** Assignment 4.3: creates and inits a file by adding explanatory comments for exporting particles as TXT. */
    void initExportFile(const std::string &filename) override;
    /** Assignment 4.3: appends a line to a specified TXT output file. */
    void appendLineToFile(std::string line) override;
};
