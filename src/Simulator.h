#pragma once
//#include <log4cxx/logger.h>
//#include <log4cxx/propertyconfigurator.h>

//#include <framework.h>
#include <omp.h>

#include "Container/ParticleContainer.h"
#include "Container/ParallelCellCont.h"

#include "Container/Iterator/SPIterator.h"

#include "Calc/ParticleCalculator.h"
#include "Calc/PCellCalculator.h"
#include "Calc/CellCalculator.h"
#include "Calc/MembraneCalculator.h"
#include "Calc/MolecularCalculator.h"
#include "Calc/AstroCalculator.h"

#include "IO/handler/Reader.h"
#include "IO/handler/Writer.h"

#include "Thermo/StandardThermo.h"
#include "Thermo/ParallelThermo.h"


class Simulator
{
public:
    Simulator(int argc, char *argv[]);
    ~Simulator();

    void main_loop();

protected:
    //static log4cxx::LoggerPtr logger;

private:

    std::shared_ptr<ParticleCalculator> createCalculator();

    /**
     * plot the particles to the specified file format
     */
    void plotParticles(int iteration);


    std::shared_ptr<ArgsStruct> m_args; // shared with Reader
    std::shared_ptr<ParticleContainer> m_cont; // shared so dynamic_cast is possible
    std::shared_ptr<ParticleCalculator> m_partCalc; // shared so dynamic_cast is possible
    //std::unique_ptr<Framework> m_fr;
    std::unique_ptr<Thermostat> m_thermostat;
};
