#pragma once

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <vector>
#include <array>

#include "Part/PPartDeref.h"

class PCell;
struct PHullCell
{
    PHullCell() = default;
    PHullCell(PCell *cell, const glm::dvec3 shift_)
            : cell(cell), shift(shift_) {}

    PCell *cell;         //!< pointer to actual adjacent cell
    glm::dvec3 shift;   //!< shift added on particle positions (non-zero when peridic hull cell)
};

struct PCell
{
    PCell(int cell_id_, unsigned capacity)
            : cell_id(cell_id_), isBoundary(false),
              newt_hull({}), hull_size(0),
              num_parts(0)
    {
        parts_pt.reserve(capacity);
        parts_vfid.reserve(capacity);
    }

    void push_back(PPartPT *partPT, PPartVFID *partVFID)
    {
        parts_pt.push_back(*partPT);
        parts_vfid.push_back(*partVFID);
        num_parts++;
    }

    void erase(int index)
    {
        parts_pt.erase(parts_pt.begin() + index);
        parts_vfid.erase(parts_vfid.begin() + index);
        num_parts--;
    }

    //! index in cell grid (sort of an id)
    int cell_id;

    //! flag set if is a boundary cell
    bool isBoundary;

    //! a hull of adjacent cells
    std::array<PHullCell, 14> newt_hull;
    unsigned hull_size;

    //! a dynamic array of cellparticles
    std::vector<PPartPT> parts_pt;
    std::vector<PPartVFID> parts_vfid;
    int num_parts;
};
