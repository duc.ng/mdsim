#pragma once

#include "ParticleContainer.h"
#include "Cell.h"

/**
 * a container for the linked cell algorithm, holding information of the cell grid and the particles in it
 * -> it provides a interface to iterators
 */
class CellContainer : public ParticleContainer
{
public:
    CellContainer(std::vector<Particle> particles,
                  std::vector<GeneralParticleInfo> general_info,
                  glm::dvec3 center, glm::dvec3 domain_size,
                  const std::array<BoundType, 6> &bound_conds,
                  double cut_off, unsigned cell_start_capacity);
    ~CellContainer() override;

    void updateCells();

    Cell *operator[](int ind);
    Cell *operator[](glm::ivec3 ind);
    int size() const override { return m_num_parts; }

    const glm::dvec3 &getLeftCorner() const { return m_left_corner; }
    const glm::dvec3 &getDomainSize() const { return m_domain_size; }
    const glm::ivec3 &getDimensions() const { return m_dimensions; }
    const glm::dvec3 &getMeasures() const { return m_measures; }
    std::vector<GeneralParticleInfo> *getGeneralInfo() { return &m_general_info; }

    //----------------------- Calculators ---------------------//
    friend class CellCalculatorNoIter;
    //----------------------- Calculators ---------------------//

    //----------------------- iterators -----------------------//
    class SPIteratorCell : public SPIterator
    {
    public:
        explicit SPIteratorCell(CellContainer *cont);
        ~SPIteratorCell() override = default;

        void resetIterator() override;
        void increment() override;
        bool isEnd() override;
        Particle *deref() override;

        std::array<bool, 6> getGhostInfo() { return m_cp_iters[m_cell_ind]->deref()->isGhost; }
        bool isInBoundary() { return m_cp_iters[m_cell_ind]->isInBoundary(); }

    private:
        void nextNonEmptyCell();

        std::vector<Cell> *s_cells;
        unsigned m_cell_ind;
        std::vector<std::unique_ptr<Cell::CellPartIterator>> m_cp_iters;
    };
    std::unique_ptr<SPIteratorCell> createSPIterator();

    class NPIteratorCell : public ParticleIterator<std::pair<Particle *, Particle *>>
    {
    public:
        explicit NPIteratorCell(CellContainer *cont);
        ~NPIteratorCell() override = default;

        void resetIterator() override;
        void increment() override;
        bool isEnd() override;
        std::pair<Particle *, Particle *> deref() override;

        glm::dvec3 getHullCellShift();

    private:
        //------- state machine -------//
        void nextDyn();
        void resetDynIterInSameCell();
        void resetDynIterInHullCell();

        void nextCellInHull();
        void resetCellIterInHull();

        void nextFixed();
        void resetFixedIter();

        void nextCellInd();
        //------- state machine -------//

        double calcSquareDistFixDyn();

    private:
        std::vector<Cell> *s_cells;;
        double m_cut_off;
        bool m_isEnd;

        unsigned m_cell_ind;
        std::vector<std::unique_ptr<Cell::NewtHullIterator>> m_hull_iters;
        std::vector<std::unique_ptr<Cell::CellPartIterator>> m_cp_iters_fix, m_cp_iters_dyn;
        unsigned m_fix_cell_ind, m_dyn_cell_ind;
    };
    std::unique_ptr<NPIteratorCell> createNPIterator();
    //----------------------- iterators -----------------------//

private:
    SPIteratorCell *polymCreateSPIter() override;

private:
    void initCells();

    glm::ivec3 calcCellFromPos(const glm::dvec3 &pos);
    bool validateInd(const glm::ivec3 &ind);
    bool validatePeriodic(const glm::ivec3 &ind);
    HullCell createMirrorHullCell(const glm::ivec3 &ind);

    void handleGhostParticles(CellParticle *cp);
    bool needsGhost(const glm::dvec3 &pos, int32_t type, GhostLoc loc);

private:
    //! one dimensional vector of all cells containing all cellparticles
    std::vector<Cell> m_cells;
    std::vector<std::unique_ptr<Cell::CellPartIterator>> m_cp_iters;
    int m_num_parts = 0;

    //! specifications of cell grid
    glm::dvec3 m_left_corner;
    glm::ivec3 m_dimensions;
    glm::dvec3 m_domain_size;
    glm::dvec3 m_measures;
    std::array<BoundType, 6> m_bound_conds;
    double m_cut_off;
    std::vector<double> m_bounce_distances;

    //! general particle information
    std::vector<GeneralParticleInfo> m_general_info;
};
