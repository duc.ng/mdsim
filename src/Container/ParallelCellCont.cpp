#include "ParallelCellCont.h"

#ifdef _OPENMP

#define SIXTH_ROOT_2 1.122462048
ParallelCellCont::ParallelCellCont(const std::vector<Particle> &particles,
                                   std::vector<GeneralParticleInfo> general_info,
                                   glm::dvec3 center, glm::dvec3 domain_size,
                                   const std::array<BoundType, 6> &bound_conds,
                                   double cut_off, unsigned cell_start_capacity)
        : m_domain_size(domain_size), m_bound_conds(bound_conds),
          m_cut_off(cut_off), m_general_info(std::move(general_info))
{
    // compute dimensions, measures and left_corner
    m_dimensions = (glm::ivec3) (m_domain_size / m_cut_off);
    m_measures = m_domain_size / (glm::dvec3) m_dimensions;
    m_left_corner = center - (m_domain_size / 2.0);

    // compute bounce distances (precompute 2^(1/6))
    m_bounce_distances.reserve(m_general_info.size());
    for(int i = 0; i < m_general_info.size(); i++)
        m_bounce_distances.push_back(SIXTH_ROOT_2 * m_general_info[i].sigma);

    // default construct cells with dimensions and standard start capacity
    std::vector<GridElem> grid_vec{};
    grid_vec.reserve((unsigned) (m_dimensions.z * m_dimensions.y * m_dimensions.x));
    for(unsigned i = 0; i < grid_vec.capacity(); i++)
        grid_vec.push_back({std::make_unique<PCell>(i, cell_start_capacity), 1});

    // construct grid and init the cells in it
    m_cell_grid = std::make_unique<CellGrid>(std::move(grid_vec), m_dimensions);
    initCells();

    // iterate over all particles and push them into cellArray accordingly
    for(const Particle &p : particles) {
        glm::ivec3 index = calcCellFromPos(p.getPos());
        if(!validateInd(index))
            throw std::runtime_error("error: particles given exceed cell grid!");

        PPartPT partPT{p.getPos(), p.getType()};
        PPartVFID partVFID{p.getV(), p.getOldF(), p.getF(), p.getID(), {false}};
        (*this)[index]->push_back(&partPT, &partVFID);
        m_num_parts++;
    }

}

ParallelCellCont::~ParallelCellCont()
{
}

void ParallelCellCont::initKdTree(unsigned num_threads)
{
    // construct KdTree from grid and get leaf_cells
    m_kdTree = std::make_unique<KdTree>(m_cell_grid.get(), num_threads);
    s_leaf_cells = m_kdTree->getLeafCells();
}

void ParallelCellCont::initCells()
{
    // set cell attributes for each cell in grid
    for(int z = 0; z < m_dimensions.z; z++) {
        for(int y = 0; y < m_dimensions.y; y++) {
            for(int x = 0; x < m_dimensions.x; x++) {
                PCell *cell = (*this)[glm::ivec3(x, y, z)];

                // set boundary flag if cell is on edge of cell grid
                cell->isBoundary = (z == 0 || z == m_dimensions.z - 1 ||
                                    y == 0 || y == m_dimensions.y - 1 ||
                                    x == 0 || x == m_dimensions.x - 1);

                // set newt_hull
                // following pattern (2D):  x x x
                // -> x: in newt_hull       o x x
                // -> o: not in new_hull    o o o
                // => in 3D: z slice behind center is in hull, z slice before center not
                int hull_ind = 0;
                std::array<PHullCell, 14> newt_hull{};
                // always has same cell in hull
                newt_hull[hull_ind++] = {cell, glm::dvec3(0.0)};
                // cell right to center
                if(validateInd(glm::ivec3(x + 1, y, z)))
                    newt_hull[hull_ind++] = {(*this)[glm::ivec3(x + 1, y, z)], glm::dvec3(0.0)};
                else if(validatePeriodic(glm::ivec3(x + 1, y, z)))
                    newt_hull[hull_ind++] = createMirrorHullCell(glm::ivec3(x + 1, y, z));
                // row of cells over center
                for(int i = x - 1; i <= x + 1; i++)
                    if(validateInd(glm::ivec3(i, y + 1, z)))
                        newt_hull[hull_ind++] = {(*this)[glm::ivec3(i, y + 1, z)], glm::dvec3(0.0)};
                    else if(validatePeriodic(glm::ivec3(i, y + 1, z)))
                        newt_hull[hull_ind++] = createMirrorHullCell(glm::ivec3(i, y + 1, z));
                // slice of cells behind center cell
                for(int j = y - 1; j <= y + 1; j++)
                    for(int i = x - 1; i <= x + 1; i++)
                        if(validateInd(glm::ivec3(i, j, z + 1)))
                            newt_hull[hull_ind++] = {(*this)[glm::ivec3(i, j, z + 1)], glm::dvec3(0.0)};
                        else if(validatePeriodic(glm::ivec3(i, j, z + 1)))
                            newt_hull[hull_ind++] = createMirrorHullCell(glm::ivec3(i, j, z + 1));


                // hull_ind should now be equivalent to hull_size
                cell->newt_hull = newt_hull;
                cell->hull_size = (unsigned) hull_ind;
            }
        }
    }
}


/**
 * update cellparticles after position calculation step
 * -> move cellparticles to now valid cell
 * -> remove if exceed cell grid
 * -> do periodic wrap around
 * -> create or delete ghost particles
 */
void ParallelCellCont::updateCells()
{
    // update all particles in cell grid (shovel to actual cells) so cell grid is in valid state afterwards
    // store what particles to move to which cell
    struct MovingPart
    {
        PPartPT partPT;
        PPartVFID partVFID;
        PCell *cell;
    };
    std::vector<MovingPart> mov_parts{};
    mov_parts.reserve(m_num_parts / 2);

    // update all particles in cell grid
    // -> first erase them in parallel
    #pragma omp parallel default(none) shared(mov_parts)
    {
        // only iterate over thread-own set of cells
        int leaf_ind = omp_get_thread_num();
        if(leaf_ind < this->s_leaf_cells->size()) {

            std::vector<PCell *> &cell_vec = (*this->s_leaf_cells)[leaf_ind];
            for(auto cell : cell_vec) {

                for(int j = 0; j < cell->num_parts; j++) {
                    PPartPT *partPT = &cell->parts_pt[j];
                    PPartVFID *partVFID = &cell->parts_vfid[j];

                    // calculate new cell index of particle
                    glm::ivec3 indVec = calcCellFromPos(cell->parts_pt[j].pos);
                    int index = indVec.z * m_dimensions.y * m_dimensions.x + indVec.y * m_dimensions.x + indVec.x;
                    if(!validateInd(indVec)) {
                        // if exceeds cell grid, ...
                        if(validatePeriodic(indVec)) {

                            // on periodic boundary: wrap to opposite cell
                            PHullCell mirrorHullCell = createMirrorHullCell(indVec);

                            // copy parts_pt and parts_vfid to new cell
                            // -> we need to now subtract the shift from the old pos
                            partPT->pos -= mirrorHullCell.shift;

                            #pragma omp critical (mov_parts)
                            mov_parts.push_back({*partPT, *partVFID, mirrorHullCell.cell});
                        }
                        // else just permanently delete particle from data structure
                        cell->erase(j);

                    }else {
                        // if valid index, check if still is the same cell
                        PCell *new_cell = m_cell_grid->getGridCellAt(index);
                        if(index != cell->cell_id) {
                            // if new cell -> copy over
                            #pragma omp critical (mov_parts)
                            mov_parts.push_back({*partPT, *partVFID, new_cell});
                            // and remove in old
                            cell->erase(j);
                        }
                        if(new_cell->isBoundary) {
                            // if is boundary cell -> check if ghosts need to be created
                            createGhostParticles(partPT, partVFID);
                        }
                    }
                }
            }
        }
    }

    // now afterwards copy them over sequentially
    for(MovingPart &elem : mov_parts) {
        elem.cell->push_back(&elem.partPT, &elem.partVFID);
    }
}


/**
 * rebalance K-d-Tree to a new cell distribution on threads
 * -> store pointer to leaf_cells
 */
void ParallelCellCont::rebalanceTree()
{
    m_kdTree->rebalance();
    s_leaf_cells = m_kdTree->getLeafCells();
}


PCell *ParallelCellCont::operator[](glm::ivec3 ind)
{
    return m_cell_grid->getGridCellAt(ind.x, ind.y, ind.z);
}

glm::ivec3 ParallelCellCont::calcCellFromPos(const glm::dvec3 &pos)
{
    //normalize position with cutoff and boundary,then round down
    return glm::floor((pos - m_left_corner) / m_measures);
}


bool ParallelCellCont::validateInd(const glm::ivec3 &ind)
{
    // is index in cell grid?
    return (ind.x >= 0 && ind.x < m_dimensions.x &&
            ind.y >= 0 && ind.y < m_dimensions.y &&
            ind.z >= 0 && ind.z < m_dimensions.z);
}

bool ParallelCellCont::validatePeriodic(const glm::ivec3 &ind)
{
    // is index a periodic cell or does it just not exist (boundary conditions)
    return !((ind.x < 0 && m_bound_conds[GhostLoc::GHOST_LEFT] != BoundType::PERIODIC_BOUND) ||
             (ind.x >= m_dimensions.x && m_bound_conds[GhostLoc::GHOST_RIGHT] != BoundType::PERIODIC_BOUND) ||
             (ind.y < 0 && m_bound_conds[GhostLoc::GHOST_DOWN] != BoundType::PERIODIC_BOUND) ||
             (ind.y >= m_dimensions.y && m_bound_conds[GhostLoc::GHOST_UP] != BoundType::PERIODIC_BOUND) ||
             (ind.z < 0 && m_bound_conds[GhostLoc::GHOST_FRONT] != BoundType::PERIODIC_BOUND) ||
             (ind.z >= m_dimensions.z && m_bound_conds[GhostLoc::GHOST_BACK] != BoundType::PERIODIC_BOUND));
}

PHullCell ParallelCellCont::createMirrorHullCell(const glm::ivec3 &ind)
{
    // evaluate cell pointed to by periodic hull cell
    glm::ivec3 pointed_to_ind{ind};
    if(ind.x < 0)
        pointed_to_ind.x += m_dimensions.x;
    else if(ind.x >= m_dimensions.x)
        pointed_to_ind.x -= m_dimensions.x;
    if(ind.y < 0)
        pointed_to_ind.y += m_dimensions.y;
    else if(ind.y >= m_dimensions.y)
        pointed_to_ind.y -= m_dimensions.y;
    if(ind.z < 0)
        pointed_to_ind.z += m_dimensions.z;
    else if(ind.z >= m_dimensions.z)
        pointed_to_ind.z -= m_dimensions.z;

    // also calculate shift of hull cell
    auto lc_pointed_to = glm::dvec3(
            m_left_corner.x + m_measures.x * pointed_to_ind.x,
            m_left_corner.y + m_measures.y * pointed_to_ind.y,
            m_left_corner.z + m_measures.z * pointed_to_ind.z);
    auto lc_actual_ind = glm::dvec3(
            m_left_corner.x + m_measures.x * ind.x,
            m_left_corner.y + m_measures.y * ind.y,
            m_left_corner.z + m_measures.z * ind.z);

    return {(*this)[pointed_to_ind], lc_actual_ind - lc_pointed_to};
}

void ParallelCellCont::createGhostParticles(PPartPT *partPT, PPartVFID *partVFID)
{
    // check if cp needs ghost for every direction, but only if reflecting boundary is set
    for(int i = 0; i < 6; i++) {
        if(m_bound_conds[i] == REFLECT_BOUND) {
            partVFID->isGhost[i] = needsGhost(*partPT, static_cast<GhostLoc>(i));
        }
    }
}


bool ParallelCellCont::needsGhost(const PPartPT &partPT, GhostLoc loc)
{
    switch(loc) {
        case GhostLoc::GHOST_LEFT:
            return partPT.pos.x <= m_left_corner.x + m_bounce_distances[partPT.type];
        case GhostLoc::GHOST_RIGHT:
            return partPT.pos.x > m_left_corner.x + m_domain_size.x - m_bounce_distances[partPT.type];
        case GhostLoc::GHOST_DOWN:
            return partPT.pos.y <= m_left_corner.y + m_bounce_distances[partPT.type];
        case GhostLoc::GHOST_UP:
            return partPT.pos.y > m_left_corner.y + m_domain_size.y - m_bounce_distances[partPT.type];
        case GhostLoc::GHOST_FRONT:
            return partPT.pos.z <= m_left_corner.z + m_bounce_distances[partPT.type];
        case GhostLoc::GHOST_BACK:
            return partPT.pos.z > m_left_corner.z + m_domain_size.z - m_bounce_distances[partPT.type];
        default:
            throw std::runtime_error("error: invalid GhostLoc!");
    }
}

#endif
