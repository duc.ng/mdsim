#include "Container/ParallelCellCont.h"

/**
 * single part iterator for parallelCellCont, iterating through every single particle in cell grid
 */

ParallelCellCont::SPIteratorParallel::SPIteratorParallel(ParallelCellCont *cont)
        : s_cont(cont)
{
    SPIteratorParallel::resetIterator();
}

void ParallelCellCont::SPIteratorParallel::resetIterator()
{
    cell_ind = -1;
    part_ind = 0;
    while(++cell_ind < s_cont->m_cell_grid->size()) {
        if(s_cont->m_cell_grid->getGridCellAt(cell_ind)->num_parts > 0)
            break;
    }
}

void ParallelCellCont::SPIteratorParallel::increment()
{
    if(++part_ind >= s_cont->m_cell_grid->getGridCellAt(cell_ind)->num_parts) {
        part_ind = 0;
        while(++cell_ind < s_cont->m_cell_grid->size()) {
            if(s_cont->m_cell_grid->getGridCellAt(cell_ind)->num_parts > 0)
                break;
        }
    }
}

bool ParallelCellCont::SPIteratorParallel::isEnd()
{
    return (cell_ind >= s_cont->m_cell_grid->size());
}

Particle *ParallelCellCont::SPIteratorParallel::deref()
{
    PPartPT *partPT = &s_cont->m_cell_grid->getGridCellAt(cell_ind)->parts_pt[part_ind];
    PPartVFID *partVFID = &s_cont->m_cell_grid->getGridCellAt(cell_ind)->parts_vfid[part_ind];
    m_parts.emplace_back(partPT->pos, partVFID->velo, partVFID->force, partVFID->old_f, partPT->type, partVFID->id);

    return &m_parts.back();
}

PDeref ParallelCellCont::SPIteratorParallel::getPDeref()
{
    PPartPT *partPT = &s_cont->m_cell_grid->getGridCellAt(cell_ind)->parts_pt[part_ind];
    PPartVFID *partVFID = &s_cont->m_cell_grid->getGridCellAt(cell_ind)->parts_vfid[part_ind];

    return {partPT, partVFID};
}

//--------------------------- ParallelCellCont ------------------------------//
std::unique_ptr<ParallelCellCont::SPIteratorParallel> ParallelCellCont::createSPIterator()
{
    return std::unique_ptr<SPIteratorParallel>(polymCreateSPIter());
}

ParallelCellCont::SPIteratorParallel *ParallelCellCont::polymCreateSPIter()
{
    return new SPIteratorParallel(this);
}
//--------------------------- ParallelCellCont ------------------------------//
