#include "Container/BasicContainer.h"

/**
 * simple iterator, iterating through every single particle
 */

BasicContainer::SPIteratorBasic::SPIteratorBasic(BasicContainer *cont)
    : s_particles(&cont->m_particles)
{
    SPIteratorBasic::resetIterator();
}

void BasicContainer::SPIteratorBasic::resetIterator()
{
    m_it = s_particles->begin();
}

void BasicContainer::SPIteratorBasic::increment()
{
    ++m_it;
}

bool BasicContainer::SPIteratorBasic::isEnd()
{
    // if m_it is past last element -> return true
    return (m_it == s_particles->end());
}

Particle *BasicContainer::SPIteratorBasic::deref()
{
    return &*m_it;
}


//--------------------------- BasicContainer ------------------------------//
std::unique_ptr<BasicContainer::SPIteratorBasic> BasicContainer::createSPIterator()
{
    return std::unique_ptr<SPIteratorBasic>(polymCreateSPIter());
}

BasicContainer::SPIteratorBasic *BasicContainer::polymCreateSPIter()
{
    return new SPIteratorBasic(this);
}
//--------------------------- BasicContainer ------------------------------//