#include "Container/CellContainer.h"

/**
 * single part iterator for cellContainer, iterating through every single particle in cell grid
 */

CellContainer::SPIteratorCell::SPIteratorCell(CellContainer *cont)
    : s_cells(&cont->m_cells)
{
    // create particle iterators for every cell
    m_cp_iters.reserve(s_cells->size());
    for(Cell &c : *s_cells)
        m_cp_iters.push_back(c.createCellPartIterator());

    SPIteratorCell::resetIterator();
}

void CellContainer::SPIteratorCell::resetIterator()
{
    // reset cell index to start
    m_cell_ind = 0;

    // only init part_it if there actually are cells (done in nextNonEmptyCell)
    if(m_cell_ind < s_cells->size()) {
        // init to first particle
        nextNonEmptyCell();
    }
}

void CellContainer::SPIteratorCell::increment()
{
    // increment our part_it
    m_cp_iters[m_cell_ind]->increment();

    // if we exceed current cell -> fetch next cell and start searching for non-empty cell from there
    if(m_cp_iters[m_cell_ind]->isEnd()) {
        ++m_cell_ind;
        nextNonEmptyCell();
    }
}

bool CellContainer::SPIteratorCell::isEnd()
{
    // is past last element, if m_ind is past max value (cellArray.size - 1)
    return (m_cell_ind >= s_cells->size());
}

Particle *CellContainer::SPIteratorCell::deref()
{
    return &m_cp_iters[m_cell_ind]->deref()->p;
}


// fetch first particle in next non-empty cell (!! starting at current m_ind !!)
void CellContainer::SPIteratorCell::nextNonEmptyCell()
{
    while(m_cell_ind < s_cells->size()) {
        // get first particle in cell, if non-empty break, else try next
        m_cp_iters[m_cell_ind]->resetIterator();
        if(!m_cp_iters[m_cell_ind]->isEnd())
            break;
        ++m_cell_ind;
    }
}


//--------------------------- CellContainer ------------------------------//
std::unique_ptr<CellContainer::SPIteratorCell> CellContainer::createSPIterator()
{
    return std::unique_ptr<SPIteratorCell>(polymCreateSPIter());
}

CellContainer::SPIteratorCell *CellContainer::polymCreateSPIter()
{
    return new SPIteratorCell(this);
}
//--------------------------- CellContainer ------------------------------//
