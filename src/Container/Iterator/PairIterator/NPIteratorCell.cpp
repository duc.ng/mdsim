#include "Container/CellContainer.h"

/**
 * newtonsh pair iterator for cellContainer
 * -> iterate only over pairs that are within reach (defined by cutoff_radius)
 *    => essentially less than those in adjacent cells
 * -> iterate only once over a set of two particles (3. Law of Newton)
 */

CellContainer::NPIteratorCell::NPIteratorCell(CellContainer *cont)
    : s_cells(&cont->m_cells),
      m_cut_off(cont->m_cut_off)
{
    NPIteratorCell::resetIterator();
}

void CellContainer::NPIteratorCell::resetIterator()
{
    // set cell index to start
    m_isEnd = false;
    m_cell_ind = 0;

    // create particle iterators for m_cp_iters_fix
    m_cp_iters_fix.clear();
    m_cp_iters_fix.reserve(s_cells->size());
    for(Cell &c : *s_cells)
        m_cp_iters_fix.push_back(c.createCellPartIterator());
    m_fix_cell_ind = 0;

    // create particle iterators for m_cp_iters_dyn
    m_cp_iters_dyn.clear();
    m_cp_iters_dyn.reserve(s_cells->size());
    for(Cell &c : *s_cells)
        m_cp_iters_dyn.push_back(c.createCellPartIterator());
    m_dyn_cell_ind = 0;

    while(m_cp_iters_fix[m_fix_cell_ind]->isEnd()) {
        // need to check if cell_ind exceeds s_cells
        if(++m_cell_ind >= s_cells->size()) {
            m_isEnd = true;
            return;
        }
        // else check again on emptiness
        m_fix_cell_ind = m_cell_ind;
    }
    // set dyn to fix (first particle in cell) -> requirement for state automaton
    m_dyn_cell_ind = m_fix_cell_ind;

    // create particle iterators for m_hull_iters (index is here equal to m_fix_cell_ind)
    m_hull_iters.clear();
    m_hull_iters.reserve(s_cells->size());
    for(Cell &c : *s_cells)
        m_hull_iters.push_back(c.createNewtHullIterator());

    // => now m_fix_it and m_dyn_it point to first particle in s_cells and m_hull_cell_it points to center cell
    //    -> increment as we are still not in valid state
    NPIteratorCell::increment();
}

void CellContainer::NPIteratorCell::increment()
{
    // get next (fix, dyn) pair (nextDyn is start of automaton)
    // and check if (fix, dyn) pair is within cut_off radius
    do {
        nextDyn();
    }while(!m_isEnd && calcSquareDistFixDyn() >= m_cut_off*m_cut_off);
}

bool CellContainer::NPIteratorCell::isEnd()
{
    return m_isEnd;
}


/**
 * dereference to std::pair of two particles (this is to be seen as a set of two particles)
 * @note first in pair: dereference of fixed particle, second in pair: dereference of dynamic particle
 * => only second can be a periodic cell particle (needs shift)
 */
std::pair<Particle *, Particle *> CellContainer::NPIteratorCell::deref()
{
    return {&m_cp_iters_fix[m_fix_cell_ind]->deref()->p, &m_cp_iters_dyn[m_dyn_cell_ind]->deref()->p };
}

/**
 * get shift of potential periodic hull cell
 */
glm::dvec3 CellContainer::NPIteratorCell::getHullCellShift()
{
    return m_hull_iters[m_fix_cell_ind]->deref()->shift;
}



// fetch next dyn particle in current hull cell
void CellContainer::NPIteratorCell::nextDyn()
{
    // if exceeds current hull cell -> get next hull cell
    m_cp_iters_dyn[m_dyn_cell_ind]->increment();
    if(m_cp_iters_dyn[m_dyn_cell_ind]->isEnd()) {
        nextCellInHull();
    }
    // else: found pair!
}

// reset m_dyn_it to first particle in same cell as m_fix_it
// -> never fails because m_fix_it is always validated before (nextFixed success or resetFixed success)
void CellContainer::NPIteratorCell::resetDynIterInSameCell()
{
    // set dyn to fix (so same pairs in same cell aren't iterated twice)
    m_cp_iters_dyn[m_dyn_cell_ind = m_fix_cell_ind] =
            std::make_unique<Cell::CellPartIterator>(*m_cp_iters_fix[m_fix_cell_ind]);
    // dyn has to be incremented again to avoid reflexive pairs
    nextDyn();
}

// reset m_dyn_it to first particle in current hull cell
void CellContainer::NPIteratorCell::resetDynIterInHullCell()
{
    // if current hull cell empty -> get next hull cell
    m_dyn_cell_ind = m_hull_iters[m_fix_cell_ind]->getCellIndex();
    m_cp_iters_dyn[m_dyn_cell_ind]->resetIterator();
    if(m_cp_iters_dyn[m_dyn_cell_ind]->isEnd()) {
        nextCellInHull();
    }
    // else: found pair!
}


// fetch next cell in current newt_hull
void CellContainer::NPIteratorCell::nextCellInHull()
{
    m_hull_iters[m_fix_cell_ind]->increment();
    if(m_hull_iters[m_fix_cell_ind]->isEnd()) {
        // if hull cell exceeds current newt_hull -> get next fixed
        nextFixed();
    }else {
        // else: reset dyn to first particle in new hull cell
        resetDynIterInHullCell();
    }
}

// reset m_hull_cell_it to first cell in current newt_hull
// -> never fails for newt_hull is never empty (at least contains current cell)
void CellContainer::NPIteratorCell::resetCellIterInHull()
{
    m_hull_iters[m_fix_cell_ind = m_cell_ind]->resetIterator();
    // dyn has to be reset to first particle in same cell afterwards
    resetDynIterInSameCell();
}


// fetch next fixed particle in current cell
void CellContainer::NPIteratorCell::nextFixed()
{
    m_cp_iters_fix[m_fix_cell_ind]->increment();
    if(m_cp_iters_fix[m_fix_cell_ind]->isEnd()) {
        // if fix exceeds current cell -> get next cell index
        nextCellInd();
    }else {
        // else: reset m_hull_cell_it to first cell of current newt_hull again
        resetCellIterInHull();
    }
}

//reset m_fix_it to first particle in current cell
void CellContainer::NPIteratorCell::resetFixedIter()
{
    m_cp_iters_fix[m_fix_cell_ind = m_cell_ind]->resetIterator();
    if(m_cp_iters_fix[m_fix_cell_ind]->isEnd()) {
        // if current cell empty -> get next cell index
        nextCellInd();
    }else {
        // else: reset m_hull_cell_it to first cell of new newt_hull (= current cell)
        resetCellIterInHull();
    }
}

// fetch next cell index of cellArray
void CellContainer::NPIteratorCell::nextCellInd()
{
    if(++m_cell_ind >= s_cells->size()) {
        // if cell index exceeds size of cellArray -> we reached end
        m_isEnd = true;
    }else {
        // else: reset m_fix_it to first particle in new cell
        resetFixedIter();
    }
}


double CellContainer::NPIteratorCell::calcSquareDistFixDyn()
{
    // only dyn_it needs HullCell shifting for fix_it doesn't iterate over hull
    glm::dvec3 pos1 = m_cp_iters_fix[m_fix_cell_ind]->deref()->p.getPos();
    glm::dvec3 pos2 = m_cp_iters_dyn[m_dyn_cell_ind]->deref()->p.getPos() + m_hull_iters[m_fix_cell_ind]->deref()->shift;

    // distance^2 = (xA - xB)^2 + (yA - yB)^2 + (zA - zB)^2
    glm::dvec3 delta = pos1 - pos2;
    return delta.x*delta.x + delta.y*delta.y + delta.z*delta.z;
}

//--------------------------- CellContainer ------------------------------//
std::unique_ptr<CellContainer::NPIteratorCell> CellContainer::createNPIterator()
{
    return std::make_unique<NPIteratorCell>(this);
}
//--------------------------- CellContainer ------------------------------//
