#include "Container/BasicContainer.h"

/**
 * pair iterator optimized for MolSim (Newton's 3. Law)
 * -> doesn't regard order (only 1,2 - not 2,1)
 * -> doesn't regard reflexive pairs (1,1)
 * => on (1,2,3,4) -> (1,2) (1,3) (1,4) (2,3) (2,4) (3,4)
 */

BasicContainer::NPIteratorBasic::NPIteratorBasic(BasicContainer *cont)
    : s_particles(&cont->m_particles)
{
    NPIteratorBasic::resetIterator();
}

void BasicContainer::NPIteratorBasic::resetIterator()
{
    // init outer on first, inner on second
    m_outerIt = s_particles->begin();
    m_innerIt = ++s_particles->begin();
}

void BasicContainer::NPIteratorBasic::increment()
{
    // increment inner until exceeds
    // -> then increment outer and set inner to next after (new) outer
    if (++m_innerIt == s_particles->end()) {
        m_innerIt = ++m_outerIt;
        ++m_innerIt;
    }
}

bool BasicContainer::NPIteratorBasic::isEnd()
{
    // if one iterator is past last element -> return true
    return (m_outerIt == s_particles->end() || m_innerIt == s_particles->end());
}

std::pair<Particle *, Particle *> BasicContainer::NPIteratorBasic::deref()
{
    return { &*m_outerIt, &*m_innerIt };
}


//--------------------------- BasicContainer ------------------------------//
std::unique_ptr<BasicContainer::NPIteratorBasic> BasicContainer::createNPIterator()
{
    return std::make_unique<NPIteratorBasic>(this);
}
//--------------------------- BasicContainer ------------------------------//

