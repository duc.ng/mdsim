#pragma once

#include <memory>
#include "Part/Particle.h"


/**
 * template base class of all Particle Iterators
 * @tparam T defines the return type of dereferencing the iterator
 */
template <typename T>
class ParticleIterator
{
public:
    ParticleIterator() = default;
    virtual ~ParticleIterator() = default;

    /**
     * reset iterator to begin
     */
    virtual void resetIterator() = 0;

    /**
     * increment the iterator (it++)
     */
    virtual void increment() = 0;

    /**
     * returns true if iterator has exceeded its last element
     */
    virtual bool isEnd() = 0;

    /**
     * dereferences iterator (*it)
     */
    virtual T deref() = 0;
};


