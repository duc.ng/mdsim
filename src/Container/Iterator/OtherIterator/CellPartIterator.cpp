#include "Container/Cell.h"

/**
 * cellparticle iterator, iterating over all valid cellparticles of a cell
 */
Cell::CellPartIterator::CellPartIterator(Cell *cell)
    : s_cell(cell)
{
    CellPartIterator::resetIterator();
}

void Cell::CellPartIterator::resetIterator()
{
    // set index to zero
    m_ind = 0;

    // get first element
    while(m_ind < s_cell->m_size && s_cell->m_cell_parts[m_ind].dirty)
        ++m_ind;
}


void Cell::CellPartIterator::increment()
{
    // increment as long as dirty flag is set on element
    while(++m_ind < s_cell->m_size && s_cell->m_cell_parts[m_ind].dirty);
}

bool Cell::CellPartIterator::isEnd()
{
    return (m_ind >= s_cell->m_size);
}

CellParticle *Cell::CellPartIterator::deref()
{
    return &s_cell->m_cell_parts[m_ind];
}

/**
 * iterator can also be used to remove the current iterator degenerates to
 * -> sets dirty flag and resizes to half capacity if array is only quarter full
 */
void Cell::CellPartIterator::remove()
{
    // set dirty flag on removed particle
    s_cell->m_cell_parts[m_ind].dirty = true;

    // size stays the same, count of valid particles decrements
    s_cell->m_count--;

    // if count decreases to fourth of capacity -> resize (only if count bigger than )
    if(s_cell->m_count < s_cell->m_capacity/4 && s_cell->m_count/2 >= s_cell->m_min_cap)
        s_cell->resize(s_cell->m_count/2);
}


//---------------------------- Cell -------------------------------//
std::unique_ptr<Cell::CellPartIterator> Cell::createCellPartIterator()
{
    return std::make_unique<CellPartIterator>(this);
}
//---------------------------- Cell -------------------------------//
