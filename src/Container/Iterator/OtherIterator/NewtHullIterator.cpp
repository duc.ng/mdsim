#include "Container/Cell.h"

/**
 * newt hull iterator, iterating over all valid HullCells of a cell
 */

Cell::NewtHullIterator::NewtHullIterator(Cell *cell)
    : s_cell(cell)
{
    NewtHullIterator::resetIterator();
}

void Cell::NewtHullIterator::resetIterator()
{
    m_hull_cell_ind = 0;
}

void Cell::NewtHullIterator::increment()
{
    m_hull_cell_ind++;
}

bool Cell::NewtHullIterator::isEnd()
{
    return (m_hull_cell_ind >= s_cell->m_hull_size);
}

HullCell *Cell::NewtHullIterator::deref()
{
    return &s_cell->m_newt_hull[m_hull_cell_ind];
}


//---------------------------- Cell -------------------------------//
std::unique_ptr<Cell::NewtHullIterator> Cell::createNewtHullIterator()
{
    return std::make_unique<NewtHullIterator>(this);
}
//---------------------------- Cell -------------------------------//