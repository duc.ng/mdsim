#pragma once

#include <omp.h>
#include "ParticleContainer.h"
#include "PCell.h"
#include "Container/KdTree/KdTree.h"

class PCellCalculator;
class PCellCalcAcc;
class MembraneCalculator;


class ParallelCellCont : public ParticleContainer
{
public:
    ParallelCellCont(const std::vector<Particle> &particles,
                     std::vector<GeneralParticleInfo> general_info,
                     glm::dvec3 center, glm::dvec3 domain_size,
                     const std::array<BoundType, 6> &bound_conds,
                     double cut_off, unsigned cell_start_capacity);
    ~ParallelCellCont() override;

    void initKdTree(unsigned num_threads);

    void updateCells();
    void rebalanceTree();

    std::vector<GeneralParticleInfo> *getGeneralInfo() { return &m_general_info; }
    int size() const override { return (int)m_num_parts; }
    PCell *operator[](glm::ivec3 ind);

    //----------------------- Calculators ---------------------//
    friend class PCellCalculator;
    friend class PCellCalcAcc;
    friend class MembraneCalculator;
    //----------------------- Calculators ---------------------//

    //----------------------- Iterators -----------------------//
    class SPIteratorParallel : public SPIterator
    {
    public:
        explicit SPIteratorParallel(ParallelCellCont *cont);
        ~SPIteratorParallel() override = default;

        void resetIterator() override;
        void increment() override;
        bool isEnd() override;
        Particle *deref() override;
        PDeref getPDeref();

    private:
        ParallelCellCont *s_cont;
        int cell_ind, part_ind;

        std::vector<Particle> m_parts;
    };
    std::unique_ptr<SPIteratorParallel> createSPIterator();
    //----------------------- Iterators -----------------------//
private:
    SPIteratorParallel *polymCreateSPIter() override;

private:
    void initCells();

    glm::ivec3 calcCellFromPos(const glm::dvec3 &pos);
    bool validateInd(const glm::ivec3 &ind);
    bool validatePeriodic(const glm::ivec3 &ind);
    PHullCell createMirrorHullCell(const glm::ivec3 &ind);

    void createGhostParticles(PPartPT *partPT, PPartVFID *partVFID);
    bool needsGhost(const PPartPT &partPT, GhostLoc loc);

private:
    //! one dimensional vector of all cells containing all cellparticles
    std::unique_ptr<CellGrid> m_cell_grid;
    std::unique_ptr<KdTree> m_kdTree;
    std::vector<std::vector<PCell *>> *s_leaf_cells;
    unsigned long m_num_parts = 0;

    //! specifications of cell grid
    glm::dvec3 m_left_corner{};
    glm::ivec3 m_dimensions{};
    glm::dvec3 m_domain_size;
    glm::dvec3 m_measures{};
    std::array<BoundType, 6> m_bound_conds;
    double m_cut_off;
    std::vector<double> m_bounce_distances;

    //! general particle information
    std::vector<GeneralParticleInfo> m_general_info;
};

