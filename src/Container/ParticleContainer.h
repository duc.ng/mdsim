#pragma once

#include <memory>
#include <vector>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/glm.hpp>
#include <glm/gtx/norm.hpp>

#include "Part/Particle.h"
#include "Container/Iterator/ParticleIterator.h"
#include "Container/Iterator/SPIterator.h"

/**
 * container class which stores all particle data
 */
struct GeneralParticleInfo {
    double sigma;
    double epsilon;
    double mass;
};

enum BoundType {
    OUTFLOW_BOUND,
    REFLECT_BOUND,
    PERIODIC_BOUND
};
enum GhostLoc {
    GHOST_LEFT,  GHOST_RIGHT,
    GHOST_DOWN,  GHOST_UP,
    GHOST_FRONT, GHOST_BACK
};

class ParticleContainer
{
public:
    ParticleContainer() = default;
    virtual ~ParticleContainer() = default;

    virtual int size() const = 0;

    std::unique_ptr<SPIterator> createSPIterator() { return std::unique_ptr<SPIterator>(polymCreateSPIter()); }

private:
    virtual SPIterator *polymCreateSPIter() = 0;
};

