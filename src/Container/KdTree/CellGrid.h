#pragma once

#include <vector>
#include <memory>

#include "Container/PCell.h"

struct GridElem
{
    std::unique_ptr<PCell> cell;
    double score;
};

class CellGrid
{
public:
    CellGrid(std::vector<GridElem> grid_vec, const glm::ivec3 &grid_dims);
    ~CellGrid() = default;

    double calcScoresInGrid();
    const glm::ivec3 &getGridDims() const { return m_grid_dims; }
    unsigned long size() const { return m_grid.size(); }

    double sumRegion(const glm::ivec3 &start, const glm::ivec3 &end) const;
    double getGridScoreAt(int x, int y, int z) const;
    PCell *getGridCellAt(int x, int y, int z) const;
    PCell *getGridCellAt(int index) const { return m_grid[index].cell.get(); }

private:
    std::vector<GridElem> m_grid;
    glm::ivec3 m_grid_dims;
};
