#include "CellGrid.h"

CellGrid::CellGrid(std::vector<GridElem> grid_vec, const glm::ivec3 &grid_dims)
        : m_grid(std::move(grid_vec)), m_grid_dims(grid_dims) {}

double CellGrid::calcScoresInGrid()
{
    double total_score = 0;
    // calculate scores for all elements of grid
    for(auto &elem : m_grid) {
        // include all particle in cell
        elem.score = elem.cell->num_parts;

        // only sum up hull cell particles if cell is non-empty
        if(elem.score <= 0)
            continue;

        // and all particles in newt_hull of cell (with factor 0.5 though)
        for(int fix = 0; fix < elem.cell->num_parts; fix++)
            for(int dyn = 1; dyn < elem.cell->hull_size; dyn++)
                elem.score += elem.cell->newt_hull[dyn].cell->num_parts;

        // add up total_score
        total_score += elem.score;
    }
    return total_score;
}

double CellGrid::sumRegion(const glm::ivec3 &start, const glm::ivec3 &end) const
{
    double sum = 0;
    for(int x = start.x; x <= end.x; x++)
        for(int y = start.y; y <= end.y; y++)
            for(int z = start.z; z <= end.z; z++)
                sum += getGridScoreAt(x, y, z);
    return sum;
}

double CellGrid::getGridScoreAt(int x, int y, int z) const
{
    return m_grid[z * m_grid_dims.y * m_grid_dims.x + y * m_grid_dims.x + x].score;
}

PCell *CellGrid::getGridCellAt(int x, int y, int z) const
{
    return m_grid[z * m_grid_dims.y * m_grid_dims.x + y * m_grid_dims.x + x].cell.get();
}

