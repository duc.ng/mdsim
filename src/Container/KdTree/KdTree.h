#pragma once

#include <vector>
#include "Container/PCell.h"

#include "CellGrid.h"
#include "KdTreeNode.h"


class KdTree
{
public:
    KdTree(CellGrid *grid, unsigned num_threads);
    ~KdTree();

    void rebalance();
    std::vector<std::vector<PCell *>> *getLeafCells() { return &m_leaf_cells; }

private:
    void fill_leaf_cells();

private:
    CellGrid *s_grid;
    unsigned m_num_threads;

    std::unique_ptr<KdTreeNode> m_root;
    std::vector<const KdTreeNode *> m_leaves;
    std::vector< std::vector<PCell *> > m_leaf_cells;
};

