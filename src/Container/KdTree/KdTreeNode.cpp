#include "KdTreeNode.h"

void KdTreeNode::partitionOnGrid(bool isRoot, const CellGrid &grid, std::vector<const KdTreeNode *> *leaves)
{
    // test end of recursion
    if(this->num_theads <= 1) {
        (*leaves)[this->thread_id] = this;
        return;
    }

    // create table to calculate optimal splittings (with minimal error)
    std::array<std::vector<OptTableElem>, 3> opt_table; // one table for every dimensions
    createOptTable(&opt_table, grid);

    // now we have the optimal element
    OptTableElem *opt_elem = find_opt_elem(opt_table);

    // if no element is found...
    if(opt_elem == nullptr) {
        // ... try with homogeneous grid
        opt_table.fill({}); // don't forget to empty opt_table
        createOptTableHomog(&opt_table);
        opt_elem = find_opt_elem(opt_table);

        // and else there is no valid distribution
        if(opt_elem == nullptr)
            throw std::runtime_error("error in KdTreeNode::find_opt_elem(): too few cells for thread distribution!");

        // if worked get opt_elem right again
        opt_elem->region_scoreA = grid.sumRegion(this->start_vec, opt_elem->end_vecA);
        opt_elem->region_scoreB = this->score - opt_elem->region_scoreA;
    }

    // create left child node for region A
    this->lChild = std::make_unique<KdTreeNode>(
            this->thread_id, opt_elem->thr_numA,
            this->start_vec, opt_elem->end_vecA,
            opt_elem->region_scoreA);
    // recursive call left child (region A)
    this->lChild->partitionOnGrid(false, grid, leaves);

    // create right child node for region B
    this->rChild = std::make_unique<KdTreeNode>(
            this->thread_id + this->lChild->num_theads, opt_elem->thr_numB,
            opt_elem->start_vecB, this->end_vec,
            opt_elem->region_scoreB);
    // recursive call right child (region B)
    this->rChild->partitionOnGrid(false, grid, leaves);
}

void KdTreeNode::createOptTable(std::array<std::vector<OptTableElem>, 3> *opt_table, const CellGrid &grid)
{
    // calculate optimal load per thread (needed for calculation of thread)
    double opt_load_per_thr = this->score / this->num_theads;
    glm::ivec3 region_dims_no_end = this->end_vec - this->start_vec;
    int load_of_func = region_dims_no_end.x * region_dims_no_end.y * region_dims_no_end.z;
    for(int dim = 0; dim < 3; dim ++)
        (*opt_table)[dim].resize((unsigned) region_dims_no_end[dim]);

    // for every dimension ...
    /*#ifdef _OPENMP
    #pragma omp parallel for if(load_of_func > 100) \
            default(none) shared(opt_table, region_dims_no_end, opt_load_per_thr, grid)
    #endif*/
    for(int dim = 0; dim < 3; dim++) {
        // ... create opt_table (with size of current region dimension)
        for(int i = 0; i < region_dims_no_end[dim]; i++) {
            OptTableElem *elem = &(*opt_table)[dim][i];

            // calculate end_vec of region A (start_vecA == node.start_vec)
            elem->end_vecA = this->end_vec;
            elem->end_vecA[dim] = this->start_vec[dim] + i;
            // calculate start_vec of region B (end_vecB == node.end_vec)
            elem->start_vecB = this->start_vec;
            elem->start_vecB[dim] = elem->end_vecA[dim] + 1;

            // sum up all scores in region A
            // if we're splitting on x-axis (dim == 0), slice will be on y-axis
            elem->region_scoreA = grid.sumRegion(this->start_vec, elem->end_vecA);
            // region score B can be calculated directly from region score A (and overall score)
            elem->region_scoreB = this->score - elem->region_scoreA;

            // calculate optimal thread number for region A (not needed for B)
            double opt_thr_numA = elem->region_scoreA / opt_load_per_thr;

            // round it to get actual number (and ensure it doesn't get all or no threads)
            int rounded_thr_numA = (int) std::lround(opt_thr_numA);
            if(rounded_thr_numA <= 0) elem->thr_numA = 1;
            else if(rounded_thr_numA >= this->num_theads) elem->thr_numA = this->num_theads - 1;
            else elem->thr_numA = rounded_thr_numA;

            // thread numbers of B can be calculated directly from those of A
            elem->thr_numB = this->num_theads - elem->thr_numA;

            // error is absolute difference of optimal thread number and actual thread number
            // error is the same in A and B (so we use A)
            elem->error = std::abs(opt_thr_numA - elem->thr_numA);
        }
    }
}

void KdTreeNode::createOptTableHomog(std::array<std::vector<OptTableElem>, 3> *opt_table)
{
    // calculate optimal load per thread (needed for calculation of thread)
    glm::ivec3 region_dims_no_end = this->end_vec - this->start_vec;
    double homog_total_score = (region_dims_no_end.x+1) * (region_dims_no_end.y+1) * (region_dims_no_end.z+1);
    double opt_load_per_thr = homog_total_score / this->num_theads;
    int load_of_func = region_dims_no_end.x * region_dims_no_end.y * region_dims_no_end.z;

    // resize opt_table
    for(int dim = 0; dim < 3; dim ++)
        (*opt_table)[dim].resize((unsigned) region_dims_no_end[dim]);

    // for every dimension ...
    /*#ifdef _OPENMP
    #pragma omp parallel for if(load_of_func > 100) \
            default(none) shared(opt_table, region_dims_no_end, opt_load_per_thr)
    #endif*/
    for(int dim = 0; dim < 3; dim++) {
        // ... create opt_table (with size of current region dimension)
        for(int i = 0; i < region_dims_no_end[dim]; i++) {
            OptTableElem *elem = &(*opt_table)[dim][i];

            // calculate end_vec of region A (start_vecA == node.start_vec)
            elem->end_vecA = this->end_vec;
            elem->end_vecA[dim] = this->start_vec[dim] + i;
            // calculate start_vec of region B (end_vecB == node.end_vec)
            elem->start_vecB = this->start_vec;
            elem->start_vecB[dim] = elem->end_vecA[dim] + 1;

            // sum up all scores in region A
            // if we're splitting on x-axis (dim == 0), slice will be on y-axis
            glm::ivec3 regionA_dims = (elem->end_vecA - this->start_vec) + 1;
            glm::ivec3 regionB_dims = (this->end_vec - elem->start_vecB) + 1;
            elem->region_scoreA = regionA_dims.x * regionA_dims.y * regionA_dims.z;
            elem->region_scoreB = regionB_dims.x * regionB_dims.y * regionB_dims.z;

            // calculate optimal thread number for region A (not needed for B)
            double opt_thr_numA = elem->region_scoreA / opt_load_per_thr;

            // round it to get actual number (and ensure it doesn't get all or no threads)
            int rounded_thr_numA = (int) std::lround(opt_thr_numA);
            if(rounded_thr_numA <= 0) elem->thr_numA = 1;
            else if(rounded_thr_numA >= this->num_theads) elem->thr_numA = this->num_theads - 1;
            else elem->thr_numA = rounded_thr_numA;

            // thread numbers of B can be calculated directly from those of A
            elem->thr_numB = this->num_theads - elem->thr_numA;

            // error is absolute difference of optimal thread number and actual thread number
            // error is the same in A and B (so we use A)
            elem->error = std::abs(opt_thr_numA - elem->thr_numA);
        }
    }
}


OptTableElem *KdTreeNode::find_opt_elem(std::array<std::vector<OptTableElem>, 3> &opt_table)
{
    // go through all dimensions (and pick minimum)
    int min_dim = -1;
    int min_ind_all_dim = -1;
    double min_error_all_dim = std::numeric_limits<double>::infinity();

    for(int dim = 0; dim < opt_table.size(); dim++) {

        // go through every element in opt_table (and pick minimum)
        int min_ind_in_dim = -1;
        double min_error_in_dim = std::numeric_limits<double>::infinity();
        for(int i = 0; i < opt_table[dim].size(); i++) {

            // check if new one is smaller than minimum till now
            OptTableElem *elem = &opt_table[dim][i];
            if(elem->error < min_error_in_dim) {
                // also check if splitting is valid
                if(hasSplittingEnoughCells(*elem)) {
                    min_error_in_dim = elem->error;
                    min_ind_in_dim = i;
                }
            }
        }

        // at end propagate found minimum up (if smaller)
        if(min_error_in_dim < min_error_all_dim) {
            min_dim = dim;
            min_ind_all_dim = min_ind_in_dim;
            min_error_all_dim = min_error_in_dim;
        }
    }

    // error check (if no optimum was found, this means no splitting was valid)
    if(min_dim < 0)
        return nullptr;

    return &opt_table[min_dim][min_ind_all_dim];
}

bool KdTreeNode::hasSplittingEnoughCells(const OptTableElem &elem)
{
    // test region A (does region A have enough cells):
    glm::ivec3 dimA = elem.end_vecA - this->start_vec + 1;
    if(dimA.x*dimA.y*dimA.z < elem.thr_numA)
        return false;

    // test region B (does region B have enough cells):
    glm::ivec3 dimB = this->end_vec - elem.start_vecB + 1;
    if(dimB.x*dimB.y*dimB.z < elem.thr_numB)
        return false;

    // else splitting is valid
    return true;
}



