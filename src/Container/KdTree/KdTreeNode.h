#pragma once

#include <memory>
#include <glm/glm.hpp>
#include <vector>
#include <numeric>

#include "CellGrid.h"

struct OptTableElem
{
    double region_scoreA, region_scoreB;
    glm::ivec3 end_vecA, start_vecB;
    unsigned thr_numA, thr_numB;
    double error;
};

class KdTreeNode
{
public:
    KdTreeNode(unsigned thread_id_, unsigned num_threads_,
               const glm::ivec3 &start_vec_, const glm::ivec3 &end_vec_,
               double score_)
            : thread_id(thread_id_), num_theads(num_threads_),
              start_vec(start_vec_), end_vec(end_vec_),
              score(score_) {}

    void partitionOnGrid(bool isRoot, const CellGrid &grid, std::vector<const KdTreeNode *> *leaves);
    void createOptTable(std::array<std::vector<OptTableElem>, 3> *opt_table, const CellGrid &grid);
    void createOptTableHomog(std::array<std::vector<OptTableElem>, 3> *opt_table);

private:
    OptTableElem *find_opt_elem(std::array<std::vector<OptTableElem>, 3> &opt_table);
    bool hasSplittingEnoughCells(const OptTableElem &elem);

public:
    unsigned thread_id, num_theads;
    glm::ivec3 start_vec, end_vec;
    double score;

    std::unique_ptr<KdTreeNode> lChild;
    std::unique_ptr<KdTreeNode> rChild;
};
