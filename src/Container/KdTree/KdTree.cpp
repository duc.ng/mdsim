#include "KdTree.h"


KdTree::KdTree(CellGrid *grid, unsigned num_threads)
        : s_grid(grid), m_num_threads(num_threads)
{
    if(m_num_threads < 1)
        throw std::runtime_error("error: KdTree received invalid number of threads!");

    // resize m_leaf_cells and m_leaves
    m_leaf_cells.resize((unsigned) m_num_threads);
    m_leaves.resize((unsigned) m_num_threads);

    // create root node and rebalance tree
    m_root = std::make_unique<KdTreeNode>(0, m_num_threads, glm::ivec3(0), s_grid->getGridDims() - 1, 0.0);
    KdTree::rebalance();
}

KdTree::~KdTree() {}

void KdTree::rebalance()
{
    m_root->score = s_grid->calcScoresInGrid();
    m_root->partitionOnGrid(true, *s_grid, &m_leaves);
    fill_leaf_cells();
}

void KdTree::fill_leaf_cells()
{
    for(auto node : m_leaves) {
        std::vector<PCell *> cell_vec{};
        for(int x = node->start_vec.x; x <= node->end_vec.x; x++)
            for(int y = node->start_vec.y; y <= node->end_vec.y; y++)
                for(int z = node->start_vec.z; z <= node->end_vec.z; z++)
                    cell_vec.push_back(s_grid->getGridCellAt(x, y, z));

        m_leaf_cells[node->thread_id] = cell_vec;
    }
}


