#pragma once

#include <memory>
#include "Part/Particle.h"
#include "Iterator/ParticleIterator.h"

/**
 * CellParticle describes particle in dynamic array of cell
 */
struct CellParticle
{
    Particle p;                     //!< actual particle (no pointer!)
    std::array<bool, 6> isGhost;    //!< does particle have ghost particle at specific GhostLoc
    bool dirty;                     //!< set dirty flag indicates particle has been removed

    /** padding to 128 Bytes (2 Cachelines a 64 Bytes) */
    volatile uint64_t pad[1];
};

/**
 * HullCell describes a cell in the newt_hull of a cell
 */
class Cell;
struct HullCell
{
    Cell *cell;         //!< pointer to actual adjacent cell
    glm::dvec3 shift;   //!< shift added on particle positions (non-zero when peridic hull cell)
};


/**
 * Cell describes one cubical cell block in the cell grid
 */
class Cell
{
public:
    Cell(int index, unsigned capacity);
    ~Cell();

    void push_back(const CellParticle &p);

    void setNewtHull(const std::array<HullCell, 14> &newt_hull, unsigned hull_size);
    void setIsBoundary(bool isBoundary) { m_isBoundary = isBoundary; }
    bool getIsBoundary() { return m_isBoundary; }


    //----------------------- Calculators ---------------------//
    friend class CellCalculatorNoIter;
    //----------------------- Calculators ---------------------//

    //----------------------- iterators -----------------------//
    class CellPartIterator : public ParticleIterator<CellParticle *>
    {
    public:
        explicit CellPartIterator(Cell *cell);
        ~CellPartIterator() override = default;

        void resetIterator() override;
        void increment() override;
        bool isEnd() override;
        CellParticle *deref() override;

        void remove();
        bool isInBoundary() { return s_cell->getIsBoundary(); } //!< returns true if current cell is boundary cell

    private:
        Cell *s_cell;
        unsigned m_ind;
    };
    std::unique_ptr<CellPartIterator> createCellPartIterator();

    class NewtHullIterator : public ParticleIterator<HullCell *>
    {
    public:
        explicit NewtHullIterator(Cell *cell);
        ~NewtHullIterator() override = default;

        void resetIterator() override;
        void increment() override;
        bool isEnd() override;
        HullCell *deref() override;

        int getCellIndex() { return s_cell->m_newt_hull[m_hull_cell_ind].cell->m_index; }

    private:
        Cell *s_cell;
        unsigned m_hull_cell_ind;
    };
    std::unique_ptr<NewtHullIterator> createNewtHullIterator();
    //----------------------- iterators -----------------------//

private:
    void resize(unsigned new_capacity);

private:
    //! index in cell grid (sort of an id)
    int m_index;

    //! flag set if is a boundary cell,
    bool m_isBoundary;

    //! a hull of adjacent cells,
    std::array<HullCell, 14> m_newt_hull{};
    unsigned m_hull_size;

    //! a dynamic array of cellparticles
    CellParticle *m_cell_parts;
    unsigned m_count, m_size, m_min_cap, m_capacity;

    volatile uint64_t pad[3];
};
