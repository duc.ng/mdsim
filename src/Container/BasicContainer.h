#pragma once

#include "ParticleContainer.h"

/**
 * a basic particle container, essentially just consisting of a vector of all particles
 * and one of all general particle information
 * -> it provides a interface to iterators
 */
class BasicContainer : public ParticleContainer
{
public:
    BasicContainer(std::vector<Particle> particles, std::vector<GeneralParticleInfo> general_info);
    ~BasicContainer() override;

    int size() const override { return m_particles.size(); }
    std::vector<GeneralParticleInfo> *getGeneralInfo() { return &m_general_info; }


    class SPIteratorBasic : public SPIterator
    {
    public:
        explicit SPIteratorBasic(BasicContainer *cont);
        ~SPIteratorBasic() override = default;

        void resetIterator() override;
        void increment() override;
        bool isEnd() override;
        Particle *deref() override;

    private:
        std::vector<Particle> *s_particles;
        std::vector<Particle>::iterator m_it;
    };

    class NPIteratorBasic : public ParticleIterator<std::pair<Particle *, Particle *>>
    {
    public:
        explicit NPIteratorBasic(BasicContainer *cont);
        ~NPIteratorBasic() override = default;

        void resetIterator() override;
        void increment() override;
        bool isEnd() override;
        std::pair<Particle *, Particle *> deref() override;

    private:
        std::vector<Particle> *s_particles;
        std::vector<Particle>::iterator m_innerIt, m_outerIt;
    };

    //----------------------- iterators -----------------------//
    std::unique_ptr<SPIteratorBasic> createSPIterator();
    std::unique_ptr<NPIteratorBasic> createNPIterator();
    //----------------------- iterators -----------------------//

private:
    SPIteratorBasic *polymCreateSPIter() override;

private:
    //static log4cxx::LoggerPtr logger;

    std::vector<Particle> m_particles;
    std::vector<GeneralParticleInfo> m_general_info;
};

