#include "BasicContainer.h"


BasicContainer::BasicContainer(std::vector<Particle> particles,
                               std::vector<GeneralParticleInfo> general_info)
    : m_particles(std::move(particles)), m_general_info(std::move(general_info))
{
}

BasicContainer::~BasicContainer()
{
}
