#include "Cell.h"

/**
 * constructor only takes capacity and allocates m_cell_parts accordingly
 * -> m_isBoundary and m_newt_hull need to be set extra
 * ->
 * @param capacity initial capacity of dynamic array
 */
Cell::Cell(int index, unsigned capacity)
    : m_index(index),
      m_isBoundary(false),
      m_hull_size(0),
      m_count(0), m_size(0),
      m_min_cap(capacity), m_capacity(capacity)
{
    m_cell_parts = new CellParticle[m_capacity];
}

Cell::~Cell()
{
    delete[] m_cell_parts;
}


/**
 * inserts given cellparticle at end of m_cell_parts
 * if exceeds capacity -> resize to double capacity (copying only non-dirty cellparticles)
 * @param p cellparticle to be inserted
 */
void Cell::push_back(const CellParticle &p)
{
    // if exceeds capacity -> resize to double capacity
    if(m_size+1 > m_capacity)
        resize(m_capacity*2);

    // after (!) potential resize increment
    ++m_count; ++m_size;

    // then hang new particle to end of list (copy!)
    // -> if not boundary cell, ghostInfo can be reset (no ghosts in inner cells)
    if(m_isBoundary)
        m_cell_parts[m_size-1] = p;
    else
        m_cell_parts[m_size-1] = { p.p, {false}, p.dirty, 0 };
}

/**
 * sets m_newt_hull and m_hull_size to given parameter
 */
void Cell::setNewtHull(const std::array<HullCell, 14> &newt_hull, unsigned hull_size)
{
    m_hull_size = hull_size;
    for(unsigned i=0; i<m_hull_size; i++)
        m_newt_hull[i] = newt_hull[i];
}


/**
 * allocates new dynamic array and copies all non-dirty cellparticles over
 * -> deletes old array and points m_cell_parts to new array
 * @param new_capacity capacity of new array
 */
void Cell::resize(unsigned new_capacity)
{
    // tmp array with new size (don't forget to set m_capacity!)
    auto *tmp = new CellParticle[m_capacity = new_capacity];

    // copy all valid particles to tmp
    int tmp_ind = 0;
    for(unsigned i=0; i<m_size; i++) {
        if(!m_cell_parts[i].dirty)
            tmp[tmp_ind++] = m_cell_parts[i];
    }

    // delete old linked list and pass new list to m_linked_parts
    delete[] m_cell_parts;
    m_cell_parts = tmp;

    // size is now exactly count
    m_size = m_count;
}

