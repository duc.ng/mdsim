#include "Simulator.h"
//#include <log4cxx/propertyconfigurator.h>
#include <chrono>
#include <iostream>

using namespace std;

using std::unique_ptr;

int main(int argc, char *argv[])
{
    // Get starting timepoint
    auto start = std::chrono::high_resolution_clock::now();

    // Set up a simple configuration via file that logs on the console.
    //log4cxx::PropertyConfigurator::configure("../../log4cxx-config_file.txt");

    if (argc != 2)
    {
        cerr << "usage: " << argv[0] << " file.xml" << endl;
        return 1;
    }

    Simulator sim(argc, argv);
    sim.main_loop();

    // Get ending timepoint
    auto stop = std::chrono::high_resolution_clock::now();

    // Get duration. Substart timepoints to
    // get durarion. To cast it to proper unit
    // use duration cast method
    auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);

    cout << "Time taken by function: " << duration.count() << " microseconds" << endl;

    return 0;

}
