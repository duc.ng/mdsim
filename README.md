## Vorbereitungen
Apache Log4Cxx installieren:  
*Hinweis: normalerweise reicht der erste Befehl bereits aus!*
```
sudo apt install liblog4cxx-dev libapr1-dev libaprutil1-dev
```
Libraries für OpenGL:  
```
sudo apt install libglew-dev libglfw3-dev libglm-dev
```

## Kompilierung

#### über Kommandozeile

Zum kompilierenden einen Build-Ordner erstellen und rein navigieren:
```
mkdir build
cd build
```
Kompilieren:
```
cmake ..
cmake --build .
```

#### Dokumentation
Zur Generierung der Doxygen-Dokumentation nach dem Kompilieren folgenden Befehl ausführen (sofern Doxygen installiert ist):
```
make doc_doxygen
```

#### über CLion
- CMake Konfiguration: im Menüband auf *Tools* -> *CMake* -> *Reload CMake Project*
- Build: aus den dadurch generierten Build-Konfiguration gewünschte auswählen (**MOLSIM** oder **UNIT_TESTS**)
anschließend mit dem Hammer-Icon zunächst *builden* 
- Ausführen: im Menüband auf *Run* -> *edit configurations* öffnen; nun im Feld *Program Arguments* eintragen (siehe Abschnitt *Benutzung*) und mit *OK* bestätigen und mit dem Play-Icon *ausführen*.

#### Hinweis zu OpenMP
Es ist möglich OpenMP je nach Build-Target über das entsprechende `cmake` file zu deaktivieren.  
**Beispiel:** Für *MolSim* in `CmakeLists.txt` im root directory Zeile 28 `set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -qopenmp")` auskommentieren. In *Tests* muss(!) OpenMP aktiviert sein.



## Benutzung
Für die Simulation sind die folgenden beiden XML Dateien notwendig:
* args.xml
* data.xml

konkretes Beispiel für Benutzung: 
```
./MolSim ../../XMLrelated/inputs/big_rt_args.xml
```

### Hinweis:
Alle XML-Eingabedateien befinden sich im Ordner `XMLrelated`.

### args.xml
Parameter                | Beschreibung                                   | Typ    | Wertebereich
------------------------ | ------------                                   | ------ | ------------
`out_enabled`            | (De-)aktiviert `.vtk` Ausgabe                  | bool   | true, false
`output_basename`        | Basename der `.vtk` Dateien                    | string | 
`output_frequency`       | Anzahl Interationen bis Ausgabe erfolgt        | int    | x >= 0, z.B: 100
| |
`t_end`                  | absolute Dauer der Simulation                  | double | x > 0 (Default: 1.0)
`delta_t`                | kleinste Zeiteinheit die simuliert wird        | double | x > 0 (Default: 0.0002f)
| |
`calculatorname`         | Angabe des zu benutzenden Calculators          | string | 
| |
`eingabefile`            | XML-Datei mit Simulationsdaten (Cuboids, Spheres, ...)  | string | URI zu .xml Datei
| |
`algorithm`              | Angabe des Simulationsalgorithmus              | string | linked_cell, direct_sum
| |
`vis_enabled`            | (De-)aktiviert OpenGL basierte Visualisierung  | bool   | true, false
`vis_color_range_min`    | Farbspektrum der Visualisierung                | double |  0.0
`vis_color_range_max`    | Farbspektrum der Visualisierung                | double | 50.0
| |
`thermostat_enabled`     | (De-)aktiviert das Thermostat                  | bool   | true, false
`gravity_enabled`        | (De-)aktiviert Gravitation                     | bool   | true, false
`gravity_constant`       | Angabe der Gravitationskonstante               | double | z.B: -12.44
`gravity_axis`           | Angabe der Gravitationsachse                   | string | x, y, z
| |
`import_simulation_data` | (De-)aktiviert das Importieren einer Datei     | bool   | true, false
`export_simulation_data` | (De-)aktiviert das Exportieren einer Datei     | bool   | true, false
`import_export_fileURI`  | Angabe der Datei für Im-/Export                | string | URI zu .txt Datei
`number_of_bins`         | Anzahl der Bins fürBerechnung der Statistik    | int    | 
`statistics_enabled`     | Ausgabe der Statistiken als CSV Datei in /bin  | bool   | 
`membrane_enabled`       | A5T1: Membran-Berechnung an/ausschalten        | bool   | 
`fz_up`                  | nach oben wirkende Kraft                       | double | 
`k`                      | Stiffness-Konstante                            | int    | 
`r`                      | durchschn. Bond-Länge eines Molekülpaars       | double | 
`x_length`               | Länge der Membran                              | int    | 
| |
`temp_init`              | Initialisierungstemperatur                     | double | 
`n_thermostat`           | Anzahl der Iterationen nach denen das Thermostat die Temperatur anpasst  | int |
`needs_init`             | Angabe ob das Thermostat gleich zu Beginn initialisiert werden muss | bool | true, false
`num_dim_of_sim`         | Anzahl der Dimensionen der Simulation          | int | {1, 2, 3}
`temp_target`            | Zieltemperatur der Simulation (optional)       | double | 
`gradual`                | Angabe ob die Temperatur schrittweise auf die Zieltemperatur erhöht werden soll | bool | true, false
`temp_delta`             | Angabe der Temperaturschritte (optional)       | double | 
| |
`center_x`               | Mittelpunkt der Simulationsdomäne              | double | x > 0
`center_y`               | Mittelpunkt der Simulationsdomäne              | double | x > 0
`center_z`               | Mittelpunkt der Simulationsdomäne              | double | x > 0
| |
`domain_x`               | Breite der Simulationsdomäne                   | double | x > 0
`domain_y`               | Höhe der Simulationsdomäne                     | double | x > 0
`domain_z`               | Tiefe der Simulationsdomäne                    | double | x > 0
| |
`cutoffradius`           | Cutoffradius für den Linked-Cell-Algorithmus   | double | x > 0
| |
`left_bound_cond`        | Spezifikation des Wand-Typs der Simulation     | string | reflect, periodic, outflow
`right_bound_cond`       | Spezifikation des Wand-Typs der Simulation     | string | reflect, periodic, outflow
`down_bound_cond`        | Spezifikation des Wand-Typs der Simulation     | string | reflect, periodic, outflow
`up_bound_cond`          | Spezifikation des Wand-Typs der Simulation     | string | reflect, periodic, outflow
`front_bound_cond`       | Spezifikation des Wand-Typs der Simulation     | string | reflect, periodic, outflow
`back_bound_cond`        | Spezifikation des Wand-Typs der Simulation     | string | reflect, periodic, outflow

### data.xml
Parameter                | Beschreibung                                        | Typ    | Wertebereich
------------------------ | ------------                                        | ------ | ------------
`brownian_motion`        | Angabe des Brownian Motion Factors                  | double | x >= 0
`num_dim_of_sim`         | Anzahl der Dimensionen der Simulation               | int    | {1, 2, 3}
`num_particle_types`     | Anzahl der Partikeltypen, z.B. Cuboid, Sphere       | int    | 
`num_objects`            | Anzahl der Partikeltypen, die in der Simulation verwendet werden | int    | 
`num_cuboids`            | Anzahl der Cuboids                                  | int    | x >= 0
`num_spheres`            | Anzahl der Spheres                                  | int    | x >= 0
| |
`sigma`                  | Angabe des Sigma-Faktors                            | double | x >= 0
`epsilon`                | Angabe des Epsilon-Faktors                          | double | x >= 0
`mass`                   | Masse des Partikeltyps                              | double | x >= 0
`fixed`                  | A5T4: Festlegen, ob Partikel des Typs fixiert sind  | bool   | 
| |
| Cuboid |
`type`                   | Referenz auf einen vorher spezifizierten Typ        | int    | 
`distance`               | Abstand der Partikel innerhalb eines Cuboids        | double | 
`left_corner_x`          | X-Koordinate der linken unteren Ecke des Cuboids    | double | 
`left_corner_y`          | Y-Koordinate der linken unteren Ecke des Cuboids    | double | 
`left_corner_z`          | Z-Koordinate der linken unteren Ecke des Cuboids    | double | 
`velo_x`                 | Beschleunigung in X-Richtung                        | double | 
`velo_y`                 | Beschleunigung in Y-Richtung                        | double | 
`velo_z`                 | Beschleunigung in Z-Richtung                        | double | 
`dim_x`                  | Anzahl der Partikel in X-Richtung                   | double | 
`dim_y`                  | Anzahl der Partikel in Y-Richtung                   | double | 
`dim_z`                  | Anzahl der Partikel in Z-Richtung                   | double | 
| |
| Sphere |
`type`                   | Referenz auf einen vorher spezifizierten Typ        | double | 
`distance`               | Abstand der Partikel innerhalb einer Sphere         | int    | 
`radius`                 | Radius der Sphere                                   | int    | 
`center_x`               | Mittelpunkt der Sphere in X-Richtung                | double | x > 0
`center_y`               | Mittelpunkt der Sphere in Y-Richtung                | double | x > 0
`center_z`               | Mittelpunkt der Sphere in Z-Richtung                | double | x > 0
`velo_x`                 | Beschleunigung in X-Richtung                        | double | 
`velo_y`                 | Beschleunigung in Y-Richtung                        | double | 
`velo_z`                 | Beschleunigung in Z-Richtung                        | double |

## Visualisierung

##### Echtzeitvisualisierung
Mit der *vis_enabled* Option des Programms kann die Simulation in Echtzeit in seperatem Fenster visualisiert werden. Diese basiert auf OpenGL sowie GLFW3 und rendert einen Würfel pro Partikel, der nach Geschwindigkeit gefärbt ist. Mittels WASD und der Maus kann die Kamera bewegt werden, mittels Pfeiltasten hoch und runter auf der y-Achse gehoben.

##### ParaView
Die im Build-Ordner erstellten vtk-Files können mit einem entsprechenden Daten-Visualisierungs-Programm, wie z.B. [Paraview](https://www.paraview.org/) geöffnet werden.
- *Import* (zu vtk-Files navigieren und auswählen) -> *Apply*
- Glyph mit folgenden Eigenschaften hinzufügen:
    - *Glyph Type: Sphere*
    - *Scale Factor: 1.0*
    - *Glyph Mode: All points*
- nach Apply: Coloring nach *velocity* richten
- *Zoom To Data*


## Konfiguration log4cxx
Die Konfiguration kann über das File `log4cxx-config_file.txt` angepasst werden, die auszugebenden Log-Statements können z.B. gleich in der ersten Zeile festgelegt werden.    
**Beispiel:** die folgende Zeile *deaktiviert* den Log-Output komplett:
```
log4j.rootLogger=OFF, A1
```

## XML und Codesynthesis XSD
Falls die XML Dateien hinsichtlich ihrer Struktur verändert werden müssen, z.B. weil ein zusätzlicher Paramter benötigt wird, so muss wie folgt vorgegangen werden:
1. entsprechende XSD Schemadatei anpassen
2. Parameter in XML Datei einfügen
3. Rekompilieren des C++ Codes, der durch Codesynthesis XSD erstellt wird:
```
xsd cxx-tree --std c++11 --root-element-last --generate-doxygen DATEI.xsd

```

